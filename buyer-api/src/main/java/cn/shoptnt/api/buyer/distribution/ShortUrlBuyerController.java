/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.distribution;

import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.vo.SuccessMessage;
import cn.shoptnt.model.errorcode.DistributionErrorCode;
import cn.shoptnt.service.distribution.exception.DistributionException;
import cn.shoptnt.model.distribution.dos.ShortUrlDO;
import cn.shoptnt.service.distribution.ShortUrlManager;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.security.model.Buyer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 短链接识别
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/23 上午8:35
 */
@RestController
@RequestMapping("/buyer/distribution/su")
@Tag(name = "短链接api")
public class ShortUrlBuyerController {


    @Resource
    private ShortUrlManager shortUrlManager;
    @Autowired
    private Cache cache;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 访问短链接 把会员id加入session中，并跳转页面
     *
     * @return
     */
    @GetMapping(value = "/visit")
    @Operation(summary = "访问短链接 把会员id加入session中，并跳转页面")
    @Parameter(name = "su", description = "短链接", required = true, in = ParameterIn.QUERY)
    public SuccessMessage visit(String su, @RequestHeader(required = false) String uuid) throws Exception {
        try {
            ShortUrlDO shortUrlDO = shortUrlManager.getLongUrl(su);
            if (shortUrlDO == null) {
                return null;
            }
            if (uuid != null) {
                cache.put(CachePrefix.DISTRIBUTION_UP.getPrefix()+uuid, getMemberId(shortUrlDO.getUrl()));
            }
            return new SuccessMessage(shortUrlDO.getUrl());
        } catch (Exception e) {
            logger.error("短连接验证出错", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());

        }

    }


    /**
     * url中提取member id
     *
     * @param url
     * @return
     */
    private Long getMemberId(String url) {
        String pattern = "(member_id=)(\\d+)";
        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(url);
        if (m.find()) {
            return new Long(m.group(2));
        }
        return 0L;
    }


    @Operation(summary = "生成短链接， 必须登录")
    @PostMapping(value = "/get-short-url")
    @Parameter(name = "goods_id", description = "商品id", in = ParameterIn.QUERY)
    public SuccessMessage getShortUrl(@Parameter(hidden = true) Long goodsId) {

        Buyer buyer = UserContext.getBuyer();
        // 没登录不能生成短链接
        if (buyer == null) {
            throw new DistributionException(DistributionErrorCode.E1001.code(), DistributionErrorCode.E1001.des());
        }
        try {
            long memberId = buyer.getUid();
            ShortUrlDO shortUrlDO = this.shortUrlManager.createShortUrl(memberId, goodsId);
            SuccessMessage successMessage = new SuccessMessage();
            successMessage.setMessage("/distribution/su/visit?su=" + shortUrlDO.getSu());
            return successMessage;
        } catch (Exception e) {
            logger.error("生成短连接出错", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }

}

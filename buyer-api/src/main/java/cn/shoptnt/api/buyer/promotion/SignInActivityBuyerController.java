package cn.shoptnt.api.buyer.promotion;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.model.promotion.sign.vos.SignBuyerInfoVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;import cn.shoptnt.service.promotion.sign.SignInActivityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignInActivityBuyerController
 * @description 签到活动相关api
 * @program: api
 * 2024/3/13 15:14
 */
@RestController
@RequestMapping("/buyer/promotions/sign/activity")
@Tag(name = "签到活动相关Api")
@Validated
public class SignInActivityBuyerController {


    @Autowired
    private SignInActivityManager signInActivityManager;


    @Operation(summary = "获取当前正在举行的签到信息")
    @GetMapping
    public SignBuyerInfoVO getInfo() {
        return signInActivityManager.getInfo();
    }


    @Operation(summary = "签到操作")
    @PostMapping
    public SignBuyerInfoVO sign() {
        return signInActivityManager.sign(UserContext.getBuyer().getUid());
    }

}

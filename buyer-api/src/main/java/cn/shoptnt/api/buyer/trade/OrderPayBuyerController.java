/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.model.payment.dto.PayParam;
import cn.shoptnt.service.trade.order.OrderPayManager;
import cn.shoptnt.model.trade.order.enums.PayStatusEnum;
import cn.shoptnt.model.trade.order.vo.OrderDetailVO;
import cn.shoptnt.service.trade.order.OrderQueryManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author fk
 * @version v2.0
 * @Description: 订单支付
 * @date 2018/4/1616:44
 * @since v7.0.0
 */
@Tag(name = "订单支付API")
@RestController
@RequestMapping("/buyer/order/pay")
@Validated
public class OrderPayBuyerController {

    @Autowired
    private OrderPayManager orderPayManager;

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Operation(summary = "订单检查 是否需要支付 为false代表不需要支付，出现支付金额为0，或者已经支付，为true代表需要支付")
    @GetMapping(value = "/needpay/{sn}")
    public boolean check(@PathVariable(name = "sn") String sn) {
        OrderDetailVO order = this.orderQueryManager.getModel(sn, null);
        return order.getNeedPayMoney() != 0 && !order.getPayStatus().equals(PayStatusEnum.PAY_YES.value());
    }

    @Operation(summary = "对一个交易发起支付")
    @Parameters({
            @Parameter(name = "sn", description = "要支付的交易sn", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "trade_type", description = "交易类型", required = true,   in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{trade_type}/{sn}")
    public Map payTrade(@PathVariable(name = "sn") String sn, @PathVariable(name = "trade_type") String tradeType, @Valid PayParam param) {

        param.setSn(sn);
        param.setTradeType(tradeType.toUpperCase());
        Map map = orderPayManager.pay(param);
        //逻辑附加的参数，去掉
        map.remove("payment_method");
        return map;
    }

    @Operation(summary = "APP对一个交易发起支付")
    @Parameters({
            @Parameter(name = "sn", description = "要支付的交易sn", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "trade_type", description = "交易类型", required = true,   in = ParameterIn.PATH)
    })
    @GetMapping(value = "/app/{trade_type}/{sn}")
    public Map appPayTrade(@PathVariable(name = "sn") String sn, @PathVariable(name = "trade_type") String tradeType, @Valid PayParam param) {

        param.setSn(sn);
        param.setTradeType(tradeType.toUpperCase());
        Map map = orderPayManager.pay(param);

        //逻辑附加的参数，去掉
        map.remove("payment_method");
        return map;
    }

}

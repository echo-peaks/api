/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.system;

import cn.shoptnt.model.system.dos.ComplainTopic;
import cn.shoptnt.service.system.ComplainTopicManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 投诉主题控制器
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2019-11-26 16:06:44
 */
@RestController
@RequestMapping("/buyer/trade/order-complains/topics")
@Tag(name = "投诉主题相关API")
public class ComplainTopicBuyerController {
	
	@Autowired
	private	ComplainTopicManager complainTopicManager;
				

	@Operation(summary	= "查询投诉主题列表")
	@GetMapping
	public List<ComplainTopic> list()	{
		
		return	this.complainTopicManager.list();
	}

				
}
/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import cn.shoptnt.service.trade.order.plugin.PaymentServicePlugin;
import cn.shoptnt.model.trade.order.enums.TradeTypeEnum;
import cn.shoptnt.model.trade.order.vo.BalancePayVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 调试支付回调器
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-04-16
 */

@Service
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class DebuggerCallbackDevice implements PaymentServicePlugin {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public String getServiceType() {
        return TradeTypeEnum.debugger.name();
    }

    @Override
    public Double getPrice(String subSn) {
        return null;
    }

    @Override
    public boolean checkStatus(String subSn, Integer times) {
        return false;
    }

    @Override
    public void paySuccess(String subSn, String returnTradeNo, Double payPrice) {
        logger.debug("支付回调：outTradeNo：【"+subSn+"】returnTradeNo：【"+returnTradeNo+"】payPrice：【"+payPrice+"】");
    }

    @Override
    public void updatePaymentMethod(String subSn, String pluginId, String methodName) {

    }

    @Override
    public void balancePay(BalancePayVO payVO,Long memberId) {

    }
}

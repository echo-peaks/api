/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.member.dos.ReceiptAddressDO;
import cn.shoptnt.service.member.ReceiptAddressManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;

/**
 * 会员收票地址API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
@Tag(name = "会员收票地址API")
@RestController
@RequestMapping("/buyer/members/receipt/address")
@Validated
public class ReceiptAddressBuyerController {

    @Autowired
    private ReceiptAddressManager receiptAddressManager;

    @Operation(summary = "新增会员收票地址")
    @PostMapping
    public ReceiptAddressDO add(@Valid ReceiptAddressDO receiptAddressDO) {

        return receiptAddressManager.add(receiptAddressDO);
    }

    @Operation(summary = "修改会员收票地址")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    @PutMapping(value = "/{id}")
    public ReceiptAddressDO edit(@Valid ReceiptAddressDO receiptAddressDO, @PathVariable Long id) {

        return receiptAddressManager.edit(receiptAddressDO, id);
    }

    @Operation(summary = "查看会员收票地址详细")
    @GetMapping(value = "/detail")
    public ReceiptAddressDO get() {
        ReceiptAddressDO receiptAddressDO = receiptAddressManager.get();
        return receiptAddressDO;
    }
}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.aftersale;

import cn.shoptnt.model.aftersale.dos.AfterSaleExpressDO;
import cn.shoptnt.model.aftersale.dto.AfterSaleOrderDTO;
import cn.shoptnt.model.aftersale.dto.AfterSaleQueryParam;
import cn.shoptnt.model.aftersale.enums.CreateChannelEnum;
import cn.shoptnt.model.aftersale.enums.ServiceTypeEnum;
import cn.shoptnt.model.aftersale.vo.*;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.service.aftersale.AfterSaleManager;
import cn.shoptnt.service.aftersale.AfterSaleQueryManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 售后服务相关API
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-24
 */
@Tag(name ="售后服务相关API")
@RestController
@RequestMapping("/buyer/after-sales")
@Validated
public class AfterSaleBuyerController {

    @Autowired
    private AfterSaleManager afterSaleManager;

    @Autowired
    private AfterSaleQueryManager afterSaleQueryManager;

    @Operation(summary = "获取申请售后服务记录列表")
    @GetMapping()
    public WebPage list(@Valid AfterSaleQueryParam param){
        param.setMemberId(UserContext.getBuyer().getUid());
        param.setCreateChannel(CreateChannelEnum.NORMAL.value());
        return afterSaleQueryManager.list(param);
    }

    @Operation(summary = "获取申请售后页面订单收货信息和商品信息")
    @Parameters({
            @Parameter(name = "order_sn", description = "订单编号", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "sku_id" , description = "商品skuID" , required = true ,  in = ParameterIn.PATH)
    })
    @GetMapping(value = "/apply/{order_sn}/{sku_id}")
    public AfterSaleOrderDTO applyServiceInfo(@PathVariable("order_sn") String orderSn, @PathVariable("sku_id") Long skuId){
        return afterSaleQueryManager.applyOrderInfo(orderSn, skuId);
    }

    @Operation(summary = "订单申请售后服务--退货申请")
    @PostMapping(value = "/apply/return/goods")
    public ReturnGoodsVO applyReturnGoods(@Valid ReturnGoodsVO returnGoodsVO){
        ApplyAfterSaleVO applyAfterSaleVO = new ApplyAfterSaleVO(returnGoodsVO);
        applyAfterSaleVO.setServiceType(ServiceTypeEnum.RETURN_GOODS.value());
        afterSaleManager.apply(applyAfterSaleVO);
        return returnGoodsVO;
    }

    @Operation(summary = "订单申请售后服务--取消订单")
    @PostMapping(value = "/apply/cancel/order")
    public RefundApplyVO applyCancelOrder(@Valid RefundApplyVO refundApplyVO){
        afterSaleManager.applyCancelOrder(refundApplyVO);
        return refundApplyVO;
    }

    @Operation(summary = "订单申请售后服务--换货申请")
    @PostMapping(value = "/apply/change/goods")
    public AfterSaleApplyVO applyChangeGoods(@Valid AfterSaleApplyVO afterSaleApplyVO){
        ApplyAfterSaleVO applyAfterSaleVO = new ApplyAfterSaleVO(afterSaleApplyVO);
        applyAfterSaleVO.setServiceType(ServiceTypeEnum.CHANGE_GOODS.value());
        afterSaleManager.apply(applyAfterSaleVO);
        return afterSaleApplyVO;
    }

    @Operation(summary = "订单申请售后服务--补发申请")
    @PostMapping(value = "/apply/replace/goods")
    public AfterSaleApplyVO applyReplaceGoods(@Valid AfterSaleApplyVO afterSaleApplyVO){
        ApplyAfterSaleVO applyAfterSaleVO = new ApplyAfterSaleVO(afterSaleApplyVO);
        applyAfterSaleVO.setServiceType(ServiceTypeEnum.SUPPLY_AGAIN_GOODS.value());
        afterSaleManager.apply(applyAfterSaleVO);
        return afterSaleApplyVO;
    }

    @Operation(summary = "保存用户填写的物流信息")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后服务单号", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "courier_company_id" , description = "物流公司ID" , required = true ,  in = ParameterIn.QUERY),
            @Parameter(name = "courier_number" , description = "快递单号" , required = true ,   in = ParameterIn.QUERY),
            @Parameter(name = "ship_time" , description = "发货时间" , required = true ,  in = ParameterIn.QUERY)
    })
    @PostMapping(value = "/apply/ship/{service_sn}")
    public AfterSaleExpressDO applyShip(@PathVariable("service_sn") String serviceSn, @Parameter(hidden = true) Long courierCompanyId, @Parameter(hidden = true) String courierNumber, @Parameter(hidden = true) Long shipTime){
        AfterSaleExpressDO afterSaleExpressDO = new AfterSaleExpressDO();
        afterSaleExpressDO.setServiceSn(serviceSn);
        afterSaleExpressDO.setCourierCompanyId(courierCompanyId);
        afterSaleExpressDO.setCourierNumber(courierNumber);
        afterSaleExpressDO.setShipTime(shipTime);
        this.afterSaleManager.applyShip(afterSaleExpressDO);
        return afterSaleExpressDO;
    }

    @Operation(summary = "获取售后服务详细信息")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后服务单号", required = true,   in = ParameterIn.PATH)
    })
    @GetMapping(value = "/detail/{service_sn}")
    public ApplyAfterSaleVO detail(@PathVariable("service_sn") String serviceSn){

        return afterSaleQueryManager.detail(serviceSn, Permission.BUYER);
    }
}

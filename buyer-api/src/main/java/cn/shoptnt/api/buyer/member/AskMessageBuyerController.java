/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.member.dos.AskMessageDO;
import cn.shoptnt.service.member.AskMessageManager;
import cn.shoptnt.framework.database.WebPage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * 会员商品咨询消息API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-18
 */
@RestController
@RequestMapping("/buyer/members/asks/message")
@Tag(name = "会员商品咨询消息API")
@Validated
public class AskMessageBuyerController {

    @Autowired
    private AskMessageManager askMessageManager;

    @Operation(summary = "查询会员商品咨询回复消息列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "is_read", description = "是否已读 YES：是 NO：否",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String isRead) {
        return this.askMessageManager.list(pageNo, pageSize, isRead);
    }

    @PutMapping(value = "/{ids}/read")
    @Operation(summary = "将会员商品咨询消息设置为已读")
    @Parameters({
            @Parameter(name = "ids", description = "要设置为已读消息的id", required = true,  in = ParameterIn.PATH)
    })
    public String setRead(@PathVariable Long[] ids) {
        this.askMessageManager.read(ids);
        return null;
    }

    @Operation(summary = "删除会员商品咨询消息")
    @Parameters({
            @Parameter(name = "ids", description = "会员商品咨询消息id", required = true,  in = ParameterIn.PATH),
    })
    @DeleteMapping("/{ids}")
    public String delete(@PathVariable("ids") Long[] ids) {

        this.askMessageManager.delete(ids);

        return "";
    }
}

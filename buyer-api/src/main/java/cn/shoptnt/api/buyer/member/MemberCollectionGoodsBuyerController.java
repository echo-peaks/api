/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.vo.SuccessMessage;
import cn.shoptnt.model.member.dos.MemberCollectionGoods;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.constraints.NotNull;

import cn.shoptnt.service.member.MemberCollectionGoodsManager;

/**
 * 会员商品收藏表控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 10:13:41
 */
@RestController
@RequestMapping("/buyer/members")
@Tag(name = "会员商品收藏表相关API")
@Validated
public class MemberCollectionGoodsBuyerController {

    @Autowired
    private MemberCollectionGoodsManager memberCollectionGoodsManager;


    @Operation(summary = "查询会员商品收藏列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping("/collection/goods")
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        return this.memberCollectionGoodsManager.list(pageNo, pageSize);
    }


    @Operation(summary = "添加会员商品收藏")
    @PostMapping("/collection/goods")
    @Parameter(name = "goods_id", description = "商品id", required = true,  in = ParameterIn.QUERY)
    public MemberCollectionGoods add(@NotNull(message = "商品id不能为空") @Parameter(hidden = true) Long goodsId) {
        MemberCollectionGoods memberCollectionGoods = new MemberCollectionGoods();
        memberCollectionGoods.setGoodsId(goodsId);
        return this.memberCollectionGoodsManager.add(memberCollectionGoods);
    }

    @DeleteMapping(value = "/collection/goods/{goods_id}")
    @Operation(summary = "删除会员商品收藏")
    @Parameters({
            @Parameter(name = "goods_id", description= "商品id", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable("goods_id") Long goodsId) {
        this.memberCollectionGoodsManager.delete(goodsId);
        return "";
    }

    @GetMapping(value = "/collection/goods/{id}")
    @Operation(summary = "查询会员是否收藏商品")
    @Parameters({
            @Parameter(name = "id", description = "商品id", required = true,  in = ParameterIn.PATH)
    })
    public SuccessMessage isCollection(@PathVariable Long id) {
        return new SuccessMessage(this.memberCollectionGoodsManager.isCollection(id));
    }


}

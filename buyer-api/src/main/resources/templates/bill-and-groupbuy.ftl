<dev>
    开始时间：<input type="text" id="billStartTime" name="billStartTime" value="">时间格式:2021-01-01 00:00:00<br>
    结束时间：<input type="text" id="billEndTime" name="billEndTime" value="">时间格式:2021-01-31 23:59:59<br>
    <input type="button" value="生成结算单" id="bill"/> <br>
</dev>

<dev>
     活动ID：<input type="text" id="Active" name="Active" value=""><br>
    <input type="button" value="删除团购活动" id="deleteGroupbuyActive"/><br>
    <input type="button" value="删除限时抢购活动" id="deleteSeckillActive"/><br>
</dev>

<script>

    $(function () {
        $("#bill").click(function () {
            var startTime = $("#billStartTime").val();
            var endTime = $("#billEndTime").val();

            $.ajax({
                url: "bills",
                type: "get",
                dataType: 'json',
                data: {start_time: startTime, end_time: endTime},
                success: function (result) {
                    if ("ok" == result) {
                        alert("结算单生成成功")
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    /*错误信息处理*/
                    console.log("进入error---");
                    console.log("状态码：" + xhr.status);
                    console.log("状态:" + xhr.readyState);//当前状态,0-未初始化，1-正在载入，2-已经载入，3-数据进行交互，4-完成。
                    console.log("错误信息:" + xhr.statusText);
                    console.log("返回响应信息：" + xhr.responseText);//这里是详细的信息
                    console.log("请求状态：" + textStatus);
                    console.log(errorThrown);
                    console.log("请求失败");
                    alert("返回响应信息：" + xhr.responseText);
                }
            })
        });
        $("#deleteGroupbuyActive").click(function () {
            var act_id = $("#Active").val();
            console.log("act_id", act_id);
            $.ajax({
                url: "group/" + act_id,
                type: "delete",
                dataType: 'json',
                success: function (result) {
                    if ("ok" == result) {
                        alert("删除活动成功")
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    /*错误信息处理*/
                    console.log("状态:" + xhr.readyState);//当前状态,0-未初始化，1-正在载入，2-已经载入，3-数据进行交互，4-完成。
                    alert("返回响应信息：" + xhr.responseText);
                }
            })
        });

        $("#deleteSeckillActive").click(function () {
            var act_id = $("#Active").val();
            console.log("act_id", act_id);
            $.ajax({
                url: "seckill/" + act_id,
                type: "delete",
                dataType: 'json',
                success: function (result) {
                    if ("ok" == result) {
                        alert("删除活动成功")
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    /*错误信息处理*/
                    console.log("状态:" + xhr.readyState);//当前状态,0-未初始化，1-正在载入，2-已经载入，3-数据进行交互，4-完成。
                    alert("返回响应信息：" + xhr.responseText);
                }
            })
        });
    })
</script>
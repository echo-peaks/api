/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author fk
 * @version v2.0
 * @Description: 微信配置参数
 * @date 2018/4/2410:25
 * @since v7.0.0
 */
@Configuration
public class ShopTntWeixinPcConfig {

    @Value("${shoptnt.weixin.pc.mchid:''}")
    private String mchid;

    @Value("${shoptnt.weixin.pc.appid:''}")
    private String appid;

    @Value("${shoptnt.weixin.pc.key:''}")
    private String key;

    @Value("${shoptnt.weixin.pc.app-secret:''}")
    private String appSecret;

    public String getMchid() {
        return mchid;
    }

    public String getAppid() {
        return appid;
    }

    public String getKey() {
        return key;
    }

    public String getAppSecret() {
        return appSecret;
    }
}

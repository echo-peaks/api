package cn.shoptnt.api;

import cn.shoptnt.framework.openapi.GlobalOperationCustomizer;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * OpenAPI3.0配置
 *
 * @author gy
 * @version 1.0
 * @sinc 7.3.0
 * @date 2022年12月08日 17:04
 * Knife4j 文档访问地址为：http://ip:port/doc.html  推荐使用
 * swagger3 访问地址为：  http://ip:port/swagger-ui/index.html
 */
@Configuration
public class BaseOpenApiConfig {


    private static final String PACKAGES_PATH = "cn.shoptnt.api.base";


    @Bean
    public GroupedOpenApi defaultApi() {
        return GroupedOpenApi.builder()
                .group("默认分组")
                .pathsToMatch("/**")
                .packagesToScan("cn.shoptnt.api")
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

    @Bean
    public GroupedOpenApi baseBaseApi() {
        return GroupedOpenApi.builder()
                .group("公共服务")
                .pathsToMatch("/**")
                .packagesToScan(PACKAGES_PATH)
                //全局操作定制器
                .addOperationCustomizer(new GlobalOperationCustomizer())
                .build();
    }

}

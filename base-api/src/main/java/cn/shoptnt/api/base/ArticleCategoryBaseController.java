/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import cn.shoptnt.model.pagedata.vo.ArticleCategoryVO;
import cn.shoptnt.service.pagedata.ArticleCategoryManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * 文章分类控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-11 15:01:32
 */
@RestController
@RequestMapping("/base/pages/article-categories")
@Tag(name = "文章分类相关API")
public class ArticleCategoryBaseController {

    @Autowired
    private ArticleCategoryManager articleCategoryManager;


    @Operation(summary = "查询分类及以下文章")
    @Parameters({
            @Parameter(name = "category_type", description ="分类类型：帮助中心，商城公告，固定位置，商城促销，其他", required = true,   in = ParameterIn.QUERY),
    })
    @GetMapping
    public ArticleCategoryVO getArticle(@Parameter String categoryType) {

        return this.articleCategoryManager.getCategoryAndArticle(categoryType);
    }

}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.shoptnt.service.base.service.CaptchaManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
 * 验证码生成
 *
 * @author zh
 * @version v7.0
 * @since v7.0
 * 2018年3月23日 上午10:12:12
 */
@RestController
@RequestMapping("/base/captchas")
@Tag(name = "验证码api")
public class CaptchaBaseController {

    @Autowired
    private CaptchaManager captchaManager;

    @GetMapping(value = "/{uuid}/{scene}")
    @Operation(summary = "生成验证码")
    @Parameters({
            @Parameter(name = "uuid", description = "uuid客户端的唯一标识", required = true, in = ParameterIn.PATH),
            @Parameter(name = "scene", description = "业务类型", required = true, in = ParameterIn.PATH)
    })
    public String getCode(@PathVariable("uuid") String uuid, @PathVariable("scene") String scene) {

        //直接调取业务类，由业务类输出流到浏览器
        this.captchaManager.writeCode(uuid, scene);

        return null;
    }
}

///*
// * SHOPTNT 版权所有。
// * 未经许可，您不得使用此文件。
// * 官方地址：www.shoptnt.cn
// */
//package cn.shoptnt.api.base;
//
//import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
//import cn.shoptnt.framework.security.sign.SignUpdateChainWrapper;
//import cn.shoptnt.mapper.User;
//import cn.shoptnt.mapper.UserMapper;
//import cn.shoptnt.service.security.SignScanManager;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * @author 妙贤
// * @version 1.0
// * @since 7.1.0
// * 2020/7/3
// */
//@RestController
//@RequestMapping("/test/mybatis")
//public class MyBatisTestCotroller {
//
//    @Autowired
//    UserMapper userMapper;
//
//    @Autowired
//    SignScanManager signScanManager;
////
////    @GetMapping("/scan")
////    public String scan() {
////        signScanManager.scan();
////        return "ok";
////    }
////
//    @GetMapping("/sign")
//    public String sign() {
//        signScanManager.reSign();
//        return "ok";
//    }
//
//    @GetMapping("/add")
//    public String add() {
//        User user = new User();
//        user.setAge(12);
//        user.setEmail("123@163.com");
//        user.setName("妙贤");
//        userMapper.insert(user);
//
////        IPage page = userMapper.selectPage();
//
//        return ""+ user.getId();
//    }
//
//
//    @GetMapping("/update")
//    public String update(Long id) {
//        User user = new User();
//        user.setId(id);
//        user.setAge(11);
//
//        user.setEmail("妙贤@163.com");
//        user.setName("妙贤");
//        userMapper.updateById(user);
//
//        return ""+ user.getId();
//    }
//
//    @GetMapping("/update1")
//    public String update1() {
//        User user = new User();
//        user.setAge(8);
//        user.setEmail("123@163.com");
//        UpdateWrapper updateWrapper = new UpdateWrapper();
//        updateWrapper.ge("age", "10");
//
//        userMapper.update(user, updateWrapper);
//
//        return ""+ user.getId();
//    }
//
//
//    @GetMapping("/update2")
//    public String update2() {
//        SignUpdateChainWrapper<User> up = new SignUpdateChainWrapper(userMapper,User.class);
//        up.set("age", 100);
//        up.eq("id", 1461985975155576834L);
//        up.update();
//
//
//        return "ok";
//    }
//
//
//    @GetMapping("/get")
//    public User get(Long id) {
//
//        return userMapper.selectById(id);
//    }
//
//
//    public static void main(String[] args) {
////        User user = new User();
//////        user.setAge(8);
////        user.setEmail("123@163.com");
//
//        UpdateWrapper<User> updateWrapper = new UpdateWrapper();
//        updateWrapper.set("age", 10);
//        User user = updateWrapper.getEntity();
//        
//        
//
//    }
//}

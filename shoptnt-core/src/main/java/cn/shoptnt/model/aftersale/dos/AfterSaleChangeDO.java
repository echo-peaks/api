/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.dos;

import com.baomidou.mybatisplus.annotation.*;
import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * 售后收货地址信息实体
 * 主要用于换货和补发，退货退款对应的是订单收货地址信息
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-15
 */
@TableName(value = "es_as_change")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Secret
public class AfterSaleChangeDO implements Serializable {

    private static final long serialVersionUID = -8031221768812955983L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long id;
    /**
     * 售后服务单号
     */
    @Schema(name = "service_sn", description =  "售后服务单号")
    private String serviceSn;
    /**
     * 收货人姓名
     */
    @Schema(name = "ship_name", description =  "收货人姓名")
    private String shipName;
    /**
     * 收货地址省份ID
     */
    @Schema(name = "province_id", description =  "收货地址省份ID", hidden = true)
    private Long provinceId;
    /**
     * 收货地址城市ID
     */
    @Schema(name = "city_id", description =  "收货地址城市ID", hidden = true)
    private Long cityId;
    /**
     * 收货地址县(区)ID
     */
    @Schema(name = "county_id", description =  "收货地址县(区)ID", hidden = true)
    private Long countyId;
    /**
     * 收货地址乡(镇)ID
     */
    @Schema(name = "town_id", description =  "收货地址乡(镇)ID", hidden = true)
    private Long townId;
    /**
     * 收货地址省份名称
     */
    @Schema(name = "province", description =  "收货地址省份名称", hidden = true)
    private String province;
    /**
     * 收货地址城市名称
     */
    @Schema(name = "city", description =  "收货地址城市名称", hidden = true)
    private String city;
    /**
     * 收货地址县(区)名称
     */
    @Schema(name = "county", description =  "收货地址县(区)名称", hidden = true)
    private String county;
    /**
     * 收货地址城镇名称
     */
    @Schema(name = "town", description =  "收货地址城镇名称", hidden = true)
    private String town;
    /**
     * 收货地址详细
     */
    @Schema(name = "ship_addr", description =  "收货地址详细")
    private String shipAddr;
    /**
     * 收货人手机号
     */
    @Schema(name = "ship_mobile", description =  "收货人手机号")
    @SecretField(SecretType.MOBILE)
    @TableField(updateStrategy = FieldStrategy.NOT_EMPTY)
    private String shipMobile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceSn() {
        return serviceSn;
    }

    public void setServiceSn(String serviceSn) {
        this.serviceSn = serviceSn;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getCountyId() {
        return countyId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    public Long getTownId() {
        return townId;
    }

    public void setTownId(Long townId) {
        this.townId = townId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getShipAddr() {
        return shipAddr;
    }

    public void setShipAddr(String shipAddr) {
        this.shipAddr = shipAddr;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public AfterSaleChangeDO () {

    }

    public AfterSaleChangeDO (OrderDO order) {
        this.setProvinceId(order.getShipProvinceId());
        this.setCityId(order.getShipCityId());
        this.setCountyId(order.getShipCountyId());
        this.setTownId(order.getShipTownId());
        this.setProvince(order.getShipProvince());
        this.setCity(order.getShipCity());
        this.setCounty(order.getShipCounty());
        this.setTown(order.getShipTown());
        this.setShipName(order.getShipName());
        this.setShipMobile(order.getShipMobile());
        this.setShipAddr(order.getShipAddr());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AfterSaleChangeDO that = (AfterSaleChangeDO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(serviceSn, that.serviceSn) &&
                Objects.equals(shipName, that.shipName) &&
                Objects.equals(provinceId, that.provinceId) &&
                Objects.equals(cityId, that.cityId) &&
                Objects.equals(countyId, that.countyId) &&
                Objects.equals(townId, that.townId) &&
                Objects.equals(province, that.province) &&
                Objects.equals(city, that.city) &&
                Objects.equals(county, that.county) &&
                Objects.equals(town, that.town) &&
                Objects.equals(shipAddr, that.shipAddr) &&
                Objects.equals(shipMobile, that.shipMobile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, serviceSn, shipName, provinceId, cityId, countyId, townId, province, city, county, town, shipAddr, shipMobile);
    }

    @Override
    public String toString() {
        return "AfterSaleChangeDO{" +
                "id=" + id +
                ", serviceSn='" + serviceSn + '\'' +
                ", shipName='" + shipName + '\'' +
                ", provinceId=" + provinceId +
                ", cityId=" + cityId +
                ", countyId=" + countyId +
                ", townId=" + townId +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", town='" + town + '\'' +
                ", shipAddr='" + shipAddr + '\'' +
                ", shipMobile='" + shipMobile + '\'' +
                '}';
    }
}

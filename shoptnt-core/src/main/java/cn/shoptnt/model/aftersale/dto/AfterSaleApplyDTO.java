/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.dto;

import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.dos.OrderItemsDO;
import cn.shoptnt.framework.security.model.Buyer;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * 申请售后所需要的相关数据DTO
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-12-03
 */
public class AfterSaleApplyDTO implements Serializable {

    private static final long serialVersionUID = 2912409102735600572L;

    @Schema(description =  "当前登录的会员信息")
    private Buyer buyer;

    @Schema(description =  "申请售后的订单信息")
    private OrderDO orderDO;

    @Schema(description =  "申请售后的订单商品信息")
    private OrderItemsDO itemsDO;

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    public OrderDO getOrderDO() {
        return orderDO;
    }

    public void setOrderDO(OrderDO orderDO) {
        this.orderDO = orderDO;
    }

    public OrderItemsDO getItemsDO() {
        return itemsDO;
    }

    public void setItemsDO(OrderItemsDO itemsDO) {
        this.itemsDO = itemsDO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AfterSaleApplyDTO that = (AfterSaleApplyDTO) o;
        return Objects.equals(buyer, that.buyer) &&
                Objects.equals(orderDO, that.orderDO) &&
                Objects.equals(itemsDO, that.itemsDO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(buyer, orderDO, itemsDO);
    }

    @Override
    public String toString() {
        return "AfterSaleApplyDTO{" +
                "buyer=" + buyer +
                ", orderDO=" + orderDO +
                ", itemsDO=" + itemsDO +
                '}';
    }
}

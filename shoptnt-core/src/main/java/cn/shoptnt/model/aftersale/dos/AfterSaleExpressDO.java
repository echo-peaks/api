/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * 售后物流信息实体
 * 主要用于退货、换货审核通过后，用户邮寄货品时使用
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-15
 */
@TableName(value = "es_as_express")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AfterSaleExpressDO implements Serializable {

    private static final long serialVersionUID = -3412149499413072796L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long id;
    /**
     * 售后服务单号
     */
    @Schema(name = "service_sn", description =  "售后服务单号")
    private String serviceSn;
    /**
     * 物流单号
     */
    @Schema(name = "courier_number", description =  "物流单号")
    private String courierNumber;
    /**
     * 物流公司id
     */
    @Schema(name = "courier_company_id", description =  "物流公司id")
    private Long courierCompanyId;
    /**
     * 物流公司名称
     */
    @Schema(name = "courier_company", description =  "物流公司名称")
    private String courierCompany;
    /**
     * 发货时间
     */
    @Schema(name = "ship_time", description =  "发货时间")
    private Long shipTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceSn() {
        return serviceSn;
    }

    public void setServiceSn(String serviceSn) {
        this.serviceSn = serviceSn;
    }

    public String getCourierNumber() {
        return courierNumber;
    }

    public void setCourierNumber(String courierNumber) {
        this.courierNumber = courierNumber;
    }

    public Long getCourierCompanyId() {
        return courierCompanyId;
    }

    public void setCourierCompanyId(Long courierCompanyId) {
        this.courierCompanyId = courierCompanyId;
    }

    public String getCourierCompany() {
        return courierCompany;
    }

    public void setCourierCompany(String courierCompany) {
        this.courierCompany = courierCompany;
    }

    public Long getShipTime() {
        return shipTime;
    }

    public void setShipTime(Long shipTime) {
        this.shipTime = shipTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AfterSaleExpressDO that = (AfterSaleExpressDO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(serviceSn, that.serviceSn) &&
                Objects.equals(courierNumber, that.courierNumber) &&
                Objects.equals(courierCompanyId, that.courierCompanyId) &&
                Objects.equals(courierCompany, that.courierCompany) &&
                Objects.equals(shipTime, that.shipTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, serviceSn, courierNumber, courierCompanyId, courierCompany, shipTime);
    }

    @Override
    public String toString() {
        return "AfterSaleExpressDO{" +
                "id=" + id +
                ", serviceSn='" + serviceSn + '\'' +
                ", courierNumber='" + courierNumber + '\'' +
                ", courierCompanyId=" + courierCompanyId +
                ", courierCompany='" + courierCompany + '\'' +
                ", shipTime=" + shipTime +
                '}';
    }
}

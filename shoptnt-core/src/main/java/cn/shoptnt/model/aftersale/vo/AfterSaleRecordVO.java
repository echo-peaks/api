/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.vo;

import cn.shoptnt.model.aftersale.dos.AfterSaleServiceDO;
import cn.shoptnt.model.aftersale.enums.ServiceStatusEnum;
import cn.shoptnt.model.aftersale.enums.ServiceTypeEnum;
import cn.shoptnt.model.aftersale.dto.ServiceOperateAllowable;
import cn.shoptnt.framework.util.JsonUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;

/**
 * 申请售后服务记录VO
 * 用于各端售后服务列表查询
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-17
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AfterSaleRecordVO implements Serializable {

    private static final long serialVersionUID = 2406372635349255098L;

    /**
     * 售后服务单号
     */
    @Schema(name = "service_sn", description = "售后服务单号")
    private String serviceSn;
    /**
     * 订单编号
     */
    @Schema(name = "order_sn", description = "订单编号")
    private String orderSn;
    /**
     * 申请售后服务会员ID
     */
    @Schema(name = "member_id", description = "申请售后服务会员ID")
    private Long memberId;
    /**
     * 申请售后服务会员名称
     */
    @Schema(name = "member_name", description = "申请售后服务会员名称")
    private String memberName;
    /**
     * 商家ID
     */
    @Schema(name = "seller_id", description = "商家ID")
    private Long sellerId;
    /**
     * 商家名称
     */
    @Schema(name = "seller_name", description = "商家名称")
    private String sellerName;
    /**
     * 申请时间
     */
    @Schema(name="create_time",description = "申请时间")
    private Long createTime;
    /**
     * 售后类型 RETURN_GOODS：退货，CHANGE_GOODS：换货，SUPPLY_AGAIN_GOODS：补发货品，ORDER_CANCEL：取消订单（订单确认付款且未收货之前）
     */
    @Schema(name = "service_type", description = "售后类型 RETURN_GOODS：退货，CHANGE_GOODS：换货，SUPPLY_AGAIN_GOODS：补发货品，ORDER_CANCEL：取消订单（订单确认付款且未收货之前）")
    private String serviceType;
    /**
     * 售后单状态 APPLY：申请，PASS：审核通过，REFUSE：审核拒绝，WAIT_FOR_MANUAL：待人工处理，STOCK_IN：入库，REFUNDING：退款中，REFUNDFAIL：退款失败，COMPLETE：完成
     */
    @Schema(name = "service_status", description = "售后单状态 APPLY：申请，PASS：审核通过，REFUSE：审核拒绝，WAIT_FOR_MANUAL：待人工处理，STOCK_IN：入库，REFUNDING：退款中，REFUNDFAIL：退款失败，COMPLETE：完成")
    private String serviceStatus;
    /**
     * 售后类型 退货，换货，补发货品，取消订单
     */
    @Schema(name = "service_type_text", description = "售后类型 退货，换货，补发货品，取消订单")
    private String serviceTypeText;
    /**
     * 售后单状态
     */
    @Schema(name = "service_status_text", description = "售后单状态")
    private String serviceStatusText;
    /**
     * 售后商品信息集合
     */
    @Schema(name="goodsList", description = "售后商品信息集合")
    private List<AfterSaleGoodsVO> goodsList;
    /**
     * 售后服务允许操作信息
     */
    @Schema(name="allowable", description = "售后服务允许操作信息")
    private ServiceOperateAllowable allowable;

    public String getServiceSn() {
        return serviceSn;
    }

    public void setServiceSn(String serviceSn) {
        this.serviceSn = serviceSn;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getServiceTypeText() {
        return serviceTypeText;
    }

    public void setServiceTypeText(String serviceTypeText) {
        this.serviceTypeText = serviceTypeText;
    }

    public String getServiceStatusText() {
        return serviceStatusText;
    }

    public void setServiceStatusText(String serviceStatusText) {
        this.serviceStatusText = serviceStatusText;
    }

    public List<AfterSaleGoodsVO> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<AfterSaleGoodsVO> goodsList) {
        this.goodsList = goodsList;
    }

    public ServiceOperateAllowable getAllowable() {
        return allowable;
    }

    public void setAllowable(ServiceOperateAllowable allowable) {
        this.allowable = allowable;
    }

    public AfterSaleRecordVO(AfterSaleServiceDO serviceDO) {
        this.serviceSn = serviceDO.getSn();
        this.orderSn = serviceDO.getOrderSn();
        this.memberId = serviceDO.getMemberId();
        this.memberName = serviceDO.getMemberName();
        this.sellerId = serviceDO.getSellerId();
        this.sellerName = serviceDO.getSellerName();
        this.createTime = serviceDO.getCreateTime();
        this.serviceType = serviceDO.getServiceType();
        this.serviceStatus = serviceDO.getServiceStatus();
        this.serviceTypeText = ServiceTypeEnum.valueOf(serviceDO.getServiceType()).description();
        this.serviceStatusText = ServiceStatusEnum.valueOf(serviceDO.getServiceStatus()).description();
        this.goodsList = JsonUtil.jsonToList(serviceDO.getGoodsJson(), AfterSaleGoodsVO.class);
    }

    @Override
    public String toString() {
        return "AfterSaleRecordVO{" +
                "serviceSn='" + serviceSn + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", sellerId=" + sellerId +
                ", sellerName='" + sellerName + '\'' +
                ", createTime=" + createTime +
                ", serviceType='" + serviceType + '\'' +
                ", serviceStatus='" + serviceStatus + '\'' +
                ", serviceTypeText='" + serviceTypeText + '\'' +
                ", serviceStatusText='" + serviceStatusText + '\'' +
                ", goodsList=" + goodsList +
                ", allowable=" + allowable +
                '}';
    }
}

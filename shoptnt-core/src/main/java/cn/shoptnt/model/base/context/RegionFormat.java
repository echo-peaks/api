/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.context;

import java.lang.annotation.*;

/**
 * Created by 妙贤 on 2018/5/2.
 * 地区注解
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/5/2
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
public @interface RegionFormat {

}

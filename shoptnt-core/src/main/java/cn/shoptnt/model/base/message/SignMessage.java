package cn.shoptnt.model.base.message;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;
import cn.shoptnt.model.member.dos.AskReplyDO;
import cn.shoptnt.model.member.dos.MemberAsk;
import cn.shoptnt.model.promotion.sign.dos.SignInRecord;

import java.io.Serializable;
import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignMessage
 * @description <TODO description class>
 * @program: api
 * 2024/3/19 10:32
 */
public class SignMessage implements Serializable, DirectMessage {

    private List<SignInRecord> signInRecords ;

    public SignMessage() {

    }

    public SignMessage(List<SignInRecord> signInRecords) {
        this.signInRecords = signInRecords;
    }
    @Override
    public String getExchange() {
        return  AmqpExchange.MEMBER_SIGN;
    }


    public List<SignInRecord> getSignInRecords() {
        return signInRecords;
    }

    public void setSignInRecords(List<SignInRecord> signInRecords) {
        this.signInRecords = signInRecords;
    }
}

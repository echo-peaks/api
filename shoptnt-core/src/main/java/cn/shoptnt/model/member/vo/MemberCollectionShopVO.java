/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.vo;

import cn.shoptnt.model.goods.vo.GoodsVO;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;
import java.util.List;


/**
 * 会员收藏店铺表实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-30 20:34:23
 */

public class MemberCollectionShopVO {
    /**
     * 收藏id
     */
    private Long id;
    /**
     * 会员id
     */
    @Schema(name = "member_id", description =  "会员id")
    private Long memberId;
    /**
     * 店铺id
     */
    @Min(message = "必须为数字", value =  0)
    @Schema(name = "shop_id", description =  "店铺id")
    private Long shopId;
    /**
     * 店铺名称
     */
    @Schema(name = "shop_name", description =  "店铺名称", required = true)
    private String shopName;
    /**
     * 收藏时间
     */
    @Schema(name = "create_time", description =  "收藏时间", required = true)
    private Long createTime;
    /**
     * 店铺logo
     */
    @Schema(name = "logo", description =  "店铺logo")
    private String logo;
    /**
     * 店铺所在省
     */
    @Schema(name = "shop_province", description =  "店铺所在省")
    private String shopProvince;
    /**
     * 店铺所在市
     */
    @Schema(name = "shop_city", description =  "店铺所在市")
    private String shopCity;
    /**
     * 店铺所在县
     */
    @Schema(name = "shop_region", description =  "店铺所在县")
    private String shopRegion;
    /**
     * 店铺所在镇
     */
    @Schema(name = "shop_town", description =  "店铺所在镇")
    private String shopTown;

    /**
     * 店铺好评率
     */
    @Schema(name = "shop_praise_rate", description =  "店铺好评率")
    private Double shopPraiseRate;
    /**
     * 店铺描述相符度
     */
    @Schema(name = "shop_description_credit", description =  "店铺描述相符度")
    private Double shopDescriptionCredit;
    /**
     * 服务态度分数
     */
    @Schema(name = "shop_service_credit", description =  "服务态度分数")
    private Double shopServiceCredit;
    /**
     * 发货速度分数
     */
    @Schema(name = "shop_delivery_credit", description =  "发货速度分数")
    private Double shopDeliveryCredit;
    /**
     * 店铺所在镇
     */
    private List<GoodsVO> goodsList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getShopProvince() {
        return shopProvince;
    }

    public void setShopProvince(String shopProvince) {
        this.shopProvince = shopProvince;
    }

    public String getShopCity() {
        return shopCity;
    }

    public void setShopCity(String shopCity) {
        this.shopCity = shopCity;
    }

    public String getShopRegion() {
        return shopRegion;
    }

    public void setShopRegion(String shopRegion) {
        this.shopRegion = shopRegion;
    }

    public String getShopTown() {
        return shopTown;
    }

    public void setShopTown(String shopTown) {
        this.shopTown = shopTown;
    }

    public Double getShopPraiseRate() {
        return shopPraiseRate;
    }

    public void setShopPraiseRate(Double shopPraiseRate) {
        this.shopPraiseRate = shopPraiseRate;
    }

    public Double getShopDescriptionCredit() {
        return shopDescriptionCredit;
    }

    public void setShopDescriptionCredit(Double shopDescriptionCredit) {
        this.shopDescriptionCredit = shopDescriptionCredit;
    }

    public Double getShopServiceCredit() {
        return shopServiceCredit;
    }

    public void setShopServiceCredit(Double shopServiceCredit) {
        this.shopServiceCredit = shopServiceCredit;
    }

    public Double getShopDeliveryCredit() {
        return shopDeliveryCredit;
    }

    public void setShopDeliveryCredit(Double shopDeliveryCredit) {
        this.shopDeliveryCredit = shopDeliveryCredit;
    }

    public List<GoodsVO> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<GoodsVO> goodsList) {
        this.goodsList = goodsList;
    }

    @Override
    public String toString() {
        return "MemberCollectionShopVO{" +
                "id=" + id +
                ", memberId=" + memberId +
                ", shopId=" + shopId +
                ", shopName='" + shopName + '\'' +
                ", createTime=" + createTime +
                ", logo='" + logo + '\'' +
                ", shopProvince='" + shopProvince + '\'' +
                ", shopCity='" + shopCity + '\'' +
                ", shopRegion='" + shopRegion + '\'' +
                ", shopTown='" + shopTown + '\'' +
                ", shopPraiseRate=" + shopPraiseRate +
                ", shopDescriptionCredit=" + shopDescriptionCredit +
                ", shopServiceCredit=" + shopServiceCredit +
                ", shopDeliveryCredit=" + shopDeliveryCredit +
                ", goodsList=" + goodsList +
                '}';
    }
}

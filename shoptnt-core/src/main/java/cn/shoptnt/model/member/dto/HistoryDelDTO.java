/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 会员足迹删除param
 *
 * @author zh
 * @version v7.1.4
 * @since vv7.1
 * 2019-06-18 15:18:56
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class HistoryDelDTO {

    /**
     * 商品足迹id
     */
    @Schema(name = "id",description =  "商品足迹id")
    private Long id;
    /**
     * 日期时间戳
     */
    @Schema(name = "date",description =  "日期时间戳", required = true)
    private String date;

    @Schema(name = "member_id",description =  "会员id", required = true)
    private Long memberId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    @Override
    public String toString() {
        return "HistoryDelDTO{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", memberId=" + memberId +
                '}';
    }
}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * 会员增票资质实体
 *
 * @author duanmingyu
 * @version v7.1.4
 * @since v7.0.0
 * 2019-06-18
 */
@TableName(value = "es_member_zpzz")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Secret
public class MemberZpzzDO implements Serializable {

    private static final long serialVersionUID = 4727610826309392575L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;

    /**
     * 会员ID
     */
    @Schema(name = "member_id", description =  "会员ID")
    private Long memberId;

    /**
     * 会员登陆用户名
     */
    @Schema(name = "uname", description =  "会员登陆用户名")
    private String uname;

    /**
     * 状态
     */
    @Schema(name = "status", description =  "状态", example = "NEW_APPLY：新申请，AUDIT_PASS：审核通过，AUDIT_REFUSE：审核未通过")
    private String status;

    /**
     * 单位名称
     */
    @Schema(name = "company_name", description =  "单位名称")
    private String companyName;

    /**
     * 纳税人识别码
     */
    @Schema(name = "taxpayer_code", description =  "纳税人识别码")
    private String taxpayerCode;

    /**
     * 公司注册地址
     */
    @Schema(name = "register_address", description =  "公司注册地址")
    private String registerAddress;

    /**
     * 公司注册电话
     */
    @Schema(name = "register_tel", description =  "公司注册电话")
    @SecretField(SecretType.MOBILE)
    private String registerTel;

    /**
     * 开户银行
     */
    @Schema(name = "bank_name", description =  "开户银行")
    private String bankName;

    /**
     * 银行账户
     */
    @Schema(name = "bank_account", description =  "银行账户")
    @SecretField(SecretType.BANK)
    private String bankAccount;

    /**
     * 平台审核备注
     */
    @Schema(name = "audit_remark", description =  "平台审核备注")
    private String auditRemark;

    /**
     * 申请时间
     */
    @Schema(name = "apply_time", description =  "申请时间")
    private Long applyTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTaxpayerCode() {
        return taxpayerCode;
    }

    public void setTaxpayerCode(String taxpayerCode) {
        this.taxpayerCode = taxpayerCode;
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress;
    }

    public String getRegisterTel() {
        return registerTel;
    }

    public void setRegisterTel(String registerTel) {
        this.registerTel = registerTel;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }

    public Long getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Long applyTime) {
        this.applyTime = applyTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemberZpzzDO that = (MemberZpzzDO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(memberId, that.memberId) &&
                Objects.equals(uname, that.uname) &&
                Objects.equals(status, that.status) &&
                Objects.equals(companyName, that.companyName) &&
                Objects.equals(taxpayerCode, that.taxpayerCode) &&
                Objects.equals(registerAddress, that.registerAddress) &&
                Objects.equals(registerTel, that.registerTel) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(bankAccount, that.bankAccount) &&
                Objects.equals(auditRemark, that.auditRemark) &&
                Objects.equals(applyTime, that.applyTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, memberId, uname, status, companyName, taxpayerCode, registerAddress, registerTel, bankName, bankAccount, auditRemark, applyTime);
    }

    @Override
    public String toString() {
        return "MemberZpzzDO{" +
                "id=" + id +
                ", memberId=" + memberId +
                ", uname='" + uname + '\'' +
                ", status='" + status + '\'' +
                ", companyName='" + companyName + '\'' +
                ", taxpayerCode='" + taxpayerCode + '\'' +
                ", registerAddress='" + registerAddress + '\'' +
                ", registerTel='" + registerTel + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankAccount='" + bankAccount + '\'' +
                ", auditRemark='" + auditRemark + '\'' +
                ", applyTime=" + applyTime +
                '}';
    }
}

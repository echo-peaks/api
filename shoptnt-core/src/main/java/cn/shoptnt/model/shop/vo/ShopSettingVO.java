/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.vo;

import javax.validation.constraints.NotEmpty;

import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 店铺设置VO
 *
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 下午7:58:55
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ShopSettingVO {
    /**
     * 店铺Id
     */
    @Column(name = "shop_id")
    @Schema(name = "shop_id", description =  "店铺Id")
    private Long shopId;
    /**
     * 店铺所在省id
     */
    @Column(name = "shop_province_id")
    @Schema(name = "shop_province_id", description =  "店铺所在省id", hidden = true)
    private Integer shopProvinceId;
    /**
     * 店铺所在市id
     */
    @Column(name = "shop_city_id")
    @Schema(name = "shop_city_id", description =  "店铺所在市id", hidden = true)
    private Integer shopCityId;
    /**
     * 店铺所在县id
     */
    @Column(name = "shop_county_id")
    @Schema(name = "shop_county_id", description =  "店铺所在县id", hidden = true)
    private Integer shopCountyId;
    /**
     * 店铺所在镇id
     */
    @Column(name = "shop_town_id")
    @Schema(name = "shop_town_id", description =  "店铺所在镇id", hidden = true)
    private Integer shopTownId;
    /**
     * 店铺所在省
     */
    @Column(name = "shop_province")
    @Schema(name = "shop_province", description =  "店铺所在省", hidden = true)
    private String shopProvince;
    /**
     * 店铺所在市
     */
    @Column(name = "shop_city")
    @Schema(name = "shop_city", description =  "店铺所在市", hidden = true)
    private String shopCity;
    /**
     * 店铺所在县
     */
    @Column(name = "shop_county")
    @Schema(name = "shop_county", description =  "店铺所在县", hidden = true)
    private String shopCounty;
    /**
     * 店铺所在镇
     */
    @Column(name = "shop_town", allowNullUpdate = true)
    @Schema(name = "shop_town", description =  "店铺所在镇", hidden = true)
    private String shopTown;
    /**
     * 店铺详细地址
     */
    @Column(name = "shop_add")
    @Schema(name = "shop_add", description =  "店铺详细地址", required = true)
    private String shopAdd;
    /**
     * 联系人电话
     */
    @Column(name = "link_phone")
    @Schema(name = "link_phone", description =  "联系人电话", required = true)
    @NotEmpty(message = "联系人电话必填")
    private String linkPhone;
    /**
     * 店铺logo
     */
    @Column(name = "shop_logo")
    @Schema(name = "shop_logo", description =  "店铺logo")
    private String shopLogo;
    /**
     * 店铺横幅
     */
    @Column(name = "shop_banner")
    @Schema(name = "shop_banner", description =  "店铺横幅")
    private String shopBanner;
    /**
     * 店铺简介
     */
    @Column(name = "shop_desc")
    @Schema(name = "shop_desc", description =  "店铺简介")
    private String shopDesc;
    /**
     * 店铺客服qq
     */
    @Column(name = "shop_qq")
    @Schema(name = "shop_qq", description =  "店铺客服qq")
    private String shopQq;

    /**
     * 店铺名称
     */
    @Column(name = "shop_name")
    @Schema(name = "shop_name", description =  "店铺名称")
    private String shopName;

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Integer getShopProvinceId() {
        return shopProvinceId;
    }

    public void setShopProvinceId(Integer shopProvinceId) {
        this.shopProvinceId = shopProvinceId;
    }

    public Integer getShopCityId() {
        return shopCityId;
    }

    public void setShopCityId(Integer shopCityId) {
        this.shopCityId = shopCityId;
    }

    public Integer getShopCountyId() {
        return shopCountyId;
    }

    public void setShopCountyId(Integer shopCountyId) {
        this.shopCountyId = shopCountyId;
    }

    public Integer getShopTownId() {
        return shopTownId;
    }

    public void setShopTownId(Integer shopTownId) {
        this.shopTownId = shopTownId;
    }

    public String getShopProvince() {
        return shopProvince;
    }

    public void setShopProvince(String shopProvince) {
        this.shopProvince = shopProvince;
    }

    public String getShopCity() {
        return shopCity;
    }

    public void setShopCity(String shopCity) {
        this.shopCity = shopCity;
    }

    public String getShopCounty() {
        return shopCounty;
    }

    public void setShopCounty(String shopCounty) {
        this.shopCounty = shopCounty;
    }

    public String getShopTown() {
        return shopTown;
    }

    public void setShopTown(String shopTown) {
        this.shopTown = shopTown;
    }

    public String getShopAdd() {
        return shopAdd;
    }

    public void setShopAdd(String shopAdd) {
        this.shopAdd = shopAdd;
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    public String getShopLogo() {
        return shopLogo;
    }

    public void setShopLogo(String shopLogo) {
        this.shopLogo = shopLogo;
    }

    public String getShopBanner() {
        return shopBanner;
    }

    public void setShopBanner(String shopBanner) {
        this.shopBanner = shopBanner;
    }

    public String getShopDesc() {
        return shopDesc;
    }

    public void setShopDesc(String shopDesc) {
        this.shopDesc = shopDesc;
    }

    public String getShopQq() {
        return shopQq;
    }

    public void setShopQq(String shopQq) {
        this.shopQq = shopQq;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @Override
    public String toString() {
        return "ShopSettingVO [shopId=" + shopId + ", shopProvinceId=" + shopProvinceId + ", shopCityId=" + shopCityId
                + ", shopCountyId=" + shopCountyId + ", shopTownId=" + shopTownId + ", shopProvince=" + shopProvince
                + ", shopCity=" + shopCity + ", shopCounty=" + shopCounty + ", shopTown=" + shopTown + ", shopAdd="
                + shopAdd + ", linkPhone=" + linkPhone + ", shopLogo=" + shopLogo + ", shopBanner=" + shopBanner
                + ", shopDesc=" + shopDesc + ", shopQq=" + shopQq + "]";
    }
}


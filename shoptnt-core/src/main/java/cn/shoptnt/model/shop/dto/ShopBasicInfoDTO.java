/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;

/**
 * @author zjp
 * @version v7.0
 * @Description 店铺基本信息VO
 * @ClassName ShopBasicInfoDTO
 * @since v7.0 下午4:36 2018/6/6
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ShopBasicInfoDTO {
    /**店铺Id*/
    @Schema(name="shop_id",description = "店铺Id")
    private Long shopId;
    /**会员Id*/
    @Schema(name="member_id",description = "会员Id")
    private Long memberId;
    /**会员名称*/
    @Schema(name="member_name",description = "会员名称")
    private String memberName;
    /**店铺名称*/
    @Schema(name="shop_name",description = "店铺名称")
    private String shopName;
    /**店铺状态*/
    @Schema(name="shop_disable",description = "店铺状态")
    private String shopDisable;
    /**店铺创建时间*/
    @Schema(name="shop_createtime",description = "店铺创建时间")
    private Long shopCreatetime;
    /**店铺关闭时间*/
    @Schema(name="shop_endtime",description = "店铺关闭时间")
    private Long shopEndtime;

    /**店铺所在省id*/
    @Schema(name="shop_province_id",description = "店铺所在省id")
    private Long shopProvinceId;
    /**店铺所在市id*/
    @Schema(name="shop_city_id",description = "店铺所在市id")
    private Long shopCityId;
    /**店铺所在县id*/
    @Schema(name="shop_county_id",description = "店铺所在县id")
    private Long shopCountyId;
    /**店铺所在镇id*/
    @Schema(name="shop_town_id",description = "店铺所在镇id")
    private Long shopTownId;
    /**店铺所在省*/
    @Schema(name="shop_province",description = "店铺所在省")
    private String shopProvince;
    /**店铺所在市*/
    @Schema(name="shop_city",description = "店铺所在市")
    private String shopCity;
    /**店铺所在县*/
    @Schema(name="shop_county",description = "店铺所在县")
    private String shopCounty;
    /**店铺所在镇*/
    @Schema(name="shop_town",description = "店铺所在镇")
    private String shopTown;
    /**店铺详细地址*/
    @Schema(name="shop_add",description = "店铺详细地址")
    private String shopAdd;
    /**店铺logo*/
    @Schema(name="shop_logo",description = "店铺logo")
    private String shopLogo;
    /**店铺横幅*/
    @Schema(name="shop_banner",description = "店铺横幅")
    private String shopBanner;
    /**店铺简介*/
    @Schema(name="shop_desc",description = "店铺简介")
    private String shopDesc;
    /**是否推荐*/
    @Schema(name="shop_recommend",description = "是否推荐")
    private Integer shopRecommend;
    /**店铺主题id*/
    @Schema(name="shop_themeid",description = "店铺主题id")
    private Long shopThemeid;
    /**店铺主题模版路径*/
    @Schema(name="shop_theme_path",description="店铺主题模版路径")
    private String shopThemePath;
    /**店铺主题id*/
    @Schema(name="wap_themeid",description="店铺主题id")
    private Long wapThemeid;
    /**wap店铺主题*/
    @Schema(name="wap_theme_path",description="wap店铺主题")
    private String wapThemePath;
    /**店铺信用*/
    @Schema(name="shop_credit",description="店铺信用")
    private Double shopCredit;
    /**店铺好评率*/
    @Schema(name="shop_praise_rate",description="店铺好评率")
    private Double shopPraiseRate;
    /**店铺描述相符度*/
    @Schema(name="shop_description_credit",description="店铺描述相符度")
    private Double shopDescriptionCredit;
    /**服务态度分数*/
    @Schema(name="shop_service_credit",description="服务态度分数")
    private Double shopServiceCredit;
    /**发货速度分数*/
    @Schema(name="shop_delivery_credit",description="发货速度分数")
    private Double shopDeliveryCredit;
    /**店铺收藏数*/
    @Schema(name="shop_collect",description="店铺收藏数")
    private Integer shopCollect;
    /**店铺商品数*/
    @Schema(name="goods_num",description="店铺商品数")
    private Integer goodsNum;
    /**店铺客服qq*/
    @Schema(name="shop_qq",description="店铺客服qq")
    private String shopQq;
    /**是否自营*/
    @Schema(name="self_operated",description="是否自营 1:是 0:否")
    private Integer selfOperated;
    /**联系人电话*/
    @Schema(name="link_phone",description="联系人电话")
    @NotEmpty(message="联系人电话必填")
    private String linkPhone;

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopDisable() {
        return shopDisable;
    }

    public void setShopDisable(String shopDisable) {
        this.shopDisable = shopDisable;
    }

    public Long getShopCreatetime() {
        return shopCreatetime;
    }

    public void setShopCreatetime(Long shopCreatetime) {
        this.shopCreatetime = shopCreatetime;
    }

    public Long getShopEndtime() {
        return shopEndtime;
    }

    public void setShopEndtime(Long shopEndtime) {
        this.shopEndtime = shopEndtime;
    }

    public Long getShopProvinceId() {
        return shopProvinceId;
    }

    public void setShopProvinceId(Long shopProvinceId) {
        this.shopProvinceId = shopProvinceId;
    }

    public Long getShopCityId() {
        return shopCityId;
    }

    public void setShopCityId(Long shopCityId) {
        this.shopCityId = shopCityId;
    }

    public Long getShopCountyId() {
        return shopCountyId;
    }

    public void setShopCountyId(Long shopCountyId) {
        this.shopCountyId = shopCountyId;
    }

    public Long getShopTownId() {
        return shopTownId;
    }

    public void setShopTownId(Long shopTownId) {
        this.shopTownId = shopTownId;
    }

    public String getShopProvince() {
        return shopProvince;
    }

    public void setShopProvince(String shopProvince) {
        this.shopProvince = shopProvince;
    }

    public String getShopCity() {
        return shopCity;
    }

    public void setShopCity(String shopCity) {
        this.shopCity = shopCity;
    }

    public String getShopCounty() {
        return shopCounty;
    }

    public void setShopCounty(String shopCounty) {
        this.shopCounty = shopCounty;
    }

    public String getShopTown() {
        return shopTown;
    }

    public void setShopTown(String shopTown) {
        this.shopTown = shopTown;
    }

    public String getShopAdd() {
        return shopAdd;
    }

    public void setShopAdd(String shopAdd) {
        this.shopAdd = shopAdd;
    }

    public String getShopLogo() {
        return shopLogo;
    }

    public void setShopLogo(String shopLogo) {
        this.shopLogo = shopLogo;
    }

    public String getShopBanner() {
        return shopBanner;
    }

    public void setShopBanner(String shopBanner) {
        this.shopBanner = shopBanner;
    }

    public String getShopDesc() {
        return shopDesc;
    }

    public void setShopDesc(String shopDesc) {
        this.shopDesc = shopDesc;
    }

    public Integer getShopRecommend() {
        return shopRecommend;
    }

    public void setShopRecommend(Integer shopRecommend) {
        this.shopRecommend = shopRecommend;
    }

    public Long getShopThemeid() {
        return shopThemeid;
    }

    public void setShopThemeid(Long shopThemeid) {
        this.shopThemeid = shopThemeid;
    }

    public String getShopThemePath() {
        return shopThemePath;
    }

    public void setShopThemePath(String shopThemePath) {
        this.shopThemePath = shopThemePath;
    }

    public Long getWapThemeid() {
        return wapThemeid;
    }

    public void setWapThemeid(Long wapThemeid) {
        this.wapThemeid = wapThemeid;
    }

    public String getWapThemePath() {
        return wapThemePath;
    }

    public void setWapThemePath(String wapThemePath) {
        this.wapThemePath = wapThemePath;
    }

    public Double getShopCredit() {
        return shopCredit;
    }

    public void setShopCredit(Double shopCredit) {
        this.shopCredit = shopCredit;
    }

    public Double getShopPraiseRate() {
        return shopPraiseRate;
    }

    public void setShopPraiseRate(Double shopPraiseRate) {
        this.shopPraiseRate = shopPraiseRate;
    }

    public Double getShopDescriptionCredit() {
        return shopDescriptionCredit;
    }

    public void setShopDescriptionCredit(Double shopDescriptionCredit) {
        this.shopDescriptionCredit = shopDescriptionCredit;
    }

    public Double getShopServiceCredit() {
        return shopServiceCredit;
    }

    public void setShopServiceCredit(Double shopServiceCredit) {
        this.shopServiceCredit = shopServiceCredit;
    }

    public Double getShopDeliveryCredit() {
        return shopDeliveryCredit;
    }

    public void setShopDeliveryCredit(Double shopDeliveryCredit) {
        this.shopDeliveryCredit = shopDeliveryCredit;
    }

    public Integer getShopCollect() {
        return shopCollect;
    }

    public void setShopCollect(Integer shopCollect) {
        this.shopCollect = shopCollect;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getShopQq() {
        return shopQq;
    }

    public void setShopQq(String shopQq) {
        this.shopQq = shopQq;
    }

    public Integer getSelfOperated() {
        return selfOperated;
    }

    public void setSelfOperated(Integer selfOperated) {
        this.selfOperated = selfOperated;
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    @Override
    public String toString() {
        return "ShopBasicInfoDTO{" +
                "shopId=" + shopId +
                ", memberId=" + memberId +
                ", memberName='" + memberName + '\'' +
                ", shopName='" + shopName + '\'' +
                ", shopDisable='" + shopDisable + '\'' +
                ", shopCreatetime=" + shopCreatetime +
                ", shopEndtime=" + shopEndtime +
                ", shopProvinceId=" + shopProvinceId +
                ", shopCityId=" + shopCityId +
                ", shopCountyId=" + shopCountyId +
                ", shopTownId=" + shopTownId +
                ", shopProvince='" + shopProvince + '\'' +
                ", shopCity='" + shopCity + '\'' +
                ", shopCounty='" + shopCounty + '\'' +
                ", shopTown='" + shopTown + '\'' +
                ", shopAdd='" + shopAdd + '\'' +
                ", shopLogo='" + shopLogo + '\'' +
                ", shopBanner='" + shopBanner + '\'' +
                ", shopDesc='" + shopDesc + '\'' +
                ", shopRecommend=" + shopRecommend +
                ", shopThemeid=" + shopThemeid +
                ", shopThemePath='" + shopThemePath + '\'' +
                ", wapThemeid=" + wapThemeid +
                ", wapThemePath='" + wapThemePath + '\'' +
                ", shopCredit=" + shopCredit +
                ", shopPraiseRate=" + shopPraiseRate +
                ", shopDescriptionCredit=" + shopDescriptionCredit +
                ", shopServiceCredit=" + shopServiceCredit +
                ", shopDeliveryCredit=" + shopDeliveryCredit +
                ", shopCollect=" + shopCollect +
                ", goodsNum=" + goodsNum +
                ", shopQq='" + shopQq + '\'' +
                ", selfOperated=" + selfOperated +
                ", linkPhone='" + linkPhone + '\'' +
                '}';
    }
}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.*;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;

import cn.shoptnt.handler.annotation.Secret;
import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Size;


/**
 * 店铺详细实体
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-21 10:06:38
 */
@TableName(value = "es_shop_detail")
@Schema
@Secret
public class ShopDetailDO implements Serializable {

    private static final long serialVersionUID = 1810092990127648L;

    /**店铺详细id*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long id;
    /**店铺id*/
    @Schema(name="shop_id",description ="店铺id")
    private Long shopId;
    /**店铺所在省id*/
    @Schema(name="shop_province_id",description ="店铺所在省id")
    private Long shopProvinceId;
    /**店铺所在市id*/
    @Schema(name="shop_city_id",description = "店铺所在市id")
    private Long shopCityId;
    /**店铺所在县id*/
    @Schema(name="shop_county_id",description = "店铺所在县id")
    private Long shopCountyId;
    /**店铺所在镇id*/
    @Schema(name="shop_town_id",description = "店铺所在镇id")
    private Long shopTownId;
    /**店铺所在省*/
    @Schema(name="shop_province",description = "店铺所在省")
    private String shopProvince;
    /**店铺所在市*/
    @Schema(name="shop_city",description = "店铺所在市")
    private String shopCity;
    /**店铺所在县*/
    @Schema(name="shop_county",description = "店铺所在县")
    private String shopCounty;
    /**店铺所在镇*/
    @Schema(name="shop_town",description = "店铺所在镇")
    private String shopTown;
    /**店铺详细地址*/
    @Schema(name="shop_add",description = "店铺详细地址")
    private String shopAdd;
    /**公司名称*/
    @Schema(name="company_name",description = "公司名称")
    private String companyName;
    /**公司地址*/
    @Schema(name="company_address",description = "公司地址")
    private String companyAddress;
    /**公司电话*/
    @Schema(name="company_phone",description = "公司电话")
    @SecretField(SecretType.MOBILE)
    private String companyPhone;
    /**电子邮箱*/
    @Schema(name="company_email",description = "电子邮箱")
    private String companyEmail;
    /**员工总数*/
    @Schema(name="employee_num",description = "员工总数")
    private Integer employeeNum;
    /**注册资金*/
    @Schema(name="reg_money",description = "注册资金")
    private Double regMoney;
    /**联系人姓名*/
    @Schema(name="link_name",description = "联系人姓名")
    private String linkName;
    /**联系人电话*/
    @Schema(name="link_phone",description = "联系人电话")
    @SecretField(SecretType.MOBILE)
    private String linkPhone;
    /**法人姓名*/
    @Schema(name="legal_name",description = "法人姓名")
    private String legalName;
    /**法人身份证*/
    @Schema(name="legal_id",description = "法人身份证")
    @SecretField(SecretType.ID_CARD)
    private String legalId;
    /**法人身份证照片*/
    @Schema(name="legal_img",description = "法人身份证照片")
    private String legalImg;
    /**营业执照号*/
    @Schema(name="license_num",description = "营业执照号")
    private String licenseNum;
    /**营业执照所在省id*/
    @Schema(name="license_province_id",description = "营业执照所在省id")
    private Long licenseProvinceId;
    /**营业执照所在市id*/
    @Schema(name="license_city_id",description = "营业执照所在市id")
    private Long licenseCityId;
    /**营业执照所在县id*/
    @Schema(name="license_county_id",description = "营业执照所在县id")
    private Long licenseCountyId;
    /**营业执照所在镇id*/
    @Schema(name="license_town_id",description = "营业执照所在镇id")
    private Long licenseTownId;
    /**营业执照所在省*/
    @Schema(name="license_province",description = "营业执照所在省")
    private String licenseProvince;
    /**营业执照所在市*/
    @Schema(name="license_city",description = "营业执照所在市")
    private String licenseCity;
    /**营业执照所在县*/
    @Schema(name="license_county",description = "营业执照所在县")
    private String licenseCounty;
    /**营业执照所在镇*/
    @Schema(name="license_town",description = "营业执照所在镇")
    private String licenseTown;
    /**营业执照详细地址*/
    @Schema(name="license_add",description = "营业执照详细地址")
    private String licenseAdd;
    /**成立日期*/
    @Schema(name="establish_date",description = "成立日期")
    private Long establishDate;
    /**营业执照有效期开始*/
    @Schema(name="licence_start",description = "营业执照有效期开始")
    private Long licenceStart;
    /**营业执照有效期结束*/
    @Schema(name="licence_end",description = "营业执照有效期结束")
    private Long licenceEnd;
    /**法定经营范围*/
    @Schema(name="scope",description = "法定经营范围")
    private String scope;
    /**营业执照电子版*/
    @Schema(name="licence_img",description = "营业执照电子版")
    private String licenceImg;
    /**组织机构代码*/
    @Schema(name="organization_code",description = "组织机构代码")
    private String organizationCode;
    /**组织机构电子版*/
    @Schema(name="code_img",description = "组织机构电子版")
    private String codeImg;
    /**一般纳税人证明电子版*/
    @Schema(name="taxes_img",description = "一般纳税人证明电子版")
    private String taxesImg;
    /**银行开户名*/
    @Schema(name="bank_account_name",description = "银行开户名")
    private String bankAccountName;
    /**银行开户账号*/
    @Schema(name="bank_number",description = "银行开户账号")
    @SecretField
    private String bankNumber;
    /**开户银行支行名称*/
    @Schema(name="bank_name",description = "开户银行支行名称")
    private String bankName;
    /**开户银行所在省id*/
    @Schema(name="bank_province_id",description = "开户银行所在省id")
    private Long bankProvinceId;
    /**开户银行所在市id*/
    @Schema(name="bank_city_id",description = "开户银行所在市id")
    private Long bankCityId;
    /**开户银行所在县id*/
    @Schema(name="bank_county_id",description = "开户银行所在县id")
    private Long bankCountyId;
    /**开户银行所在镇id*/
    @Schema(name="bank_town_id",description = "开户银行所在镇id")
    private Long bankTownId;
    /**开户银行所在省*/
    @Schema(name="bank_province",description = "开户银行所在省")
    private String bankProvince;
    /**开户银行所在市*/
    @Schema(name="bank_city",description = "开户银行所在市")
    private String bankCity;
    /**开户银行所在县*/
    @Schema(name="bank_county",description = "开户银行所在县")
    private String bankCounty;
    /**开户银行所在镇*/
    @Schema(name="bank_town",description = "开户银行所在镇")
    private String bankTown;
    /**开户银行许可证电子版*/
    @Schema(name="bank_img",description = "开户银行许可证电子版")
    private String bankImg;
    /**税务登记证号*/
    @Schema(name="taxes_certificate_num",description = "税务登记证号")
    private String taxesCertificateNum;
    /**纳税人识别号*/
    @Schema(name="taxes_distinguish_num",description = "纳税人识别号")
    private String taxesDistinguishNum;
    /**税务登记证书*/
    @Schema(name="taxes_certificate_img",description = "税务登记证书")
    private String taxesCertificateImg;
    /**店铺经营类目*/
    @Schema(name="goods_management_category",description = "店铺经营类目")
    private String goodsManagementCategory;
    /**店铺等级*/
    @Schema(name="shop_level",description = "店铺等级")
    private Integer shopLevel;
    /**店铺等级申请*/
    @Schema(name="shop_level_apply",description = "店铺等级申请")
    private Integer shopLevelApply;
    /**店铺相册已用存储量*/
    @Schema(name="store_space_capacity",description = "店铺相册已用存储量")
    private Double storeSpaceCapacity;
    /**店铺logo*/
    @Schema(name="shop_logo",description = "店铺logo")
    private String shopLogo;
    /**店铺横幅*/
    @Schema(name="shop_banner",description = "店铺横幅")
    private String shopBanner;
    /**店铺简介*/
    @Size(max=200,message = "店铺简介长度不符，不能超过200")
    @Schema(name="shop_desc",description = "店铺简介")
    private String shopDesc;
    /**是否推荐*/
    @Schema(name="shop_recommend",description = "是否推荐")
    private Integer shopRecommend;
    /**店铺主题id*/
    @Schema(name="shop_themeid",description = "店铺主题id")
    private Long shopThemeid;
    /**店铺主题模版路径*/
    @Schema(name="shop_theme_path",description = "店铺主题模版路径")
    private String shopThemePath;
    /**店铺主题id*/
    @Schema(name="wap_themeid",description = "店铺主题id")
    private Long wapThemeid;
    /**wap店铺主题*/
    @Schema(name="wap_theme_path",description = "wap店铺主题")
    private String wapThemePath;
    /**店铺信用*/
    @Schema(name="shop_credit",description = "店铺信用")
    private Double shopCredit;
    /**店铺好评率*/
    @Schema(name="shop_praise_rate",description = "店铺好评率")
    private Double shopPraiseRate;
    /**店铺描述相符度*/
    @Schema(name="shop_description_credit",description = "店铺描述相符度")
    private Double shopDescriptionCredit;
    /**服务态度分数*/
    @Schema(name="shop_service_credit",description = "服务态度分数")
    private Double shopServiceCredit;
    /**发货速度分数*/
    @Schema(name="shop_delivery_credit",description = "发货速度分数")
    private Double shopDeliveryCredit;
    /**店铺收藏数*/
    @Schema(name="shop_collect",description = "店铺收藏数")
    private Integer shopCollect;
    /**店铺商品数*/
    @Schema(name="goods_num",description = "店铺商品数")
    private Integer goodsNum;
    /**店铺客服qq*/
    @Schema(name="shop_qq",description = "店铺客服qq")
    private String shopQq;
    /**店铺佣金比例*/
    @Schema(name="shop_commission",description = "店铺佣金比例")
    private Double shopCommission;
    /**货品预警数*/
    @Schema(name="goods_warning_count",description = "货品预警数")
    private Integer goodsWarningCount;
    /**是否自营*/
    @Schema(name="self_operated",description = "是否自营",required=true)
    private Integer selfOperated;
    /**申请开店进度*/
    @Schema(name="step",description = "申请开店进度：1,2,3,4")
    private Integer step;
    /**是否允许开具增值税普通发票 0：否，1：是*/
    @Schema(name="ordin_receipt_status",description="是否允许开具增值税普通发票 0：否，1：是", allowableValues = "0,1")
    private Integer ordinReceiptStatus;
    /**是否允许开具电子普通发票 0：否，1：是*/
    @Schema(name="elec_receipt_status",description="是否允许开具电子普通发票 0：否，1：是", allowableValues = "0,1")
    private Integer elecReceiptStatus;
    /**是否允许开具增值税专用发票 0：否，1：是*/
    @Schema(name="tax_receipt_status",description="是否允许开具增值税专用发票 0：否，1：是", allowableValues = "0,1")
    private Integer taxReceiptStatus;


    @PrimaryKeyField
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getShopProvinceId() {
        return shopProvinceId;
    }
    public void setShopProvinceId(Long shopProvinceId) {
        this.shopProvinceId = shopProvinceId;
    }

    public Long getShopCityId() {
        return shopCityId;
    }
    public void setShopCityId(Long shopCityId) {
        this.shopCityId = shopCityId;
    }

    public Long getShopCountyId() {
        return shopCountyId;
    }
    public void setShopCountyId(Long shopCountyId) {
        this.shopCountyId = shopCountyId;
    }

    public Long getShopTownId() {
        return shopTownId;
    }
    public void setShopTownId(Long shopTownId) {
        this.shopTownId = shopTownId;
    }

    public String getShopProvince() {
        return shopProvince;
    }
    public void setShopProvince(String shopProvince) {
        this.shopProvince = shopProvince;
    }

    public String getShopCity() {
        return shopCity;
    }
    public void setShopCity(String shopCity) {
        this.shopCity = shopCity;
    }

    public String getShopCounty() {
        return shopCounty;
    }
    public void setShopCounty(String shopCounty) {
        this.shopCounty = shopCounty;
    }

    public String getShopTown() {
        return shopTown;
    }
    public void setShopTown(String shopTown) {
        this.shopTown = shopTown;
    }

    public String getShopAdd() {
        return shopAdd;
    }
    public void setShopAdd(String shopAdd) {
        this.shopAdd = shopAdd;
    }

    public String getCompanyName() {
        return companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public Integer getEmployeeNum() {
        return employeeNum;
    }
    public void setEmployeeNum(Integer employeeNum) {
        this.employeeNum = employeeNum;
    }

    public Double getRegMoney() {
        return regMoney;
    }

    public void setRegMoney(Double regMoney) {
        this.regMoney = regMoney;
    }

    public String getLinkName() {
        return linkName;
    }
    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public String getLinkPhone() {
        return linkPhone;
    }
    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    public String getLegalName() {
        return legalName;
    }
    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getLegalId() {
        return legalId;
    }
    public void setLegalId(String legalId) {
        this.legalId = legalId;
    }

    public String getLegalImg() {
        return legalImg;
    }
    public void setLegalImg(String legalImg) {
        this.legalImg = legalImg;
    }

    public String getLicenseNum() {
        return licenseNum;
    }
    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    public Long getLicenseProvinceId() {
        return licenseProvinceId;
    }
    public void setLicenseProvinceId(Long licenseProvinceId) {
        this.licenseProvinceId = licenseProvinceId;
    }

    public Long getLicenseCityId() {
        return licenseCityId;
    }
    public void setLicenseCityId(Long licenseCityId) {
        this.licenseCityId = licenseCityId;
    }

    public Long getLicenseCountyId() {
        return licenseCountyId;
    }
    public void setLicenseCountyId(Long licenseCountyId) {
        this.licenseCountyId = licenseCountyId;
    }

    public Long getLicenseTownId() {
        return licenseTownId;
    }
    public void setLicenseTownId(Long licenseTownId) {
        this.licenseTownId = licenseTownId;
    }

    public String getLicenseProvince() {
        return licenseProvince;
    }
    public void setLicenseProvince(String licenseProvince) {
        this.licenseProvince = licenseProvince;
    }

    public String getLicenseCity() {
        return licenseCity;
    }
    public void setLicenseCity(String licenseCity) {
        this.licenseCity = licenseCity;
    }

    public String getLicenseCounty() {
        return licenseCounty;
    }
    public void setLicenseCounty(String licenseCounty) {
        this.licenseCounty = licenseCounty;
    }

    public String getLicenseTown() {
        return licenseTown;
    }
    public void setLicenseTown(String licenseTown) {
        this.licenseTown = licenseTown;
    }

    public String getLicenseAdd() {
        return licenseAdd;
    }
    public void setLicenseAdd(String licenseAdd) {
        this.licenseAdd = licenseAdd;
    }

    public Long getEstablishDate() {
        return establishDate;
    }
    public void setEstablishDate(Long establishDate) {
        this.establishDate = establishDate;
    }

    public Long getLicenceStart() {
        return licenceStart;
    }
    public void setLicenceStart(Long licenceStart) {
        this.licenceStart = licenceStart;
    }

    public Long getLicenceEnd() {
        return licenceEnd;
    }
    public void setLicenceEnd(Long licenceEnd) {
        this.licenceEnd = licenceEnd;
    }

    public String getScope() {
        return scope;
    }
    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getLicenceImg() {
        return licenceImg;
    }
    public void setLicenceImg(String licenceImg) {
        this.licenceImg = licenceImg;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }
    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getCodeImg() {
        return codeImg;
    }
    public void setCodeImg(String codeImg) {
        this.codeImg = codeImg;
    }

    public String getTaxesImg() {
        return taxesImg;
    }
    public void setTaxesImg(String taxesImg) {
        this.taxesImg = taxesImg;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }
    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankNumber() {
        return bankNumber;
    }
    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getBankName() {
        return bankName;
    }
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getBankProvinceId() {
        return bankProvinceId;
    }
    public void setBankProvinceId(Long bankProvinceId) {
        this.bankProvinceId = bankProvinceId;
    }

    public Long getBankCityId() {
        return bankCityId;
    }
    public void setBankCityId(Long bankCityId) {
        this.bankCityId = bankCityId;
    }

    public Long getBankCountyId() {
        return bankCountyId;
    }
    public void setBankCountyId(Long bankCountyId) {
        this.bankCountyId = bankCountyId;
    }

    public Long getBankTownId() {
        return bankTownId;
    }
    public void setBankTownId(Long bankTownId) {
        this.bankTownId = bankTownId;
    }

    public String getBankProvince() {
        return bankProvince;
    }
    public void setBankProvince(String bankProvince) {
        this.bankProvince = bankProvince;
    }

    public String getBankCity() {
        return bankCity;
    }
    public void setBankCity(String bankCity) {
        this.bankCity = bankCity;
    }

    public String getBankCounty() {
        return bankCounty;
    }
    public void setBankCounty(String bankCounty) {
        this.bankCounty = bankCounty;
    }

    public String getBankTown() {
        return bankTown;
    }
    public void setBankTown(String bankTown) {
        this.bankTown = bankTown;
    }

    public String getBankImg() {
        return bankImg;
    }
    public void setBankImg(String bankImg) {
        this.bankImg = bankImg;
    }

    public String getTaxesCertificateNum() {
        return taxesCertificateNum;
    }
    public void setTaxesCertificateNum(String taxesCertificateNum) {
        this.taxesCertificateNum = taxesCertificateNum;
    }

    public String getTaxesDistinguishNum() {
        return taxesDistinguishNum;
    }
    public void setTaxesDistinguishNum(String taxesDistinguishNum) {
        this.taxesDistinguishNum = taxesDistinguishNum;
    }

    public String getTaxesCertificateImg() {
        return taxesCertificateImg;
    }
    public void setTaxesCertificateImg(String taxesCertificateImg) {
        this.taxesCertificateImg = taxesCertificateImg;
    }

    public String getGoodsManagementCategory() {
        return goodsManagementCategory;
    }
    public void setGoodsManagementCategory(String goodsManagementCategory) {
        this.goodsManagementCategory = goodsManagementCategory;
    }

    public Integer getShopLevel() {
        return shopLevel;
    }
    public void setShopLevel(Integer shopLevel) {
        this.shopLevel = shopLevel;
    }

    public Integer getShopLevelApply() {
        return shopLevelApply;
    }
    public void setShopLevelApply(Integer shopLevelApply) {
        this.shopLevelApply = shopLevelApply;
    }
    public Double getStoreSpaceCapacity() {
        return storeSpaceCapacity;
    }
    public void setStoreSpaceCapacity(Double storeSpaceCapacity) {
        this.storeSpaceCapacity = storeSpaceCapacity;
    }

    public String getShopLogo() {
        return shopLogo;
    }
    public void setShopLogo(String shopLogo) {
        this.shopLogo = shopLogo;
    }

    public String getShopBanner() {
        return shopBanner;
    }
    public void setShopBanner(String shopBanner) {
        this.shopBanner = shopBanner;
    }

    public String getShopDesc() {
        return shopDesc;
    }
    public void setShopDesc(String shopDesc) {
        this.shopDesc = shopDesc;
    }

    public Integer getShopRecommend() {
        return shopRecommend;
    }
    public void setShopRecommend(Integer shopRecommend) {
        this.shopRecommend = shopRecommend;
    }

    public Long getShopThemeid() {
        return shopThemeid;
    }
    public void setShopThemeid(Long shopThemeid) {
        this.shopThemeid = shopThemeid;
    }

    public String getShopThemePath() {
        return shopThemePath;
    }
    public void setShopThemePath(String shopThemePath) {
        this.shopThemePath = shopThemePath;
    }

    public Long getWapThemeid() {
        return wapThemeid;
    }
    public void setWapThemeid(Long wapThemeid) {
        this.wapThemeid = wapThemeid;
    }

    public String getWapThemePath() {
        return wapThemePath;
    }
    public void setWapThemePath(String wapThemePath) {
        this.wapThemePath = wapThemePath;
    }

    public Double getShopCredit() {
        return shopCredit;
    }

    public void setShopCredit(Double shopCredit) {
        this.shopCredit = shopCredit;
    }

    public Double getShopPraiseRate() {
        return shopPraiseRate;
    }
    public void setShopPraiseRate(Double shopPraiseRate) {
        this.shopPraiseRate = shopPraiseRate;
    }

    public Double getShopDescriptionCredit() {
        return shopDescriptionCredit;
    }

    public void setShopDescriptionCredit(Double shopDescriptionCredit) {
        this.shopDescriptionCredit = shopDescriptionCredit;
    }

    public Double getShopServiceCredit() {
        return shopServiceCredit;
    }

    public void setShopServiceCredit(Double shopServiceCredit) {
        this.shopServiceCredit = shopServiceCredit;
    }

    public Double getShopDeliveryCredit() {
        return shopDeliveryCredit;
    }

    public void setShopDeliveryCredit(Double shopDeliveryCredit) {
        this.shopDeliveryCredit = shopDeliveryCredit;
    }

    public Integer getShopCollect() {
        return shopCollect;
    }
    public void setShopCollect(Integer shopCollect) {
        this.shopCollect = shopCollect;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }
    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getShopQq() {
        return shopQq;
    }
    public void setShopQq(String shopQq) {
        this.shopQq = shopQq;
    }

    public Double getShopCommission() {
        return shopCommission;
    }
    public void setShopCommission(Double shopCommission) {
        this.shopCommission = shopCommission;
    }

    public Integer getGoodsWarningCount() {
        return goodsWarningCount;
    }
    public void setGoodsWarningCount(Integer goodsWarningCount) {
        this.goodsWarningCount = goodsWarningCount;
    }
	public Integer getSelfOperated() {
		return selfOperated;
	}
	public void setSelfOperated(Integer selfOperated) {
		this.selfOperated = selfOperated;
	}

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public Integer getOrdinReceiptStatus() {
        return ordinReceiptStatus;
    }

    public void setOrdinReceiptStatus(Integer ordinReceiptStatus) {
        this.ordinReceiptStatus = ordinReceiptStatus;
    }

    public Integer getElecReceiptStatus() {
        return elecReceiptStatus;
    }

    public void setElecReceiptStatus(Integer elecReceiptStatus) {
        this.elecReceiptStatus = elecReceiptStatus;
    }

    public Integer getTaxReceiptStatus() {
        return taxReceiptStatus;
    }

    public void setTaxReceiptStatus(Integer taxReceiptStatus) {
        this.taxReceiptStatus = taxReceiptStatus;
    }

    @Override
    public String toString() {
        return "ShopDetailDO{" +
                "id=" + id +
                ", shopId=" + shopId +
                ", shopProvinceId=" + shopProvinceId +
                ", shopCityId=" + shopCityId +
                ", shopCountyId=" + shopCountyId +
                ", shopTownId=" + shopTownId +
                ", shopProvince='" + shopProvince + '\'' +
                ", shopCity='" + shopCity + '\'' +
                ", shopCounty='" + shopCounty + '\'' +
                ", shopTown='" + shopTown + '\'' +
                ", shopAdd='" + shopAdd + '\'' +
                ", companyName='" + companyName + '\'' +
                ", companyAddress='" + companyAddress + '\'' +
                ", companyPhone='" + companyPhone + '\'' +
                ", companyEmail='" + companyEmail + '\'' +
                ", employeeNum=" + employeeNum +
                ", regMoney=" + regMoney +
                ", linkName='" + linkName + '\'' +
                ", linkPhone='" + linkPhone + '\'' +
                ", legalName='" + legalName + '\'' +
                ", legalId='" + legalId + '\'' +
                ", legalImg='" + legalImg + '\'' +
                ", licenseNum='" + licenseNum + '\'' +
                ", licenseProvinceId=" + licenseProvinceId +
                ", licenseCityId=" + licenseCityId +
                ", licenseCountyId=" + licenseCountyId +
                ", licenseTownId=" + licenseTownId +
                ", licenseProvince='" + licenseProvince + '\'' +
                ", licenseCity='" + licenseCity + '\'' +
                ", licenseCounty='" + licenseCounty + '\'' +
                ", licenseTown='" + licenseTown + '\'' +
                ", licenseAdd='" + licenseAdd + '\'' +
                ", establishDate=" + establishDate +
                ", licenceStart=" + licenceStart +
                ", licenceEnd=" + licenceEnd +
                ", scope='" + scope + '\'' +
                ", licenceImg='" + licenceImg + '\'' +
                ", organizationCode='" + organizationCode + '\'' +
                ", codeImg='" + codeImg + '\'' +
                ", taxesImg='" + taxesImg + '\'' +
                ", bankAccountName='" + bankAccountName + '\'' +
                ", bankNumber='" + bankNumber + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankProvinceId=" + bankProvinceId +
                ", bankCityId=" + bankCityId +
                ", bankCountyId=" + bankCountyId +
                ", bankTownId=" + bankTownId +
                ", bankProvince='" + bankProvince + '\'' +
                ", bankCity='" + bankCity + '\'' +
                ", bankCounty='" + bankCounty + '\'' +
                ", bankTown='" + bankTown + '\'' +
                ", bankImg='" + bankImg + '\'' +
                ", taxesCertificateNum='" + taxesCertificateNum + '\'' +
                ", taxesDistinguishNum='" + taxesDistinguishNum + '\'' +
                ", taxesCertificateImg='" + taxesCertificateImg + '\'' +
                ", goodsManagementCategory='" + goodsManagementCategory + '\'' +
                ", shopLevel=" + shopLevel +
                ", shopLevelApply=" + shopLevelApply +
                ", storeSpaceCapacity=" + storeSpaceCapacity +
                ", shopLogo='" + shopLogo + '\'' +
                ", shopBanner='" + shopBanner + '\'' +
                ", shopDesc='" + shopDesc + '\'' +
                ", shopRecommend=" + shopRecommend +
                ", shopThemeid=" + shopThemeid +
                ", shopThemePath='" + shopThemePath + '\'' +
                ", wapThemeid=" + wapThemeid +
                ", wapThemePath='" + wapThemePath + '\'' +
                ", shopCredit=" + shopCredit +
                ", shopPraiseRate=" + shopPraiseRate +
                ", shopDescriptionCredit=" + shopDescriptionCredit +
                ", shopServiceCredit=" + shopServiceCredit +
                ", shopDeliveryCredit=" + shopDeliveryCredit +
                ", shopCollect=" + shopCollect +
                ", goodsNum=" + goodsNum +
                ", shopQq='" + shopQq + '\'' +
                ", shopCommission=" + shopCommission +
                ", goodsWarningCount=" + goodsWarningCount +
                ", selfOperated=" + selfOperated +
                ", step=" + step +
                ", ordinReceiptStatus=" + ordinReceiptStatus +
                ", elecReceiptStatus=" + elecReceiptStatus +
                ", taxReceiptStatus=" + taxReceiptStatus +
                '}';
    }
}

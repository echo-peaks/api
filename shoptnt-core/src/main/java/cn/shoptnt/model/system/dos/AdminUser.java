/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 平台管理员实体
 *
 * @author zh
 * @version v7.0
 * @since v7.0.0
 * 2018-06-20 20:38:26
 */
@TableName("es_admin_user")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AdminUser implements Serializable {

    private static final long serialVersionUID = 9076352194131639L;

    /**
     * 平台管理员id
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = false)
    private Long id;
    /**
     * 管理员名称
     */
    @Schema(name = "username", description = "管理员名称")
    private String username;
    /**
     * 管理员密码
     */
    @Schema(name = "password", description = "管理员密码")
    private String password;
    /**
     * 部门
     */
    @Schema(name = "department", description = "部门")
    private String department;
    /**
     * 权限id
     */
    @Schema(name = "role_id", description = "权限id")
    private Long roleId;
    /**
     * 创建日期
     */
    @Schema(name = "date_line", description = "创建日期")
    private Long dateLine;
    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;
    /**
     * 是否删除
     */
    @Schema(name = "user_state", description = "是否删除,0为正常,-1为删除状态")
    private Integer userState;
    /**
     * 管理员真实姓名
     */
    @Schema(name = "real_name", description = "管理员真实姓名")
    private String realName;
    /**
     * 头像
     */
    @Schema(name = "face", description = "头像")
    private String face;
    /**
     * 是否为超级管理员
     */
    @Schema(name = "founder", description = "是否为超级管理员,1为超级管理员,0为其他")
    private Integer founder;

    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getDateLine() {
        return dateLine;
    }

    public void setDateLine(Long dateLine) {
        this.dateLine = dateLine;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getUserState() {
        return userState;
    }

    public void setUserState(Integer userState) {
        this.userState = userState;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public Integer getFounder() {
        return founder;
    }

    public void setFounder(Integer founder) {
        this.founder = founder;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdminUser that = (AdminUser) o;
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (username != null ? !username.equals(that.username) : that.username != null) {
            return false;
        }
        if (password != null ? !password.equals(that.password) : that.password != null) {
            return false;
        }
        if (department != null ? !department.equals(that.department) : that.department != null) {
            return false;
        }
        if (roleId != null ? !roleId.equals(that.roleId) : that.roleId != null) {
            return false;
        }
        if (dateLine != null ? !dateLine.equals(that.dateLine) : that.dateLine != null) {
            return false;
        }
        if (remark != null ? !remark.equals(that.remark) : that.remark != null) {
            return false;
        }
        if (userState != null ? !userState.equals(that.userState) : that.userState != null) {
            return false;
        }
        if (realName != null ? !realName.equals(that.realName) : that.realName != null) {
            return false;
        }
        if (face != null ? !face.equals(that.face) : that.face != null) {
            return false;
        }
        return founder != null ? founder.equals(that.founder) : that.founder == null;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (department != null ? department.hashCode() : 0);
        result = 31 * result + (roleId != null ? roleId.hashCode() : 0);
        result = 31 * result + (dateLine != null ? dateLine.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (userState != null ? userState.hashCode() : 0);
        result = 31 * result + (realName != null ? realName.hashCode() : 0);
        result = 31 * result + (face != null ? face.hashCode() : 0);
        result = 31 * result + (founder != null ? founder.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AdminUserVO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", department='" + department + '\'' +
                ", roleId=" + roleId +
                ", dateLine=" + dateLine +
                ", remark='" + remark + '\'' +
                ", userState=" + userState +
                ", realName='" + realName + '\'' +
                ", face='" + face + '\'' +
                ", founder=" + founder +
                '}';
    }


}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.vo;

import cn.shoptnt.handler.annotation.SecretField;
import cn.shoptnt.handler.enums.SecretType;
import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.model.base.context.RegionFormat;
import cn.shoptnt.model.member.dos.MemberAddress;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Map;


/**
 * 快递平台实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-11 14:42:50
 */

public class AddressDetailVO implements Serializable {

    private static final long serialVersionUID = -5701023324446074796L;
    /**
     * 所属县(区)名称
     */
    @Schema(name = "county", description =  "所属县(区)名称", hidden = true)
    private String county;
    /**
     * 所属城市名称
     */
    @Schema(name = "city", description =  "所属城市名称", hidden = true)
    private String city;
    /**
     * 所属省份名称
     */
    @Schema(name = "province", description =  "所属省份名称", hidden = true)
    private String province;
    /**
     * 所属城镇名称
     */
    @Schema(name = "town", description =  "所属城镇名称", hidden = true)
    private String town;
    /**
     * 详细地址
     */
    @NotEmpty(message = "详细地址不能为空")
    @Schema(name = "addr", description =  "详细地址")
    private String addr;

    /**
     * 手机号码
     */
    @Schema(name = "mobile", description =  "手机号码")
    private String mobile;

    /**
     * 姓名
     */
    @Schema(name = "name", description =  "姓名")
    private String name;

    /**
     * 所属省份ID
     */
    @Schema(name = "province_id", description =  "所属省份ID", hidden = true)
    private Long provinceId;
    /**
     * 所属城市ID
     */
    @Schema(name = "city_id", description =  "所属城市ID", hidden = true)
    private Long cityId;
    /**
     * 所属县(区)ID
     */
    @Schema(name = "county_id", description =  "所属县(区)ID", hidden = true)
    private Long countyId;
    /**
     * 所属城镇ID
     */
    @Schema(name = "town_id", description =  "所属城镇ID", hidden = true)
    private Long townId;


    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getCountyId() {
        return countyId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    public Long getTownId() {
        return townId;
    }

    public void setTownId(Long townId) {
        this.townId = townId;
    }
}
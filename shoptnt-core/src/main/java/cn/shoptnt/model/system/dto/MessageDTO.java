/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dto;

import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;


/**
 * 站内消息实体DTO
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-04 21:50:52
 */
@Table(name = "es_message")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MessageDTO implements Serializable {

    private static final long serialVersionUID = 8197127057448115L;

    /**
     * 站内消息主键
     */
    @Id(name = "id")
    @Schema(hidden = true)
    private Long id;
    /**
     * 标题
     */
    @Column(name = "title")
    @Schema(name = "title", description = "标题")
    private String title;
    /**
     * 消息内容
     */
    @Column(name = "content")
    @Schema(name = "content", description = "消息内容")
    private String content;
    /**
     * 会员id
     */
    @Column(name = "member_ids")
    @Schema(name = "member_ids", description = "会员id")
    private String memberIds;
    /**
     * 管理员id
     */
    @Column(name = "admin_id")
    @Schema(name = "admin_id", description = "管理员id")
    private Long adminId;
    /**
     * 管理员名称
     */
    @Column(name = "admin_name")
    @Schema(name = "admin_name", description = "管理员名称")
    private String adminName;
    /**
     * 发送时间
     */
    @Column(name = "send_time")
    @Schema(name = "send_time", description = "发送时间")
    private Long sendTime;
    /**
     * 发送类型
     */
    @Column(name = "send_type")
    @Schema(name = "send_type", description = "发送类型,0全站，1指定会员")
    private String sendType;

    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(String memberIds) {
        this.memberIds = memberIds;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }

    public String getSendType() {
        if ("0".equals(sendType)) {
            return "全站";
        } else {
            return "指定会员";
        }
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MessageDTO that = (MessageDTO) o;
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (title != null ? !title.equals(that.title) : that.title != null) {
            return false;
        }
        if (content != null ? !content.equals(that.content) : that.content != null) {
            return false;
        }
        if (memberIds != null ? !memberIds.equals(that.memberIds) : that.memberIds != null) {
            return false;
        }
        if (adminId != null ? !adminId.equals(that.adminId) : that.adminId != null) {
            return false;
        }
        if (adminName != null ? !adminName.equals(that.adminName) : that.adminName != null) {
            return false;
        }
        if (sendTime != null ? !sendTime.equals(that.sendTime) : that.sendTime != null) {
            return false;
        }
        return sendType != null ? sendType.equals(that.sendType) : that.sendType == null;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (memberIds != null ? memberIds.hashCode() : 0);
        result = 31 * result + (adminId != null ? adminId.hashCode() : 0);
        result = 31 * result + (adminName != null ? adminName.hashCode() : 0);
        result = 31 * result + (sendTime != null ? sendTime.hashCode() : 0);
        result = 31 * result + (sendType != null ? sendType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", memberIds='" + memberIds + '\'' +
                ", adminId=" + adminId +
                ", adminName='" + adminName + '\'' +
                ", sendTime=" + sendTime +
                ", sendType=" + sendType +
                '}';
    }


}

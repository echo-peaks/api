/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dos;

import java.io.Serializable;
import java.util.Objects;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 站内消息实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-04 21:50:52
 */
@TableName("es_message")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Message implements Serializable {

    private static final long serialVersionUID = 8197127057448115L;

    /**
     * 站内消息主键
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    /**
     * 标题
     */
    @Schema(name = "title", description = "标题")
    private String title;
    /**
     * 消息内容
     */
    @Schema(name = "content", description = "消息内容")
    private String content;
    /**
     * 会员id
     */
    @Schema(name = "member_ids", description = "会员id")
    private String memberIds;
    /**
     * 管理员id
     */
    @Schema(name = "admin_id", description = "管理员id")
    private Long adminId;
    /**
     * 管理员名称
     */
    @Schema(name = "admin_name", description = "管理员名称")
    private String adminName;
    /**
     * 发送时间
     */
    @Schema(name = "send_time", description = "发送时间")
    private Long sendTime;
    /**
     * 发送类型
     */
    @Schema(name = "send_type", description = "发送类型,0全站，1指定会员")
    private Integer sendType;
    /**
     * 是否删除 0：否，1：是
     */
    @Schema(name = "disabled", description = "是否删除 0：否，1：是")
    private Integer disabled;

    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(String memberIds) {
        this.memberIds = memberIds;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Message message = (Message) o;
        return Objects.equals(id, message.id) &&
                Objects.equals(title, message.title) &&
                Objects.equals(content, message.content) &&
                Objects.equals(memberIds, message.memberIds) &&
                Objects.equals(adminId, message.adminId) &&
                Objects.equals(adminName, message.adminName) &&
                Objects.equals(sendTime, message.sendTime) &&
                Objects.equals(sendType, message.sendType) &&
                Objects.equals(disabled, message.disabled);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, content, memberIds, adminId, adminName, sendTime, sendType, disabled);
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", memberIds='" + memberIds + '\'' +
                ", adminId=" + adminId +
                ", adminName='" + adminName + '\'' +
                ", sendTime=" + sendTime +
                ", sendType=" + sendType +
                ", disabled=" + disabled +
                '}';
    }
}

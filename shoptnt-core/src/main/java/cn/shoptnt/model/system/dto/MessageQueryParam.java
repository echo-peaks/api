/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 消息搜索参数实体
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-12-06
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MessageQueryParam {

    @Schema(description = "页码", name = "page_no")
    private Long pageNo;

    @Schema(description = "分页数", name = "page_size")
    private Long pageSize;

    @Schema(description = "模糊查询的关键字", name = "keyword")
    private String keyword;

    @Schema(description = "消息标题", name = "title")
    private String title;

    @Schema(description = "消息内容", name = "content")
    private String content;

    @Schema(description = "发送类型", name = "send_type")
    private Integer sendType;

    @Schema(description = "开始时间", name = "start_time")
    private Long startTime;

    @Schema(description = "起止时间", name = "end_time")
    private Long endTime;

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "MessageQueryParam{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", keyword='" + keyword + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", sendType=" + sendType +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
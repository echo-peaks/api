/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.vo;

import cn.shoptnt.model.base.vo.ConfigItem;
import cn.shoptnt.model.system.dos.WayBillDO;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;


/**
 * 电子面单实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-06-08 16:26:05
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class WayBillVO implements Serializable {

    private static final long serialVersionUID = 6990005251050581L;

    /**
     * 电子面单id
     */
    @Schema(hidden = true)
    private Long id;
    /**
     * 名称
     */
    @Schema(name = "name", description = "名称")
    private String name;
    /**
     * 是否开启
     */
    @Schema(name = "open", description = "是否开启", required = true, hidden = true)
    private Integer open;
    /**
     * 电子面单配置
     */
    @Schema(name = "config", description = "电子面单配置")
    private String config;

    @Schema(name = "configItems", description = "电子面单配置项", required = true)
    private List<ConfigItem> configItems;
    /**
     * beanid
     */
    @Schema(name = "bean", description = "beanid")
    private String bean;


    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOpen() {
        return open;
    }

    public void setOpen(Integer open) {
        this.open = open;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getBean() {
        return bean;
    }

    public void setBean(String bean) {
        this.bean = bean;
    }

    public List<ConfigItem> getConfigItems() {
        return configItems;
    }

    public void setConfigItems(List<ConfigItem> configItems) {
        this.configItems = configItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WayBillVO that = (WayBillVO) o;
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (open != null ? !open.equals(that.open) : that.open != null) {
            return false;
        }
        if (config != null ? !config.equals(that.config) : that.config != null) {
            return false;
        }
        return bean != null ? bean.equals(that.bean) : that.bean == null;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (open != null ? open.hashCode() : 0);
        result = 31 * result + (config != null ? config.hashCode() : 0);
        result = 31 * result + (bean != null ? bean.hashCode() : 0);
        return result;
    }

    public WayBillVO(WayBillDO wayBillDO) {
        this.id = wayBillDO.getId();
        this.name = wayBillDO.getName();
        this.open = wayBillDO.getOpen();
        this.bean = wayBillDO.getBean();
        Gson gson = new Gson();
        this.configItems = gson.fromJson(wayBillDO.getConfig(), new TypeToken<List<ConfigItem>>() {
        }.getType());
    }


    public WayBillVO() {
    }

    @Override
    public String toString() {
        return "WayBillDO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", open=" + open +
                ", config='" + config + '\'' +
                ", bean='" + bean + '\'' +
                '}';
    }


}

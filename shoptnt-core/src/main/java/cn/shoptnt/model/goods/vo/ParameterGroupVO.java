/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import java.io.Serializable;
import java.util.List;

import cn.shoptnt.model.goods.dos.ParametersDO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 参数组vo
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月20日 下午4:33:21
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ParameterGroupVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 724427321881170297L;
    @Schema(description = "参数组关联的参数集合")
    private List<ParametersDO> params;
    @Schema(description = "参数组名称")
    private String groupName;
    @Schema(description = "参数组id")
    private Long groupId;

    public List<ParametersDO> getParams() {
        return params;
    }

    public void setParams(List<ParametersDO> params) {
        this.params = params;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "ParameterGroupVO [params=" + params + ", groupName=" + groupName + ", groupId=" + groupId + "]";
    }

}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.enums;

/**
 *
 * 库存日志类型
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/6/5
 */
public enum QuantityLogType {

    /**
     * 订单创建扣减库存
     */
    ORDER_CREATE,

    /**
     * 订单取消
     */
    ORDER_CANCEL,

    /**
     * 库存回滚
     */
    ROLLBACK,

    /**
     * 发货
     */
    SHIP,

    /**
     * 退货
     */
    RETURN
}
/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import cn.shoptnt.model.goods.dos.DraftGoodsDO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Arrays;
import java.util.List;

/**
 * 草稿箱商品vo
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/7/6 上午3:00
 */
@Schema
@JsonNaming(value =  PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DraftGoodsVO extends DraftGoodsDO {

    /**
     * 商品分类名称
     */
    @Schema(name = "category_name", description =  "商品分类名称")
    private String categoryName;

    @Schema(name = "category_ids", description =  "分类id数组")
    private Long[] categoryIds;

    @Schema(name = "intro_list", description =  "商品移动端详情数据集合")
    private List<GoodsMobileIntroVO> introList;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<GoodsMobileIntroVO> getIntroList() {
        return introList;
    }

    public void setIntroList(List<GoodsMobileIntroVO> introList) {
        this.introList = introList;
    }

    public Long[] getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Long[] categoryIds) {
        this.categoryIds = categoryIds;
    }

    @Override
    public String toString() {
        return "DraftGoodsVO{" +
                "categoryName='" + categoryName + '\'' +
                ", categoryIds=" + Arrays.toString(categoryIds) +
                ", introList=" + introList +
                '}';
    }
}
/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.payment.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;


/**
 * 支付方式实体
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-04-16 15:01:49
 */
@TableName("es_payment_method")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PaymentMethodDO implements Serializable {

    private static final long serialVersionUID = 6216378920396390L;

    /**
     * 支付方式id
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long methodId;
    /**
     * 支付方式名称
     */
    @Schema(name = "method_name", description = "支付方式名称")
    private String methodName;
    /**
     * 支付插件名称
     */
    @Schema(name = "plugin_id", description = "支付插件名称")
    private String pluginId;
    /**
     * pc是否可用
     */
    @Schema(name = "pc_config", description = "pc是否可用")
    private String pcConfig;
    /**
     * wap是否可用
     */
    @Schema(name = "wap_config", description = "wap是否可用")
    private String wapConfig;
    /**
     * app 原生是否可用
     */
    @Schema(name = "app_native_config", description = "app 原生是否可用")
    private String appNativeConfig;
    /**
     * 支付方式图片
     */
    @Schema(name = "image", description = "支付方式图片")
    private String image;
    /**
     * 是否支持原路退回
     */
    @Schema(name = "is_retrace", description = "是否支持原路退回")
    private Integer isRetrace;
    /**
     * app RN是否可用
     */
    @Schema(name = "app_react_config", description = "app RN是否可用")
    private String appReactConfig;
    /**
     * 小程序是否可用
     */
    @Schema(name = "mini_config", description = "pc是否可用")
    private String miniConfig;

    @PrimaryKeyField
    public Long getMethodId() {
        return methodId;
    }

    public void setMethodId(Long methodId) {
        this.methodId = methodId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public String getPcConfig() {
        return pcConfig;
    }

    public void setPcConfig(String pcConfig) {
        this.pcConfig = pcConfig;
    }

    public String getWapConfig() {
        return wapConfig;
    }

    public void setWapConfig(String wapConfig) {
        this.wapConfig = wapConfig;
    }

    public String getAppNativeConfig() {
        return appNativeConfig;
    }

    public void setAppNativeConfig(String appNativeConfig) {
        this.appNativeConfig = appNativeConfig;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getIsRetrace() {
        return isRetrace;
    }

    public void setIsRetrace(Integer isRetrace) {
        this.isRetrace = isRetrace;
    }

    public String getAppReactConfig() {
        return appReactConfig;
    }

    public void setAppReactConfig(String appReactConfig) {
        this.appReactConfig = appReactConfig;
    }

    public String getMiniConfig() {
        return miniConfig;
    }

    public void setMiniConfig(String miniConfig) {
        this.miniConfig = miniConfig;
    }

    @Override
    public String toString() {
        return "PaymentMethodDO{" +
                "methodId=" + methodId +
                ", methodName='" + methodName + '\'' +
                ", pluginId='" + pluginId + '\'' +
                ", pcConfig='" + pcConfig + '\'' +
                ", wapConfig='" + wapConfig + '\'' +
                ", appNativeConfig='" + appNativeConfig + '\'' +
                ", image='" + image + '\'' +
                ", isRetrace=" + isRetrace +
                ", appReactConfig='" + appReactConfig + '\'' +
                ", miniConfig='" + miniConfig + '\'' +
                '}';
    }

}

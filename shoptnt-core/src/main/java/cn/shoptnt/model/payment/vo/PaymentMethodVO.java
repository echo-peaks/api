/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.payment.vo;

import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author fk
 * @version v1.0
 * @Description: 支付方式VO 简要
 * @date 2018/4/2317:06
 * @since v7.0.0
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PaymentMethodVO implements Serializable {

    /**
     * 支付方式名称
     */
    @Column(name = "method_name")
    @Schema(name = "method_name", description = "支付方式名称")
    private String methodName;
    /**
     * 支付插件id
     */
    @Column(name = "plugin_id")
    @Schema(name = "plugin_id", description = "支付插件名称")
    private String pluginId;

    /**
     * 是否支持原路退回
     */
    @Column(name = "is_retrace")
    @Schema(name = "is_retrace", description = "是否支持原路退回")
    private Integer isRetrace;

    /**
     * 支付方式图片
     */
    @Column(name = "image")
    @Schema(name = "image", description = "支付方式图片")
    private String image;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public Integer getIsRetrace() {
        return isRetrace;
    }

    public void setIsRetrace(Integer isRetrace) {
        this.isRetrace = isRetrace;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "PaymentMethodVO{" +
                "methodName='" + methodName + '\'' +
                ", pluginId='" + pluginId + '\'' +
                ", isRetrace=" + isRetrace +
                ", image='" + image + '\'' +
                '}';
    }
}

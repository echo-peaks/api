/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.pintuan;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by 妙贤 on 2019-01-22.
 * 拼团商品信息VO
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2019-01-22
 */
public class PinTuanGoodsVO extends PintuanGoodsDO {

    @Schema(name = "required_num", description =  "成团人数")
    private Integer requiredNum;

    @Schema(name = "time_left", description =  "剩余时间，秒数")
    private Long timeLeft;

    @Schema(name = "promotion_rule", description =  "拼团规则")
    private String promotionRule;

    @Schema(name = "limit_num", description =  "限购数量")
    private Integer limitNum;

    @Schema(hidden = true, description =  "结束时间戳")
    private Long endTime;

    @Schema(name = "enable_quantity", description =  "可用库存")
    private Integer enableQuantity;

    public Integer getRequiredNum() {
        return requiredNum;
    }

    public void setRequiredNum(Integer requiredNum) {
        this.requiredNum = requiredNum;
    }

    public Long getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(Long timeLeft) {
        this.timeLeft = timeLeft;
    }

    public String getPromotionRule() {
        return promotionRule;
    }

    public void setPromotionRule(String promotionRule) {
        this.promotionRule = promotionRule;
    }

    public Integer getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getEnableQuantity() {
        return enableQuantity;
    }

    public void setEnableQuantity(Integer enableQuantity) {
        this.enableQuantity = enableQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PinTuanGoodsVO that = (PinTuanGoodsVO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(requiredNum, that.requiredNum)
                .append(timeLeft, that.timeLeft)
                .append(promotionRule, that.promotionRule)
                .append(limitNum, that.limitNum)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(requiredNum)
                .append(timeLeft)
                .append(promotionRule)
                .append(limitNum)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "PinTuanGoodsVO{" +
                "requiredNum=" + requiredNum +
                ", timeLeft=" + timeLeft +
                ", promotionRule='" + promotionRule + '\'' +
                ", limitNum=" + limitNum +
                "} " + super.toString();
    }
}

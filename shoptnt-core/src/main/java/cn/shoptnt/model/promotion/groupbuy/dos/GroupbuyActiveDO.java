/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.groupbuy.dos;

import java.io.Serializable;
import java.util.Objects;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 团购活动表实体
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-21 11:52:14
 */
@TableName(value = "es_groupbuy_active")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GroupbuyActiveDO implements Serializable {

    private static final long serialVersionUID = 8396241558782003L;

    /**活动Id*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long actId;

    /**活动名称*/
    @Schema(name="act_name",description = "活动名称")
    private String actName;

    /**活动开启时间*/
    @Schema(name="start_time",description = "活动开启时间")
    private Long startTime;

    /**团购结束时间*/
    @Schema(name="end_time",description = "团购结束时间")
    private Long endTime;

    /**团购报名截止时间*/
    @Schema(name="join_end_time",description = "团购报名截止时间")
    private Long joinEndTime;

    /**团购添加时间*/
    @Schema(name="add_time",description = "团购添加时间")
    private Long addTime;

    /**团购活动标签Id*/
    @Schema(name="act_tag_id",description = "团购活动标签Id")
    private Integer actTagId;

    /**参与团购商品数量*/
    @Schema(name="goods_num",description = "参与团购商品数量")
    private Integer goodsNum;

    /**是否删除 DELETED：已删除，NORMAL：正常*/
    @Schema(name="delete_status",description = "是否删除 DELETED：已删除，NORMAL：正常")
    private String deleteStatus;

    /**删除原因*/
    @Schema(name="delete_reason",description = "删除原因")
    private String deleteReason;

    /**删除日期*/
    @Schema(name="delete_time",description = "删除日期")
    private Long deleteTime;

    /**删除操作人*/
    @Schema(name="delete_name",description = "删除操作人")
    private String deleteName;

    @PrimaryKeyField
    public Long getActId() {
        return actId;
    }
    public void setActId(Long actId) {
        this.actId = actId;
    }

    public String getActName() {
        return actName;
    }
    public void setActName(String actName) {
        this.actName = actName;
    }

    public Long getStartTime() {
        return startTime;
    }
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getJoinEndTime() {
        return joinEndTime;
    }
    public void setJoinEndTime(Long joinEndTime) {
        this.joinEndTime = joinEndTime;
    }

    public Long getAddTime() {
        return addTime;
    }
    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public Integer getActTagId() {
        return actTagId;
    }
    public void setActTagId(Integer actTagId) {
        this.actTagId = actTagId;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }
    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    public Long getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Long deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getDeleteName() {
        return deleteName;
    }

    public void setDeleteName(String deleteName) {
        this.deleteName = deleteName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupbuyActiveDO that = (GroupbuyActiveDO) o;
        return Objects.equals(actId, that.actId) &&
                Objects.equals(actName, that.actName) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(joinEndTime, that.joinEndTime) &&
                Objects.equals(addTime, that.addTime) &&
                Objects.equals(actTagId, that.actTagId) &&
                Objects.equals(goodsNum, that.goodsNum) &&
                Objects.equals(deleteStatus, that.deleteStatus) &&
                Objects.equals(deleteReason, that.deleteReason) &&
                Objects.equals(deleteTime, that.deleteTime) &&
                Objects.equals(deleteName, that.deleteName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actId, actName, startTime, endTime, joinEndTime, addTime, actTagId, goodsNum, deleteStatus, deleteReason, deleteTime, deleteName);
    }
}

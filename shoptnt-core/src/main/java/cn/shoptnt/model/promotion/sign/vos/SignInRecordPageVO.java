package cn.shoptnt.model.promotion.sign.vos;

import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.model.promotion.sign.dos.SignInRecord;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author zh
 * @version 1.0
 * @title SignInRecordPageVO
 * @description 签到返回对象
 * @program: api
 * 2024/3/12 14:29
 */
public class SignInRecordPageVO extends SignInRecord {


    private static final long serialVersionUID = 6861569955554536374L;
    @Schema(description =  "奖品")
    private String prizeTypeText;


}

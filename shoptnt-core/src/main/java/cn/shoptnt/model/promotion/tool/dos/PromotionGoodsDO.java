/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.tool.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 有效活动商品对照表实体
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-27 15:17:47
 */
@TableName("es_promotion_goods")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PromotionGoodsDO implements Serializable {

    private static final long serialVersionUID = 686823101861419L;

    /**
     * 商品对照ID
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long pgId;

    /**
     * 商品id
     */
    @Schema(name = "goods_id", description = "商品id")
    private Long goodsId;

    /**
     * 货品id
     */
    @Schema(name = "sku_id", description = "货品id")
    private Long skuId;

    /**
     * 活动开始时间
     */
    @Schema(name = "start_time", description = "活动开始时间")
    private Long startTime;

    /**
     * 活动结束时间
     */
    @Column(name = "end_time")
    @Schema(name = "end_time", description = "活动结束时间")
    private Long endTime;

    /**
     * 活动id
     */
    @Schema(name = "activity_id", description = "活动id")
    private Long activityId;

    /**
     * 促销工具类型
     */
    @Schema(name = "promotion_type", description = "促销工具类型")
    private String promotionType;

    /**
     * 活动标题
     */
    @Schema(name = "title", description = "活动标题")
    private String title;

    /**
     * 参与活动的商品数量
     */
    @Schema(name = "num", description = "参与活动的商品数量")
    private Integer num;

    /**
     * 活动时商品的价格
     */
    @Schema(name = "price", description = "活动时商品的价格")
    private Double price;

    @Schema(name = "seller_id", description = "商家ID")
    private Long sellerId;

    @PrimaryKeyField
    public Long getPgId() {
        return pgId;
    }

    public void setPgId(Long pgId) {
        this.pgId = pgId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public String toString() {
        return "PromotionGoodsDO{" +
                "pgId=" + pgId +
                ", goodsId=" + goodsId +
                ", skuId=" + skuId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", activityId=" + activityId +
                ", promotionType='" + promotionType + '\'' +
                ", title='" + title + '\'' +
                ", num=" + num +
                ", price=" + price +
                ", sellerId=" + sellerId +
                '}';
    }
}

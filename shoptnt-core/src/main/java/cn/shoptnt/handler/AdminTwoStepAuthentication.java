/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.handler;

import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.security.model.Admin;
import cn.shoptnt.model.base.CachePrefix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 管理端敏感信息鉴权
 * @author snow
 * 2021年11月20日11:16:45
 *
 */
@Component
public class AdminTwoStepAuthentication {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Cache cache;

    public void sensitive(){
        Admin adminUser = AdminUserContext.getAdmin();
        //判断是否是超级管理员，如果是，则通过
        if(adminUser.getFounder() == 1){
            //PASS
        }else {
            //读取redis,验证二次身份验证
            String twoStepResult = (String) this.cache.get(CachePrefix.TWO_STEP_FLAG.getPrefix()+adminUser.getUid());
            //如果不等于SUCCESS，代表二次验证未通过
            if(!"SUCCESS".equals(twoStepResult)){
                throw new ServiceException("500","敏感信息验证失败");
            } else {
                this.cache.remove(CachePrefix.TWO_STEP_FLAG.getPrefix()+adminUser.getUid());
            }
        }
    }

}

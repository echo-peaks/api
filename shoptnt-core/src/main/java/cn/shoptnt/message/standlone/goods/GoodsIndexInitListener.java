package cn.shoptnt.message.standlone.goods;

import cn.shoptnt.message.dispatcher.goods.GoodsIndexInitDispatcher;
import cn.shoptnt.model.base.message.IndexCreateMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 初始化商品索引 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class GoodsIndexInitListener {

    private static Logger logger= LoggerFactory.getLogger(GoodsIndexInitListener.class);

    @Autowired
    private GoodsIndexInitDispatcher goodsIndexInitDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void initGoodsIndex(IndexCreateMessage indexCreateMessage) {
        logger.debug("监听到异步消息：",indexCreateMessage);
        goodsIndexInitDispatcher.dispatch(indexCreateMessage);
    }
}

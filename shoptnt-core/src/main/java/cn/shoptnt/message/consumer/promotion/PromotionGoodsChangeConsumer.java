/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.promotion;

import cn.shoptnt.client.trade.PintuanGoodsClient;
import cn.shoptnt.message.event.GoodsSkuChangeEvent;
import cn.shoptnt.client.promotion.PromotionGoodsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 删除促销活动商品
 *
 * @author liuyulei
 * @version v1.0
 * @since v7.1.3 2019-07-30
 */
@Component
public class PromotionGoodsChangeConsumer implements GoodsSkuChangeEvent {

    @Autowired
    private PromotionGoodsClient promotionGoodsClient;

    @Autowired
    private PintuanGoodsClient pintuanGoodsClient;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void goodsSkuChange(List<Long> delSkuIds) {
        //删除促销活动商品
        promotionGoodsClient.delPromotionGoods(delSkuIds);
        //删除拼团商品
        pintuanGoodsClient.deletePinTuanGoods(delSkuIds);

    }

}

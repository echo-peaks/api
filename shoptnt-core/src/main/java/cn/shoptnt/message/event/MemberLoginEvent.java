/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.event;

import cn.shoptnt.model.member.vo.MemberLoginMsg;

/**
 * 会员登录事件
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:25:27
 */
public interface MemberLoginEvent {

    /**
     * 会员登录
     *
     * @param memberLoginMsg
     */
    void memberLogin(MemberLoginMsg memberLoginMsg);
}

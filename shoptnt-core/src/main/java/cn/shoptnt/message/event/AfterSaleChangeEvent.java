/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.event;

import cn.shoptnt.model.base.message.AfterSaleChangeMessage;

/**
 * 售后服务单状态变化事件
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-24
 */
public interface AfterSaleChangeEvent {

    /**
     * 售后服务单状态变化后执行
     * @param afterSaleChangeMessage
     */
    void afterSaleChange(AfterSaleChangeMessage afterSaleChangeMessage);

}

package cn.shoptnt.message.event;

import cn.shoptnt.model.shop.vo.ShopRegisterMsg;

/**
 * 商家入驻事件接口
 * @author dmy
 * @version 1.0
 * @since 5.2.3
 * 2022-05-07
 */
public interface ShopRegisterEvent {

    /**
     * 商家入驻接口
     * @param shopRegisterMsg 入驻消息
     */
    void shopRegister(ShopRegisterMsg shopRegisterMsg);

}

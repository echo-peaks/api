package cn.shoptnt.message.dispatcher.trade.aftersale;

import cn.shoptnt.message.event.AfterSaleChangeEvent;
import cn.shoptnt.model.base.message.AfterSaleChangeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 售后服务单状态变化 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 **/
@Service
public class AfterSaleChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<AfterSaleChangeEvent> events;

    public void dispatch(AfterSaleChangeMessage afterSaleChangeMessage) {
        if (events != null) {
            for (AfterSaleChangeEvent event : events) {
                try {
                    event.afterSaleChange(afterSaleChangeMessage);
                } catch (Exception e) {
                    logger.error("处理售后服务单状态变化消息出错", e);
                }
            }
        }
    }
}

package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.MemberSignEvent;
import cn.shoptnt.model.base.message.SignMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员站内消息 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class MemberSignMessageDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberSignEvent> events;

    public void dispatch(SignMessage message) {
        if (events != null) {
            for (MemberSignEvent event : events) {
                try {
                    event.sign(message.getSignInRecords());
                } catch (Exception e) {
                    logger.error("签到发送出错", e);
                }
            }
        }
    }
}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.goods.dos.DraftGoodsParamsDO;
import cn.shoptnt.model.goods.vo.GoodsParamsVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * DraftGoodsParams的Mapper
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface DraftGoodsParamsMapper extends BaseMapper<DraftGoodsParamsDO> {

    /**
     * 查询草稿商品关联的参数及参数值
     * @param categoryId 分类id
     * @param draftGoodsId 草稿商品id
     * @return
     */
    List<GoodsParamsVO> queryDraftGoodsParamsValue(@Param("category_id") Long categoryId,  @Param("draft_goods_id")Long draftGoodsId);
}

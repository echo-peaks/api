package cn.shoptnt.mapper.promotion.sigin;

import cn.shoptnt.model.promotion.sign.dos.SignInReward;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zh
 * @version 1.0
 * @title signInActivityMapper
 * @description 签到活动奖品mapper
 * @program: api
 * 2024/3/12 12:05
 */
public interface SignInRewardMapper extends BaseMapper<SignInReward> {
}

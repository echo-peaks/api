package cn.shoptnt.mapper.promotion.sigin;

import cn.shoptnt.model.promotion.sign.dos.SignInActivity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zh
 * @version 1.0
 * @title signInActivityMapper
 * @description 签到活动mapper
 * @program: api
 * 2024/3/12 12:05
 */
public interface SignInActivityMapper extends BaseMapper<SignInActivity> {
}

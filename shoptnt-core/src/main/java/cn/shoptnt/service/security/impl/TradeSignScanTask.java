/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.client.trade.TradeClient;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.model.trade.order.dos.TradeDO;
import cn.shoptnt.service.security.BaseSignScanTask;
import cn.shoptnt.model.security.ScanResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 交易数据签名扫描
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/19 16:23
 **/
@Service("tradeSignScanTask")
public class TradeSignScanTask extends BaseSignScanTask<TradeDO> {

    @Autowired
    private TradeClient tradeClient;


    @Override
    protected ScanResult scanModule(String rounds) {
        QueryWrapper<TradeDO> queryWrapper = createWrapper(rounds);
        ScanModuleDTO<TradeDO> scanModuleDTO = new ScanModuleDTO<>(rounds, queryWrapper, pageSize);
        return tradeClient.scanModule(scanModuleDTO);
    }

    @Override
    public void reSign() {
        tradeClient.reSign();
    }

}

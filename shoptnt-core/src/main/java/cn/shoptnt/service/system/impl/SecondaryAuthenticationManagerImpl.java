/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Admin;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.system.AuthenticationMapper;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.errorcode.SystemErrorCode;
import cn.shoptnt.model.system.dos.AuthenticationDO;
import cn.shoptnt.service.system.SecondaryAuthenticationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import cn.shoptnt.framework.database.WebPage;

import java.util.List;


/**
 * 二次身份验证业务类
 *
 * @author shenyanwu
 * @version v2.0
 * @since v2.0
 * 2021-11-19 18:45:02
 */
@Service
public class SecondaryAuthenticationManagerImpl implements SecondaryAuthenticationManager {

    @Autowired
    private AuthenticationMapper authenticationMapper;

    @Override
    public WebPage list(Long pageNo, Long pageSize) {

        //新建查询条件包装器
        QueryWrapper<AuthenticationDO> wrapper = new QueryWrapper<>();

        IPage<AuthenticationDO> iPage = authenticationMapper.selectPage(new Page<>(pageNo, pageSize), wrapper);

        return PageConvert.convert(iPage);
    }

    @Override
    @Transactional(value = "", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public AuthenticationDO add(AuthenticationDO authentication) {
        this.authenticationMapper.insert(authentication);

        return authentication;
    }

    @Override
    @Transactional(value = "", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public AuthenticationDO edit(AuthenticationDO authentication) {

        Admin admin = AdminUserContext.getAdmin();
        AuthenticationDO modelByUid = getModelByUid(admin.getUid());

        if (StringUtil.notEmpty(authentication.getPassword())) {
            String md5Password = StringUtil.md5(authentication.getPassword() + admin.getUsername().toLowerCase());
            authentication.setPassword(md5Password);
        }
        if (modelByUid == null) {
            authentication.setAdminId(admin.getUid());
            this.authenticationMapper.insert(authentication);
        } else {
            authentication.setId(modelByUid.getId());
            this.authenticationMapper.updateById(authentication);
        }

        return authentication;
    }

    @Override
    @Transactional(value = "", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(Long uid) {
        QueryWrapper<AuthenticationDO> wrapper = new QueryWrapper<>();
        wrapper.eq("admin_id", uid);
        this.authenticationMapper.delete(wrapper);
    }

    @Override
    public AuthenticationDO getModel(Long id) {
        return this.authenticationMapper.selectById(id);
    }

    @Override
    public AuthenticationDO getModelByUid(Long uid) {
        QueryWrapper<AuthenticationDO> wrapper = new QueryWrapper<>();
        wrapper.eq("admin_id", uid);
        List<AuthenticationDO> list = authenticationMapper.selectList(wrapper);
        if (list.isEmpty()) {
            return null;
        }
        AuthenticationDO authenticationDO = list.get(0);
        //authenticationMapper.selectOne(wrapper);
        return authenticationDO;
    }
}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.goods;

import cn.shoptnt.model.goods.dos.BrandDO;
import cn.shoptnt.model.goods.vo.BrandVO;
import cn.shoptnt.model.goods.vo.SelectVO;
import cn.shoptnt.framework.database.WebPage;

import java.util.List;

/**
 * 品牌业务层
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-16 16:32:45
 */
public interface BrandManager {

	/**
	 * 查询品牌列表
	 *
	 * @param page 页码
	 * @param pageSize 每页数量
	 * @param name 品牌名称
     * @return WebPage 分页数据
	 */
	WebPage list(long page, long pageSize, String name);

	/**
	 * 添加品牌
	 *
	 * @param brand 品牌
	 * @return Brand 品牌
	 */
	BrandDO add(BrandDO brand);

	/**
	 * 修改品牌
	 *
	 * @param brand 品牌
	 * @param id 品牌主键
	 * @return Brand 品牌
	 */
	BrandDO edit(BrandDO brand, Long id);

	/**
	 * 删除品牌
	 *
	 * @param ids 品牌主键
	 */
	void delete(Long[] ids);

	/**
	 * 获取品牌
	 *
	 * @param id 品牌主键
	 * @return Brand 品牌
	 */
	BrandDO getModel(Long id);

	/**
	 * 查询某分类下的品牌
	 *
	 * @param categoryId 分类id
	 * @return 品牌列表
	 */
	List<BrandDO> getBrandsByCategory(Long categoryId);

	/**
	 * 查询分类品牌，所有品牌，分类绑定的品牌为已选中状态
	 *
	 * @param categoryId 分类id
	 * @return 品牌列表
	 */
	List<SelectVO> getCatBrand(Long categoryId);


	/**
	 * 查询全部的品牌
	 * @return 品牌列表
	 */
    List<BrandDO> getAllBrands();

	/**
	 * 查询所有关联了一级分类的品牌信息集合
	 * @return
	 */
	List<BrandVO> getAllBrandCategory();

	/**
	 * 模糊匹配分类所有品牌，分类绑定的品牌为已选中状态
	 * @param categoryId 分类 id
	 * @param name 品牌名称
	 * @return
	 */
	List<SelectVO> searchBrand(Long categoryId,String name);
}

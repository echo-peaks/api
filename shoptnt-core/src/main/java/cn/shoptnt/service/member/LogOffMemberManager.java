package cn.shoptnt.service.member;

import cn.shoptnt.model.member.dos.LogOffMember;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author zh
 * @version 1.0
 * @title LogOffMemberManager
 * @description 注销账户会员
 * @program: api
 * 2024/3/6 16:48
 */
public interface LogOffMemberManager extends IService<LogOffMember> {

}

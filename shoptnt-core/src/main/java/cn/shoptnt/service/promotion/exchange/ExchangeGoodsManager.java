/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.promotion.exchange;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.exchange.dto.ExchangeQueryParam;
import cn.shoptnt.model.promotion.tool.dto.PromotionGoodsDTO;
import cn.shoptnt.model.promotion.exchange.dos.ExchangeDO;

/**
 * 积分商品业务层
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-21 11:47:18
 */
public interface ExchangeGoodsManager {

	/**
	 * 查询积分商品列表
	 * @param param 查询参数
	 * @return
	 */
	WebPage list(ExchangeQueryParam param);

	/**
	 * 添加积分兑换
	 * @param exchangeSetting 积分兑换
	 * @param goodsDTO 商品DTO
	 * @return ExchangeSetting 积分兑换
	 */
	ExchangeDO add(ExchangeDO exchangeSetting, PromotionGoodsDTO goodsDTO);

	/**
	* 修改积分兑换
	* @param exchangeSetting 积分兑换
	* @param goodsDTO 商品DTO
	* @return ExchangeSetting 积分兑换
	*/
	ExchangeDO edit(ExchangeDO exchangeSetting, PromotionGoodsDTO goodsDTO);

	/**
	 * 删除积分兑换
	 * @param id 积分兑换主键
	 */
	void delete(Long id);

	/**
	 * 获取积分兑换
	 * @param id 积分兑换主键
	 * @return ExchangeSetting  积分兑换
	 */
	ExchangeDO getModel(Long id);

	/**
	 * 查询某个商品的积分兑换信息
	 * @param goodsId 商品ID
	 * @return
	 */
    ExchangeDO getModelByGoods(Long goodsId);

	/**
	 * 查询某个积分分类的积分兑换信息
	 * @param categoryId 积分商品分类ID
	 * @return
	 */
	ExchangeDO getModelByCategoryId(Long categoryId);

	/**
	 * 删除某个商品的积分信息
	 * @param goodsId 商品ID
	 */
	void deleteByGoods(Long goodsId);
}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.promotion.coupon.impl;

import cn.shoptnt.framework.lock.Lock;
import cn.shoptnt.framework.lock.LockFactory;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.exception.SystemErrorCodeV1;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.framework.trigger.Interface.TimeTrigger;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.promotion.CouponMapper;
import cn.shoptnt.mapper.promotion.fulldiscount.FullDiscountMapper;
import cn.shoptnt.model.base.message.PromotionScriptMsg;
import cn.shoptnt.framework.message.TimeExecute;
import cn.shoptnt.model.errorcode.PromotionErrorCode;
import cn.shoptnt.model.goods.vo.CacheGoods;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.model.promotion.coupon.dto.CouponParams;
import cn.shoptnt.model.promotion.coupon.enums.CouponType;
import cn.shoptnt.model.promotion.coupon.enums.CouponUseScope;
import cn.shoptnt.model.promotion.coupon.vo.CouponVO;
import cn.shoptnt.model.promotion.coupon.vo.GoodsCouponVO;
import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountDO;
import cn.shoptnt.model.promotion.tool.enums.PromotionTypeEnum;
import cn.shoptnt.model.promotion.tool.enums.ScriptOperationTypeEnum;
import cn.shoptnt.model.util.PromotionValid;
import cn.shoptnt.service.promotion.coupon.CouponManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * 优惠券业务类
 *
 * @author Snow
 * @version v2.0
 * @since v7.0.0
 * 2018-04-17 23:19:39
 */
@SuppressWarnings("Duplicates")
@Service
public class CouponManagerImpl implements CouponManager {

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private TimeTrigger timeTrigger;

    @Autowired
    private CouponMapper couponMapper;

    @Autowired
    private FullDiscountMapper fullDiscountMapper;

    @Autowired
    private LockFactory lockFactory;

    /**
     * 查询优惠券分页数据列表
     *
     * @param couponParams 查询参数
     * @return WebPage
     */
    @Override
    public WebPage list(CouponParams couponParams) {

        QueryWrapper<CouponDO> wrapper = new QueryWrapper<CouponDO>()
                .eq("seller_id", couponParams.getSellerId())
                //如果开始时间不为空，则>=开始时间
                .ge(couponParams.getStartTime() != null, "start_time", couponParams.getStartTime())
                //如果结束时间不为空，则<=结束时间
                .le(couponParams.getEndTime() != null, "end_time", couponParams.getEndTime())
                //如果关键字不为空，则模糊查询title
                .like(StringUtil.notEmpty(couponParams.getKeyword()), "title", couponParams.getKeyword())
                //优惠券id倒叙
                .orderByDesc("coupon_id");


        IPage<CouponVO> iPage = this.couponMapper.selectCouponPage(new Page(couponParams.getPageNo(), couponParams.getPageSize()), wrapper);

        return PageConvert.convert(iPage);
    }

    /**
     * 读取商品可用优惠券
     *
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public List<GoodsCouponVO> getListByGoods(Long goodsId) {

        List<GoodsCouponVO> res = new ArrayList<>();

        //查询商品的分类
        CacheGoods goods = goodsClient.getFromCache(goodsId);
        //商品的规格信息
        List<GoodsSkuVO> skuList = goods.getSkuList();

        Long sellerId = goods.getSellerId();
        Long categoryId = goods.getCategoryId();

        long time = DateUtil.getDateline();

        List<CouponDO> couponList = this.couponMapper.selectList(new QueryWrapper<CouponDO>()
                .lt("start_time", time)
                .gt("end_time", time)
                .eq("type", CouponType.FREE_GET.name())
                .and(ew -> {
                    //店铺优惠券
                    ew.eq("seller_id", sellerId).or(e -> {
                        //平台优惠券
                        e.eq("seller_id", 0)
                                .and(catOrAllWrapper -> {
                                    //使用范围是全部
                                    catOrAllWrapper.eq("use_scope", CouponUseScope.ALL.name())
                                            //或者
                                            .or(catWrapper -> {
                                                //使用范围是分类，且满足分类id
                                                catWrapper.eq("use_scope", CouponUseScope.CATEGORY.name())
                                                        .like("concat(',',scope_id,',')", "," + categoryId + ",");
                                            });
                                });
                    });
                }));


        //查询当前可以使用的部分商品的优惠券
        List<CouponDO> someGoodsCouponList = this.couponMapper.selectList(new QueryWrapper<CouponDO>()
                .lt("start_time", time)
                .gt("end_time", time)
                .eq("type", CouponType.FREE_GET.name())
                .eq("seller_id", 0)
                .eq("use_scope", CouponUseScope.SOME_GOODS.name()));

        //循环sku，嵌套循环部分商品优惠券，查看skuid是否在scope_id中
        if (skuList != null) {
            for (GoodsSkuVO skuVO : skuList) {
                GoodsCouponVO couponVO = new GoodsCouponVO();
                List<CouponDO> skuCouponList = new ArrayList<>();
                skuCouponList.addAll(couponList);
                couponVO.setGoodsId(goodsId);
                couponVO.setSkuId(skuVO.getSkuId());
                //循环部分商品的优惠券
                if (someGoodsCouponList != null) {
                    for (CouponDO couponDO : someGoodsCouponList) {
                        if (couponDO.getScopeId().contains("," + skuVO.getSkuId() + ",")) {
                            //添加到sku可用的优惠券中
                            skuCouponList.add(couponDO);
                        }
                    }
                }
                couponVO.setCouponList(skuCouponList);
                res.add(couponVO);
            }
        }
        return res;
    }

    /**
     * 读取商家可用优惠券
     *
     * @param sellerId 商家ID
     * @return
     */
    @Override
    public List<CouponDO> getList(Long sellerId,String type) {

        //查询免费领取的优惠券
        List<CouponDO> couponDOList = this.couponMapper.selectList(new QueryWrapper<CouponDO>()
                .eq("seller_id", sellerId)
                .lt("start_time", DateUtil.getDateline())
                .gt("end_time", DateUtil.getDateline())
                .eq("type", type));

        return couponDOList;
    }

    /**
     * 添加优惠券信息
     *
     * @param coupon 优惠券信息
     * @return coupon 优惠券信息
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {ServiceException.class, RuntimeException.class, Exception.class})
    public CouponDO add(CouponDO coupon) {
        //检测开始时间和结束时间
        PromotionValid.paramValid(coupon.getStartTime(), coupon.getEndTime(), 1, null);
        if (coupon.getLimitNum() < 0) {
            throw new ServiceException(PromotionErrorCode.E406.code(), "限领数量不能为负数");
        }
        //校验每人限领数是都大于发行量
        if (coupon.getLimitNum() > coupon.getCreateNum()) {
            throw new ServiceException(PromotionErrorCode.E405.code(), "限领数量超出发行量");
        }
        if (coupon.getCouponPrice() == null || coupon.getCouponPrice() <= 0) {
            throw new ServiceException(PromotionErrorCode.E409.code(), "优惠券面额必须大于0元");
        }
        if (coupon.getCouponThresholdPrice() == null || coupon.getCouponThresholdPrice() <= 0) {
            throw new ServiceException(PromotionErrorCode.E409.code(), "优惠券门槛价格必须大于0元");
        }
        //校验优惠券面额是否小于门槛价格
        if (coupon.getCouponPrice() >= coupon.getCouponThresholdPrice()) {
            throw new ServiceException(PromotionErrorCode.E409.code(), "优惠券面额必须小于优惠券门槛价格");
        }

        //开始时间取前段+00:00:00 结束时间取前段+23:59:59
        String startStr = DateUtil.toString(coupon.getStartTime(), "yyyy-MM-dd");
        String endStr = DateUtil.toString(coupon.getEndTime(), "yyyy-MM-dd");

        coupon.setStartTime(DateUtil.getDateline(startStr + " 00:00:00"));
        coupon.setEndTime(DateUtil.getDateline(endStr + " 23:59:59", "yyyy-MM-dd hh:mm:ss"));

        this.paramValid(coupon.getStartTime(), coupon.getEndTime());

        coupon.setReceivedNum(0);
        coupon.setUsedNum(0);
        //部分商品和分类的id存储增加,,
        if (CouponUseScope.SOME_GOODS.name().equals(coupon.getUseScope()) || CouponUseScope.CATEGORY.name().equals(coupon.getUseScope())) {
            coupon.setScopeId("," + coupon.getScopeId() + ",");
        }

        this.couponMapper.insert(coupon);

        //启用延时任务创建优惠券脚本信息
        PromotionScriptMsg promotionScriptMsg = new PromotionScriptMsg();
        promotionScriptMsg.setPromotionId(coupon.getCouponId());
        promotionScriptMsg.setPromotionName(coupon.getTitle());
        promotionScriptMsg.setPromotionType(PromotionTypeEnum.COUPON);
        promotionScriptMsg.setOperationType(ScriptOperationTypeEnum.CREATE);
        promotionScriptMsg.setEndTime(coupon.getEndTime());
        String uniqueKey = "{TIME_TRIGGER_" + PromotionTypeEnum.COUPON.name() + "}_" + coupon.getCouponId();
        timeTrigger.add(TimeExecute.COUPON_SCRIPT_EXECUTER, promotionScriptMsg, coupon.getStartTime(), uniqueKey);

        return coupon;
    }


    /**
     * 修改优惠券信息
     *
     * @param coupon 优惠券信息
     * @param id     优惠券主键ID
     * @return coupon 优惠券信息
     */
    @Override
    public CouponDO edit(CouponDO coupon, Long id) {

        Lock lock = lockFactory.getLock(id + "");

        try {
            //因为会员修改优惠券在多线程的情况可能出现脏读 需要加锁
            lock.lock();
            //校验每人限领数是都大于发行量
            if (coupon.getLimitNum() > coupon.getCreateNum()) {
                throw new ServiceException(PromotionErrorCode.E405.code(), "限领数量超出发行量");
            }
            if (coupon.getCouponPrice() == null || coupon.getCouponPrice() <= 0) {
                throw new ServiceException(PromotionErrorCode.E409.code(), "优惠券面额必须大于0元");
            }
            if (coupon.getCouponThresholdPrice() == null || coupon.getCouponThresholdPrice() <= 0) {
                throw new ServiceException(PromotionErrorCode.E409.code(), "优惠券门槛价格必须大于0元");
            }
            //校验优惠券面额是否小于门槛价格
            if (coupon.getCouponPrice() >= coupon.getCouponThresholdPrice()) {
                throw new ServiceException(PromotionErrorCode.E409.code(), "优惠券面额必须小于优惠券门槛价格");
            }

            //开始时间取前段+00:00:00 结束时间取前段+23:59:59
            String startStr = DateUtil.toString(coupon.getStartTime(), "yyyy-MM-dd");
            String endStr = DateUtil.toString(coupon.getEndTime(), "yyyy-MM-dd");

            coupon.setStartTime(DateUtil.getDateline(startStr + " 00:00:00"));
            coupon.setEndTime(DateUtil.getDateline(endStr + " 23:59:59", "yyyy-MM-dd hh:mm:ss"));

            this.paramValid(coupon.getStartTime(), coupon.getEndTime());

            //验证开始结束时间
            this.paramValid(coupon.getStartTime(), coupon.getEndTime());
            //验证优惠券活动是不是不是可更改的状态
            this.verifyStatus(id);
            //检测开始时间和结束时间
            PromotionValid.paramValid(coupon.getStartTime(), coupon.getEndTime(), 1, null);
            //获取修改操作之前的优惠券信息
            CouponDO oldCoupon = this.getModel(id);

            coupon.setCouponId(id);
            this.couponMapper.updateById(coupon);

            //启用延时任务创建优惠券脚本信息
            PromotionScriptMsg promotionScriptMsg = new PromotionScriptMsg();
            promotionScriptMsg.setPromotionId(id);
            promotionScriptMsg.setPromotionName(coupon.getTitle());
            promotionScriptMsg.setPromotionType(PromotionTypeEnum.COUPON);
            promotionScriptMsg.setOperationType(ScriptOperationTypeEnum.CREATE);
            promotionScriptMsg.setEndTime(coupon.getEndTime());

            String uniqueKey = "{TIME_TRIGGER_" + PromotionTypeEnum.COUPON.name() + "}_" + id;
            timeTrigger.edit(TimeExecute.COUPON_SCRIPT_EXECUTER, promotionScriptMsg, oldCoupon.getStartTime(), coupon.getStartTime(), uniqueKey);

        } finally {
            lock.unlock();
        }
        return coupon;
    }

    /**
     * 删除优惠券信息
     *
     * @param id 优惠券主键ID
     */
    @Override
    public void delete(Long id) {
        //验证是否可修改和删除
        this.verifyStatus(id);

        //获取优惠券信息
        CouponDO couponDO = this.getModel(id);

        this.couponMapper.deleteById(id);

        //删除缓存中的延时任务执行器
        String uniqueKey = "{TIME_TRIGGER_" + PromotionTypeEnum.COUPON.name() + "}_" + id;
        timeTrigger.delete(TimeExecute.COUPON_SCRIPT_EXECUTER, couponDO.getStartTime(), uniqueKey);
        if (couponDO.getEndTime().longValue() < DateUtil.getDateline()) {
            timeTrigger.delete(TimeExecute.COUPON_SCRIPT_EXECUTER, couponDO.getEndTime(), uniqueKey);
        }
    }

    /**
     * 获取优惠券信息
     *
     * @param id 优惠券主键ID
     * @return Coupon 优惠券信息
     */
    @Override
    public CouponDO getModel(Long id) {

        CouponDO couponDO = this.couponMapper.selectById(id);

        return couponDO;
    }

    /**
     * 验证操作权限<br/>
     * 如有问题直接抛出权限异常
     *
     * @param id 优惠券主键ID
     */
    @Override
    public void verifyAuth(Long id) {
        CouponDO couponDO = this.getModel(id);

        if (couponDO == null) {
            throw new NoPermissionException("无权操作或者数据不存在");
        }

        if (couponDO.getSellerId() != 0 && !couponDO.getSellerId().equals(UserContext.getSeller().getSellerId())) {
            throw new NoPermissionException("无权操作或者数据不存在");
        }

        if (couponDO.getSellerId() == 0 && AdminUserContext.getAdmin() == null) {
            throw new NoPermissionException("无权操作或者数据不存在");
        }
    }

    /**
     * 增加优惠券已使用数量
     *
     * @param couponId 优惠券主键ID
     */
    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void addUsedNum(Long couponId) {
        this.couponMapper.update(null, new UpdateWrapper<CouponDO>()
                .setSql("used_num = used_num+1")
                .eq("coupon_id", couponId));
    }

    /**
     * 增加优惠券被领取数量
     *
     * @param couponId 优惠券主键ID
     */
    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void addReceivedNum(Long couponId) {

        this.couponMapper.update(null, new UpdateWrapper<CouponDO>()
                .setSql("received_num = received_num+1")
                .eq("coupon_id", couponId));
    }

    /**
     * 查询商家所有优惠券分页数据列表
     *
     * @param page     页码
     * @param pageSize 每页数量
     * @param sellerId 商家id
     * @return WebPage
     */
    @Override
    public WebPage all(long page, long pageSize, Long sellerId) {
        Long nowTime = DateUtil.getDateline();

        IPage iPage = this.couponMapper.selectPage(new Page<>(page, pageSize), new QueryWrapper<CouponDO>()
                .le("start_time", nowTime)
                .gt("end_time", nowTime)
                .eq("type", CouponType.FREE_GET.name())
                .eq(sellerId != null, "seller_id", sellerId)
                .orderByDesc("coupon_id"));

        return PageConvert.convert(iPage);
    }

    /**
     * 修改优惠券所属的店铺名称
     * 当商家修改店铺名称时执行的操作
     *
     * @param shopId   店铺id
     * @param shopName 店铺名称
     */
    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void editCouponShopName(Long shopId, String shopName) {
        //修改优惠券的店铺名称
        this.couponMapper.update(null, new UpdateWrapper<CouponDO>()
                .set("seller_name", shopName)
                .eq("seller_id", shopId));
    }

    /**
     * 根据失效状态获取优惠券数据集合
     *
     * @param sellerId
     * @param status   失效状态 0：全部，1：有效，2：失效
     * @return
     */
    @Override
    public List<CouponDO> getByStatus(Long sellerId, Integer status) {
        //获取当前时间
        Long currentTime = DateUtil.getDateline();

        return this.couponMapper.selectList(new QueryWrapper<CouponDO>()
                .eq("seller_id", sellerId)
                //如果状态值为1，则添加条件
                .and(status == 1, e -> {
                    //结束时间>=当前时间
                    e.ge("end_time", currentTime)
                            //活动赠送
                            .eq("type", CouponType.ACTIVITY_GIVE.name())
                            //数量-领取数量>0
                            .last(" and create_num-received_num>0");
                })
                //如果状态值为2，则结束时间<当前时间
                .lt(status == 2, "end_time", currentTime));
    }

    @Override
    public List<CouponDO> selectNoEndCoupon() {
        return new QueryChainWrapper<>(couponMapper).gt("end_time", DateUtil.getDateline()).list();
    }

    /**
     * 验证是否可修改和删除
     *
     * @param id 优惠券主键ID
     */
    private void verifyStatus(Long id) {
        CouponDO couponDO = this.getModel(id);
        long nowTime = DateUtil.getDateline();

        //如果当前时间大于起始时间，小于终止时间，标识活动已经开始了，不可修改和删除。
        if (couponDO.getStartTime().longValue() < nowTime && couponDO.getEndTime().longValue() > nowTime) {
            throw new ServiceException(PromotionErrorCode.E400.code(), "活动已经开始，不能进行编辑删除操作");
        }

        List<FullDiscountDO> list = fullDiscountMapper.selectList(new QueryWrapper<FullDiscountDO>()
                .eq("is_send_bonus", 1)
                .eq("bonus_id", id)
                .eq("seller_id", couponDO.getSellerId())
                .gt("end_time", DateUtil.getDateline()));

        if (list != null && list.size() != 0) {
            String msg = "";
            for (FullDiscountDO full : list) {
                msg += "【" + full.getTitle() + "】";
            }

            throw new ServiceException(PromotionErrorCode.E400.code(), "当前优惠券参与了促销活动" + msg + "，不能进行编辑删除操作");
        }
    }

    /**
     * 参数验证
     *
     * @param startTime 生效时间
     * @param endTime   失效时间
     */
    private void paramValid(Long startTime, Long endTime) {
        // 开始时间不能大于结束时间
        if (startTime.longValue() > endTime.longValue()) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动起始时间不能大于活动结束时间");
        }
    }

}

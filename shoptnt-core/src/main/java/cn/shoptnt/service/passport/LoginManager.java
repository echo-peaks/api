/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.passport;

import cn.shoptnt.model.member.dto.LoginUserDTO;
import cn.shoptnt.model.member.enums.ConnectTypeEnum;

import java.util.Map;

public interface LoginManager {

    /**
     * 根据UnionId登陆
     * @param loginUserDTO
     * @return
     */
    Map loginByUnionId(LoginUserDTO loginUserDTO);
}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.base.plugin.express;

import cn.shoptnt.model.base.vo.ConfigItem;
import cn.shoptnt.model.base.vo.RadioOption;
import cn.shoptnt.model.system.vo.AddressDetailVO;
import cn.shoptnt.service.base.plugin.express.util.HttpRequest;
import cn.shoptnt.service.base.plugin.express.util.MD5;
import cn.shoptnt.client.system.LogiCompanyClient;
import cn.shoptnt.model.system.vo.ExpressDetailVO;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.StringUtil;
import org.checkerframework.checker.units.qual.A;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 快递100 快递实现
 *
 * @author zh
 * @version v7.0
 * @date 18/7/11 下午3:52
 * @since v7.0
 */
@Component
public class Kuaidi100Plugin implements ExpressPlatform {

    @Autowired
    private LogiCompanyClient logiCompanyClient;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public List<ConfigItem> definitionConfigItem() {
        List<ConfigItem> list = new ArrayList<>();
        ConfigItem codeItem = new ConfigItem();
        codeItem.setName("code");
        codeItem.setText("公司代码");
        codeItem.setType("text");

        ConfigItem secretItem = new ConfigItem();
        secretItem.setName("id");
        secretItem.setText("id");
        secretItem.setType("text");

        ConfigItem typeItem = new ConfigItem();
        typeItem.setName("user");
        typeItem.setText("用户类型");
        typeItem.setType("radio");
        //组织用户类型可选项
        List<RadioOption> options = new ArrayList<>();
        RadioOption radioOption = new RadioOption();
        radioOption.setLabel("普通用户");
        radioOption.setValue(0);
        options.add(radioOption);
        radioOption = new RadioOption();
        radioOption.setLabel("企业用户");
        radioOption.setValue(1);
        options.add(radioOption);
        typeItem.setOptions(options);


        ConfigItem analysisItem = new ConfigItem();
        analysisItem.setName("analysis_open");
        analysisItem.setText("地址智能分析");
        analysisItem.setType("radio");

        //组织用户类型可选项
        List<RadioOption> analysisOptions = new ArrayList<>();
        RadioOption analysisRadioOption = new RadioOption();
        analysisRadioOption.setLabel("开启");
        analysisRadioOption.setValue(1);
        analysisOptions.add(analysisRadioOption);
        analysisRadioOption = new RadioOption();
        analysisRadioOption.setLabel("关闭");
        analysisRadioOption.setValue(0);
        analysisOptions.add(analysisRadioOption);
        analysisItem.setOptions(analysisOptions);

        ConfigItem analysisSecretKeyItem = new ConfigItem();
        analysisSecretKeyItem.setName("analysis_secret_key");
        analysisSecretKeyItem.setText("SecretKey");
        analysisSecretKeyItem.setType("text");


        ConfigItem analysisSecretCodeItem = new ConfigItem();
        analysisSecretCodeItem.setName("analysis_secret_code");
        analysisSecretCodeItem.setText("SecretCode");
        analysisSecretCodeItem.setType("text");


        ConfigItem analysisSecretSignItem = new ConfigItem();
        analysisSecretSignItem.setName("analysis_secret_sign");
        analysisSecretSignItem.setText("SecretSign");
        analysisSecretSignItem.setType("text");

        list.add(codeItem);
        list.add(secretItem);
        list.add(typeItem);
        list.add(analysisItem);
        list.add(analysisSecretKeyItem);
        list.add(analysisSecretCodeItem);
        list.add(analysisSecretSignItem);
        return list;
    }

    @Override
    public String getPluginId() {
        return "kuaidi100Plugin";
    }

    @Override
    public String getPluginName() {
        return "快递100";
    }

    @Override
    public Integer getIsOpen() {
        return 0;
    }

    @Override
    public ExpressDetailVO getExpressDetail(String abbreviation, String num, Map config) {
        String url = "";
        //获取快递平台参数
        Integer user = new Double(StringUtil.toDouble(config.get("user"), false)).intValue();
        String code = StringUtil.toString(config.get("code"));
        String id = StringUtil.toString(config.get("id"));
        HashMap<String, String> parms = new HashMap<String, String>(16);
        //根据不同的用户类型调取不同的查询接口
        if (user.equals(1)) {
            url = "http://poll.kuaidi100.com/poll/query.do";
            String param = "{\"com\":\"" + abbreviation + "\",\"num\":\"" + num + "\"}";
            String sign = MD5.encode(param + id + code);
            parms.put("param", param);
            parms.put("sign", sign);
            parms.put("customer", code);
        } else {
            url = "http://api.kuaidi100.com/api?id=" + id + "&nu=" + num + "&com=" + abbreviation + "&muti=1&order=asc";
        }
        try {
            String content = HttpRequest.postData(url, parms, "utf-8").toString();
            Map map = JsonUtil.toMap(content);

            String message = map.get("message").toString();
            if (!"ok".equals(message)) {
                logger.error(content);
                return null;
            }

            ExpressDetailVO expressDetailVO = new ExpressDetailVO();
            expressDetailVO.setData((List<Map>) map.get("data"));
            expressDetailVO.setCourierNum(map.get("nu").toString());
            expressDetailVO.setName(logiCompanyClient.getLogiByCode(map.get("com").toString()).getName());
            return expressDetailVO;
        } catch (Exception e) {
            logger.error("快递查询错误" + e);
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public AddressDetailVO getAddress(String content, Map config) {
        Map<String, String> params = new HashMap();
        params.put("secret_key",  StringUtil.toString(config.get("analysis_secret_key")));
        params.put("secret_code",  StringUtil.toString(config.get("analysis_secret_code")));
        params.put("secret_sign", StringUtil.toString(config.get("analysis_secret_sign")));
        params.put("content", content);
        try {
            String res = HttpRequest.postData("http://cloud.kuaidi100.com/api", params, "utf-8").toString();

            Map map = JsonUtil.toMap(res);

            String message = map.get("message").toString();
            if (!"success".equals(message)) {
                logger.error(res);
                return null;
            }

            JSONObject jsonObj = new JSONObject(res);

            JSONArray dataArray = jsonObj.getJSONArray("data");

            AddressDetailVO addressDetailVO = new AddressDetailVO();

            if (dataArray != null && dataArray.length() > 0) {
                JSONObject dataObj = dataArray.getJSONObject(0);
                //详细地址
                String address = dataObj.getString("address");
                //手机号码
                JSONArray mobileArray = dataObj.getJSONArray("mobile");
                String mobile = mobileArray.getString(0);
                addressDetailVO.setMobile(mobile);

                String name = dataObj.getString("name");

                JSONObject xzqObj = dataObj.getJSONObject("xzq");
                String province = xzqObj.getString("province");
                String city = xzqObj.getString("city");
                String district = xzqObj.getString("district");
                String subArea = xzqObj.getString("subArea");

                addressDetailVO.setProvince(province);
                addressDetailVO.setCity(city);
                addressDetailVO.setCounty(district);
                addressDetailVO.setTown(subArea);
                addressDetailVO.setAddr(address);
                addressDetailVO.setName(name);

            }
            return addressDetailVO;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.payment.plugin.alipay.executor;

import com.alipay.api.AlipayClient;
import com.alipay.api.request.AlipayTradeCancelRequest;
import com.alipay.api.response.AlipayTradeCancelResponse;
import cn.shoptnt.framework.logs.Debugger;
import cn.shoptnt.service.payment.PaymentPluginManager;
import cn.shoptnt.service.payment.plugin.alipay.AlipayPluginConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fk
 * @version v1.0
 * @Description: 支付宝关闭交易
 * @date 2018/4/17 14:55
 * @since v7.0.0
 */
@Service
public class AlipayCloseTradeExcutor extends AlipayPluginConfig {

    @Autowired
    private Debugger debugger;
    @Autowired
    private List<PaymentPluginManager> paymentPluginList;

    /**
     * 关闭交易
     *
     * @param outTradeNo 第三方交易号
     * @param configMap  参数
     * @return
     */
    public void closeTrade(Map<String, String> configMap, String outTradeNo) {
        try {
            debugger.log("基础参数为", configMap.toString());
            //获得初始化的AlipayClient
            AlipayClient alipayClient = buildClient(configMap);
            //设置请求参数
            AlipayTradeCancelRequest alipayRequest = new AlipayTradeCancelRequest();

            Map<String, String> sParaTemp = new HashMap<>(16);
            sParaTemp.put("out_trade_no", outTradeNo);

            debugger.log("请求参数为：", sParaTemp.toString());

            ObjectMapper json = new ObjectMapper();
            //填充业务参数
            alipayRequest.setBizContent(json.writeValueAsString(sParaTemp));

            debugger.log("向支付宝发出请求");
            AlipayTradeCancelResponse response = alipayClient.execute(alipayRequest);

            debugger.log("请求结果：" + response.isSuccess());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

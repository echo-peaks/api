/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.cart.cartbuilder.impl;

import cn.shoptnt.model.trade.cart.vo.CartVO;
import cn.shoptnt.service.trade.cart.cartbuilder.CartShipPriceCalculator;
import cn.shoptnt.service.trade.order.ShippingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/12/19
 */
@Service
public class CartShipPriceCalculatorImpl implements CartShipPriceCalculator {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ShippingManager shippingManager;

    @Override
    public void countShipPrice(List<CartVO> cartList) {
        shippingManager.setShippingPrice(cartList);

        logger.debug("购物车处理运费结果为：",cartList);
    }

}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.pintuan.impl;

import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.service.trade.pintuan.PintuanCartManager;
import cn.shoptnt.model.errorcode.TradeErrorCode;
import cn.shoptnt.model.trade.cart.enums.CartType;
import cn.shoptnt.model.trade.cart.enums.CheckedWay;
import cn.shoptnt.model.trade.cart.vo.CartSkuOriginVo;
import cn.shoptnt.model.trade.cart.vo.CartView;
import cn.shoptnt.service.trade.cart.cartbuilder.*;
import cn.shoptnt.service.trade.cart.cartbuilder.impl.DefaultCartBuilder;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Buyer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by 妙贤 on 2019-01-23.
 * 拼团购物车业务类实现
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-01-23
 */
@Service
public class PintuanCartManagerImpl implements PintuanCartManager {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 购物车促销渲染器
     */
    @Autowired
    @Qualifier(value = "cartPromotionRendererImpl")
    private CartPromotionRenderer cartPromotionRuleRenderer;

    /**
     * 购物车价格计算器
     */
    @Autowired
    @Qualifier(value = "pintuanCartPriceCalculatorImpl")
    private CartPriceCalculator cartPriceCalculator;

    /**
     * 拼团购物车sku数据渲染器
     */
    @Autowired
    @Qualifier(value = "pintuanCartSkuRenderer")
    private CartSkuRenderer pintuanCartSkuRenderer;

    /**
     * 数据校验
     */
    @Autowired
    private CheckDataRebderer checkDataRebderer;

    /**
     * 购物车优惠券渲染器
     */
    @Autowired
    private CartCouponRenderer cartCouponRenderer;

    /**
     * 购物车运费价格计算器
     */
    @Autowired
    private CartShipPriceCalculator cartShipPriceCalculator;


    @Autowired
    private GoodsClient goodsClient;


    @Autowired
    private Cache cache;


    /**
     * 获取拼团购物车
     * @return 购物车视图
     */
    @Override
    public CartView getCart() {
        //调用CartView生产流程线进行生产
        CartBuilder cartBuilder = new DefaultCartBuilder(CartType.PINTUAN, pintuanCartSkuRenderer, cartPromotionRuleRenderer, cartPriceCalculator, cartCouponRenderer, cartShipPriceCalculator, checkDataRebderer);

        //生产流程为：渲染sku->渲染促销规则(计算优惠券）->计算运费->计算价格 -> 渲染优惠券 ->生成成品
        CartView cartView = cartBuilder.renderSku(CheckedWay.CART).countShipPrice().countPrice(false).checkData().build();
        logger.debug("cartView:"+cartView);
        return cartView;
    }

    /**
     * 将一个拼团的sku加入到购物车中
     * @param skuId 商品sku id
     * @param num 加入的数量
     * @return 购物车原始数据
     */
    @SuppressWarnings("Duplicates")
    @Override
    public CartSkuOriginVo addSku(Long skuId, Integer num) {
        CartSkuOriginVo skuVo = new CartSkuOriginVo();
        GoodsSkuVO sku = this.goodsClient.getSkuFromCache(skuId);
        if (sku == null) {
            throw new ServiceException(TradeErrorCode.E451.code(), "商品已失效，请刷新购物车");
        }
        if(num<=0){
            throw new ServiceException(TradeErrorCode.E451.code(), "商品数量不能小于等于0。");
        }

        //读取sku的可用库存
        Integer enableQuantity = sku.getEnableQuantity();
        if (enableQuantity <= 0) {
            throw new ServiceException(TradeErrorCode.E451.code(), "商品库存已不足，不能购买。");
        }

        BeanUtils.copyProperties(sku, skuVo);
        skuVo.setNum(num);
        skuVo.setChecked(1);

        String originKey = this.getOriginKey();

        cache.put(originKey, skuVo);
        logger.debug("将拼团商品加入缓存：" + skuVo);

        return skuVo;
    }

    /**
     * 读取当前会员购物车原始数据key
     *
     * @return
     */
    @SuppressWarnings("Duplicates")
    protected String getOriginKey() {

        String cacheKey = "";
        //如果会员登录了，则要以会员id为key
        Buyer buyer = UserContext.getBuyer();
        if (buyer != null) {
            cacheKey = CachePrefix.CART_SKU_PREFIX.getPrefix() + buyer.getUid();
        }

        return cacheKey;
    }
}

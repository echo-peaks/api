/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.cart.cartbuilder;

import cn.shoptnt.model.trade.cart.vo.CartVO;
import cn.shoptnt.model.trade.cart.vo.CouponVO;

import java.util.List;

/**
 * 购物车优惠券渲染器
 * 文档请参考：<br>
 * <a href="http://doc.shoptnt.cn/current/achitecture/jia-gou/ding-dan/cart-and-checkout.html" >购物车架构</a>
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/12/18
 */
public interface CartCouponRenderer {

    List<CouponVO> render(List<CartVO> cartList);

}

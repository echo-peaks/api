/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.MemberZpzzClient;
import cn.shoptnt.model.member.dos.MemberZpzzDO;
import cn.shoptnt.service.member.MemberZpzzManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 会员赠票资质默认实现
 *
 * @author zs
 * @version v7.0
 * @date 2021-12-21
 * @since v5.2.3
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class MemberZpzzClientDefaultImpl implements MemberZpzzClient {

    @Autowired
    private MemberZpzzManager memberZpzzManager;

    @Override
    public MemberZpzzDO get() {
        return memberZpzzManager.get();
    }
}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.MemberNoticeLogClient;
import cn.shoptnt.model.member.dos.MemberNoticeLog;
import cn.shoptnt.service.member.MemberNoticeLogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;


/**
 * @author fk
 * @version v2.0
 * @Description: 会员站内短消息
 * @date 2018/8/14 10:18
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class MemberNoticeLogClientDefaultImpl implements MemberNoticeLogClient {

    @Autowired
    private MemberNoticeLogManager memberNociceLogManager;

    @Override
    public MemberNoticeLog add(String content, long sendTime, Long memberId, String title) {

        return memberNociceLogManager.add(content, sendTime, memberId, title);
    }

}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system;

import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.pagedata.HotKeyword;

import java.util.List;

/**
 * @author zs
 * @version v1.0
 * @Description: 热点关键字client
 * @date 2020-11-17
 * @since v7.2.2
 */
public interface HotkeywordClient {

    /**
     * 查询热门关键字
     * @param num
     * @return
     */
    List<HotKeyword> listByNum(Integer num);


}

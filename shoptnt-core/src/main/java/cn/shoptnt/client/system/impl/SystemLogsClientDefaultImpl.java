/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.client.system.SystemLogsClient;
import cn.shoptnt.model.system.dos.SystemLogs;
import cn.shoptnt.service.system.SystemLogsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 系统日志client
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:58
 */
@Service
public class SystemLogsClientDefaultImpl implements SystemLogsClient {

    @Autowired
    private SystemLogsManager systemLogsManager;


    @Override
    public void add(SystemLogs systemLogs) {

        systemLogsManager.add(systemLogs);
    }
}

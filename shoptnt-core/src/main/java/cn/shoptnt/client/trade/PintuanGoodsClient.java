/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade;

import java.util.List;

/**
 * @author zh
 * @version v1.0
 * @Description: 拼团索引
 * @date 2019/3/5 10:33
 * @since v7.0.0
 */
public interface PintuanGoodsClient {

    /**
     * 删除拼团商品
     * @param goodsId
     */
    void delete(Long goodsId);

    /**
     * 删除拼团商品
     * @param delSkuIds
     */
    void deletePinTuanGoods(List<Long> delSkuIds);




}

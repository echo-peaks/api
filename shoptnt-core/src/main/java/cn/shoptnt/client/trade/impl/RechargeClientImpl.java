/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade.impl;

import cn.shoptnt.client.trade.RechargeClient;
import cn.shoptnt.model.member.dto.DepositeParamDTO;
import cn.shoptnt.service.trade.deposite.RechargeManager;
import cn.shoptnt.framework.database.WebPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @description: 预存款充值客户端实现
 * @author: liuyulei
 * @create: 2020-01-02 16:43
 * @version:1.0
 * @since:7.1.4
 **/
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class RechargeClientImpl implements RechargeClient {

    @Autowired
    private RechargeManager rechargeManager;

    /**
     * 查询充值记录列表
     * @param paramDTO 搜索参数
     * @return WebPage
     */
    @Override
    public WebPage list(DepositeParamDTO paramDTO) {
        return rechargeManager.list(paramDTO);
    }

}

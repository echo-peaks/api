/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade;


import cn.shoptnt.model.promotion.pintuan.PintuanChildOrder;
import cn.shoptnt.model.promotion.pintuan.PintuanOrderDetailVo;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 拼团订单操作SDK
 *
 * @author fk create in 2020/4/7
 * @version v2.0
 * @since v7.2.0
 */
public interface PintuanOrderClient {

    /**
     * 通过普通订单号查找拼团主订单
     * @param orderSn
     * @return
     */
    PintuanOrderDetailVo getMainOrderBySn(String orderSn);

    /**
     * 取消拼团订单
     * @param orderSn 订单号
     */
    void cancelOrder(String orderSn);


    /**
     * 对一个拼团订单进行支付处理
     * @param order 普通订单
     */
    void payOrder(OrderDO order);


    /**
     * 拼团主id查询相关的拼团订单
     * @param pintuanOrderId
     * @return
     */
    List<PintuanChildOrder> queryChildOrderByOrderId(Long pintuanOrderId);

}

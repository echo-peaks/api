/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.distribution.impl;

import cn.shoptnt.client.distribution.CommissionTplClient;
import cn.shoptnt.model.distribution.dos.CommissionTpl;
import cn.shoptnt.service.distribution.CommissionTplManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * CommissionTplClientDefaultImpl
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-14 下午2:19
 */

@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class CommissionTplClientDefaultImpl  implements CommissionTplClient{
    @Autowired
    private CommissionTplManager commissionTplManager;
    /**
     * 获取默认模版
     *
     * @return
     */
    @Override
    public CommissionTpl getDefaultCommission() {
        return commissionTplManager.getDefaultCommission();
    }
}

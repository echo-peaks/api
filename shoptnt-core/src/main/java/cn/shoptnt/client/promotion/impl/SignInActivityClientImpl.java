package cn.shoptnt.client.promotion.impl;

import cn.shoptnt.client.promotion.SignInActivityClient;
import cn.shoptnt.model.promotion.sign.dos.SignInRecord;
import cn.shoptnt.service.promotion.sign.SignInActivityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignInActivityClientImpl
 * @description <TODO description class>
 * @program: api
 * 2024/3/19 11:46
 */
@Service
public class SignInActivityClientImpl implements SignInActivityClient {

    @Autowired
    private SignInActivityManager signInActivityManager;

    @Override
    public void sendGift(List<SignInRecord> recordList) {
        signInActivityManager.sendGift(recordList);
    }
}

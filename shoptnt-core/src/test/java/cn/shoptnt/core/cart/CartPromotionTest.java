/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.core.cart;

import cn.shoptnt.model.promotion.exchange.dos.ExchangeDO;
import cn.shoptnt.service.promotion.exchange.ExchangeGoodsManager;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyGoodsVO;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyGoodsManager;
import cn.shoptnt.model.promotion.halfprice.vo.HalfPriceVO;
import cn.shoptnt.service.promotion.halfprice.HalfPriceManager;
import cn.shoptnt.model.promotion.minus.vo.MinusVO;
import cn.shoptnt.service.promotion.minus.MinusManager;
import cn.shoptnt.service.trade.cart.CartPromotionManager;
import cn.shoptnt.model.trade.cart.vo.SelectedPromotionVo;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.security.model.Buyer;
import cn.shoptnt.framework.test.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.Mockito.when;

/**
 * Created by 妙贤 on 2018/12/17.
 * 购物车促销测试
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/12/17
 */
public class CartPromotionTest extends BaseTest {

    @Autowired
    private CartPromotionManager cartPromotionManager;

    @MockBean
    private ExchangeGoodsManager exchangeGoodsManager;

    @MockBean
    private GroupbuyGoodsManager groupbuyGoodsManager;

    @MockBean
    private MinusManager minusManager;

    @MockBean
    private HalfPriceManager halfPriceManager;

    @Test
    public void test() {

        //模拟当前用户
        Buyer buyer = new Buyer();
        buyer.setUid(1L);
        UserContext.mockBuyer = buyer;

        //模拟一个积分兑换活动
        ExchangeDO exchangeDO = SkuMocker.mockExchangeDO();

        //模拟一个团购活动
        GroupbuyGoodsVO groupbuyGoodsVO = SkuMocker.mockGroupBuyVO();

        //模拟一个第二件半价活动
        HalfPriceVO halfPriceVO =  SkuMocker.mockHalfPriceVO();

        //模拟一个单品立即减活动
        MinusVO minusVO = SkuMocker.createMinusVO();


        when(exchangeGoodsManager.getModel(1L)).thenReturn(exchangeDO);
        when(groupbuyGoodsManager.getModel(2L)).thenReturn(groupbuyGoodsVO);
        when(halfPriceManager.getFromDB(3L)).thenReturn(halfPriceVO);
        when(minusManager.getFromDB(4L)).thenReturn(minusVO);


        long sellerId1 = 1;
        long sellerId2 = 2;

        //给这个店铺来两个带活动的商品
//        cartPromotionManager.usePromotion(sellerid1, 1, 1, PromotionTypeEnum.EXCHANGE);
//        cartPromotionManager.usePromotion(sellerid1, 2, 1, PromotionTypeEnum.EXCHANGE);
//
//        //第二个店铺有一个商品
//        cartPromotionManager.usePromotion(sellerid2, 3, 1, PromotionTypeEnum.EXCHANGE);


        //看看现在的活动情况
        
        SelectedPromotionVo selectedPromotionVo = cartPromotionManager.getSelectedPromotion();
        

        //删除一个看看
        
        cartPromotionManager.delete(new Long[]{1L});
        selectedPromotionVo = cartPromotionManager.getSelectedPromotion();
        

        //再删除一个看看
        
        cartPromotionManager.delete(new Long[]{3L});
        selectedPromotionVo = cartPromotionManager.getSelectedPromotion();
        


        //再加上sku1,然后变换活动
        
//        cartPromotionManager.usePromotion(sellerid1, 1, 4, PromotionTypeEnum.MINUS);
//        cartPromotionManager.usePromotion(sellerid1, 1, 2,PromotionTypeEnum.GROUPBUY );
        selectedPromotionVo = cartPromotionManager.getSelectedPromotion();
        

        
        cartPromotionManager.clean();
        selectedPromotionVo = cartPromotionManager.getSelectedPromotion();
        

    }

}

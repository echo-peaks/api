/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.statistics.enums.RegionsDataType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 商家中心，运营报告管理业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/08/05
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class ReportsStatisticManagerTest {

    @Autowired
    private ReportsStatisticsManager reportsStatisticsManager;

    @Test
    public void countShop() {

        
        
        
    }

    @Test
    public void getSalesNum() {

        
        
        
    }

    @Test
    public void getSalesPage() {

        
        
        
    }

    @Test
    public void getSalesSummary() {

        
        
        
    }

    @Test
    public void regionsMap() {


        String type1 = RegionsDataType.ORDER_MEMBER_NUM.value();
        String type2 = RegionsDataType.ORDER_PRICE.value();
        String type3 = RegionsDataType.ORDER_NUM.value();

        List list1 = reportsStatisticsManager.regionsMap(getSearchCriteria3(), type1);
        List list2 = reportsStatisticsManager.regionsMap(getSearchCriteria3(), type2);
        List list3 = reportsStatisticsManager.regionsMap(getSearchCriteria3(), type3);
        List list4 = reportsStatisticsManager.regionsMap(getSearchCriteria1(), type3);
        List list5 = reportsStatisticsManager.regionsMap(getSearchCriteria2(), type3);

        
        
        
        
        
    }

    @Test
    public void orderDistribution() {

        Integer[] prices = {0, 10, 20};

        
        
        
    }

    @Test
    public void purchasePeriod() {

        
        
        
    }


    private SearchCriteria getSearchCriteria1(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType("YEAR");
        searchCriteria.setYear(2019);
        searchCriteria.setMonth(12);
        searchCriteria.setCategoryId(0l);
        searchCriteria.setSellerId(17l);

        return searchCriteria;
    }

    private SearchCriteria getSearchCriteria2(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType("YEAR");
        searchCriteria.setYear(2020);
        searchCriteria.setMonth(12);
        searchCriteria.setCategoryId(555l);
        searchCriteria.setSellerId(17l);

        return searchCriteria;
    }

    private SearchCriteria getSearchCriteria3(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType("YEAR");
        searchCriteria.setYear(2020);
        searchCriteria.setMonth(12);
        searchCriteria.setCategoryId(0l);
        searchCriteria.setSellerId(17l);

        return searchCriteria;
    }

}

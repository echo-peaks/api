/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.model.trade.order.dto.OrderQueryParam;
import cn.shoptnt.model.trade.order.enums.CommentStatusEnum;
import cn.shoptnt.model.trade.order.vo.OrderFlowNode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 订单相关业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/08/10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class OrderQueryManagerTest {

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Test
    public void list() {

        OrderQueryParam paramDTO = new OrderQueryParam();
        paramDTO.setPageNo(1l);
        paramDTO.setPageSize(10l);

        
    }

    @Test
    public void export() {

        OrderQueryParam paramDTO = new OrderQueryParam();
        paramDTO.setPageNo(1l);
        paramDTO.setPageSize(10l);

        

    }

    @Test
    public void getOrderStatusNum() {

        OrderQueryParam paramDTO = new OrderQueryParam();
        paramDTO.setPageNo(1l);
        paramDTO.setPageSize(10l);

        

    }

    @Test
    public void getOrderNumByMemberId() {

        
    }

    @Test
    public void getOrderCommentNumByMemberId() {

        
    }

    @Test
    public void getOrderByTradeSn() {

        
    }

    @Test
    public void getModel() {

        
    }

    @Test
    public void listOrderByGoods() {

        
    }

    @Test
    public void getItemsPromotionTypeandNum() {

        
    }

    @Test
    public void getOrderFlow() {

        List<OrderFlowNode> orderFlow = orderQueryManager.getOrderFlow("43181208032784395");

        
    }

}

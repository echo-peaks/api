/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.member.MemberHistoryDispatcher;
import cn.shoptnt.model.member.dto.HistoryDTO;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 会员足迹
 *
 * @author zh
 * @version v7.0
 * @date 19/07/05 下午4:17
 * @since v7.0
 */
@Component
public class MemberHistoryReceiver {

    @Autowired
    private MemberHistoryDispatcher dispatcher;


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMBER_HISTORY + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMBER_HISTORY, type = ExchangeTypes.FANOUT)
    ))
    public void createIndexPage(HistoryDTO historyDTO) {
        dispatcher.dispatch(historyDTO);
    }
}

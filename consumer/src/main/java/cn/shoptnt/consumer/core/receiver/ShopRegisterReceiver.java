package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.shop.ShopRegisterDispatcher;
import cn.shoptnt.model.shop.vo.ShopRegisterMsg;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 商家入驻消息接收类
 *
 * @author dmy
 * @version 1.0
 * @since 5.2.3
 * 2022-05-07
 */
@Component
public class ShopRegisterReceiver {

    @Autowired
    private ShopRegisterDispatcher dispatcher;

    /**
     * 商家入驻
     *
     * @param shopRegisterMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SHOP_REGISTER + "_ROUTING"),
            exchange = @Exchange(value = AmqpExchange.SHOP_REGISTER, type = ExchangeTypes.FANOUT)
    ))
    public void shopChange(ShopRegisterMsg shopRegisterMsg) {
        dispatcher.dispatch(shopRegisterMsg);
    }
}

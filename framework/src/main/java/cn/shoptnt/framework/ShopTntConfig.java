/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * shoptnt配置
 *
 * @author zh
 * @version v7.0
 * @date 18/4/13 下午8:19
 * @since v7.0
 */
@Configuration
@ConfigurationProperties(prefix = "shoptnt")
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@SuppressWarnings("ConfigurationProperties")
public class ShopTntConfig {

    /**
     * token加密秘钥
     */
    private String tokenSecret;

    /**
     * 访问token失效时间
     */
    @Value("${shoptnt.timeout.accessTokenTimeout:#{null}}")
    private Integer accessTokenTimeout;

    @Value("${shoptnt.timeout.refreshTokenTimeout:#{null}}")
    private Integer refreshTokenTimeout;

    @Value("${shoptnt.timeout.captchaTimout:#{null}}")
    private Integer captchaTimout;

    @Value("${shoptnt.timeout.smscodeTimout:#{null}}")
    private Integer smscodeTimout;

    @Value("${shoptnt.isDemoSite:#{false}}")
    private boolean isDemoSite;

    @Value("${shoptnt.ssl:#{false}}")
    private boolean ssl;

    @Value("${shoptnt.sign:#{false}}")
    private boolean sign;

    @Value("${shoptnt.debugger:#{false}}")
    private boolean debugger;

    /**
     * 缓冲次数
     */
    @Value("${shoptnt.pool.max-update-timet:#{null}}")
    private Integer maxUpdateTime;

    /**
     * 缓冲区大小
     */
    @Value("${shoptnt.pool.max-pool-size:#{null}}")
    private Integer maxPoolSize;

    /**
     * 缓冲时间（秒数）
     */
    @Value("${shoptnt.pool.max-lazy-second:#{null}}")
    private Integer maxLazySecond;

    /**
     * 商品库存缓冲池开关
     * false：关闭（如果配置文件中没有配置此项，则默认为false）
     * true：开启（优点：缓解程序压力；缺点：有可能会导致商家中心商品库存数量显示延迟；）
     */
    @Value("${shoptnt.pool.stock:#{false}}")
    private boolean stock;

    /**
     * referer域名集合
     */
    @Value("${shoptnt.referer:#{null}}")
    private List<String> referer;

    /**
     * 服务气端即数据库敏感信息是否加密存储
     */
    @Value("${shoptnt.encrypt.setting:#{false}}")
    private boolean encryptSetting;

    @Value("${shoptnt.runmode:#{'standalone'}}")
    private String runmode;

    public ShopTntConfig() {
    }


    @Override
    public String toString() {
        return "ShopTntConfig{" +
                "accessTokenTimeout=" + accessTokenTimeout +
                ", refreshTokenTimeout=" + refreshTokenTimeout +
                ", captchaTimout=" + captchaTimout +
                ", smscodeTimout=" + smscodeTimout +
                ", isDemoSite=" + isDemoSite +
                ", ssl=" + ssl +
                ", maxUpdateTime=" + maxUpdateTime +
                ", maxPoolSize=" + maxPoolSize +
                ", maxLazySecond=" + maxLazySecond +
                ", stock=" + stock +
                '}';
    }

    /**
     * 获取协议
     *
     * @return 协议
     */
    public final String getScheme() {
        if (this.getSsl()) {
            return "https://";
        }
        return "http://";
    }

    public boolean isEncryptSetting() {
        return encryptSetting;
    }

    public void setEncryptSetting(boolean encryptSetting) {
        this.encryptSetting = encryptSetting;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    public boolean isDemoSite() {
        return isDemoSite;
    }

    public boolean getSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    public Integer getAccessTokenTimeout() {
        return accessTokenTimeout;
    }

    public void setAccessTokenTimeout(Integer accessTokenTimeout) {
        this.accessTokenTimeout = accessTokenTimeout;
    }

    public Integer getRefreshTokenTimeout() {
        return refreshTokenTimeout;
    }

    public void setRefreshTokenTimeout(Integer refreshTokenTimeout) {
        this.refreshTokenTimeout = refreshTokenTimeout;
    }

    public Integer getCaptchaTimout() {
        return captchaTimout;
    }

    public void setCaptchaTimout(Integer captchaTimout) {
        this.captchaTimout = captchaTimout;
    }

    public Integer getSmscodeTimout() {
        return smscodeTimout;
    }

    public void setSmscodeTimout(Integer smscodeTimout) {
        this.smscodeTimout = smscodeTimout;
    }

    public boolean getIsDemoSite() {
        return isDemoSite;
    }

    public void setDemoSite(boolean demoSite) {
        isDemoSite = demoSite;
    }

    public Integer getMaxUpdateTime() {
        return maxUpdateTime;
    }

    public void setMaxUpdateTime(Integer maxUpdateTime) {
        this.maxUpdateTime = maxUpdateTime;
    }

    public Integer getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(Integer maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public Integer getMaxLazySecond() {
        return maxLazySecond;
    }

    public void setMaxLazySecond(Integer maxLazySecond) {
        this.maxLazySecond = maxLazySecond;
    }

    public boolean isStock() {
        return stock;
    }

    public void setStock(boolean stock) {
        this.stock = stock;
    }

    public boolean isSsl() {
        return ssl;
    }

    public boolean isDebugger() {
        return debugger;
    }

    public void setDebugger(boolean debugger) {
        this.debugger = debugger;
    }

    public List<String> getReferer() {
        return referer;
    }

    public void setReferer(List<String> referer) {
        this.referer = referer;
    }

    public boolean isSign() {
        return sign;
    }

    public void setSign(boolean sign) {
        this.sign = sign;
    }

    public String getRunmode() {
        return runmode;
    }

    public void setRunmode(String runmode) {
        this.runmode = runmode;
    }
}

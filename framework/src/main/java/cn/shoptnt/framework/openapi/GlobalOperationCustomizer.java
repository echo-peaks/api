package cn.shoptnt.framework.openapi;

import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.media.StringSchema;
import io.swagger.v3.oas.models.parameters.HeaderParameter;
import org.springdoc.core.customizers.OperationCustomizer;
import org.springframework.web.method.HandlerMethod;

/**
 * 全局默认操作定制器
 * 自定义默认OpenAPI描述的操作
 * 暂时只定义了全局的header参数  Authorization 用于传请求的token
 *
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2022年12月08日 16:37
 */
public class GlobalOperationCustomizer implements OperationCustomizer {
    @Override
    public Operation customize(Operation operation, HandlerMethod handlerMethod) {

        return operation.addParametersItem(
                //用于传请求的token
                new HeaderParameter().name("Authorization")
                        .description("令牌")
                        .schema(new StringSchema()._default("BR")
                                .name("Authorization")
                                .description("令牌")));
    }
}

package cn.shoptnt.framework.lock.cluster;

import cn.shoptnt.framework.lock.Lock;
import org.redisson.api.RLock;

/**
 * redisson lock的实现
 * @author kingapex
 * @version 1.0
 * @data 2022/10/21 19:03
 **/
public class RedissonLock implements Lock {

    private RLock rLock;

    public RedissonLock(RLock rLock) {
        this.rLock = rLock;
    }

    @Override
    public void lock() {
        rLock.lock();
    }

    @Override
    public void unlock() {
        rLock.unlock();
    }
}

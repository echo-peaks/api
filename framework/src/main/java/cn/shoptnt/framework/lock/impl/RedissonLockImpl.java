package cn.shoptnt.framework.lock.impl;

import cn.shoptnt.framework.lock.GlobalLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.Lock;

/**
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2022年10月14日 17:00
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "cluster")
public class RedissonLockImpl implements GlobalLock {

    @Autowired
    private RedissonClient redissonClient;


    @Override
    public Lock getLock(String name) {
        return redissonClient.getLock(name);
    }

}

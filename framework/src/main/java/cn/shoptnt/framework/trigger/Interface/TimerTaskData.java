package cn.shoptnt.framework.trigger.Interface;

import cn.shoptnt.framework.util.DateUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;

import java.io.Serializable;
import java.util.TimerTask;

/**
 * 延时任务
 *
 * @author kingapex
 * @version 1.0
 * @data 2022/11/4 16:15
 **/
@TableName("es_timer_task")
public class TimerTaskData implements Serializable, Cloneable {

    private static JdkSerializationRedisSerializer serializer = new JdkSerializationRedisSerializer();

    public TimerTaskData() {
    }

    public TimerTaskData(Long triggerTime, String uniqueKey, TimerTask timerTask) {
        this.triggerTime = triggerTime;
        this.uniqueKey = uniqueKey;
        this.timerTask = timerTask;
        this.updateTime = DateUtil.getDateline();

        //将timerTask序列化，以便入库
        byte[] bytes = serializer.serialize(timerTask);
        this.taskObject = bytes;
    }

    /**
     * 根据taskObject bytes转为timerTask 对象
     * @return
     */
    public TimerTask convertTimerTask() {

        if (this.taskObject == null) {
            return null;
        }

        this.timerTask = (TimerTask) serializer.deserialize(this.taskObject);
        return timerTask;
    }

    /**
     * 主键
     */
    @TableId
    private String id;
    /**
     * 触发时间
     */
    private Long triggerTime;
    /**
     * 唯一key
     */
    private String uniqueKey;
    /**
     * task对象字节码
     */
    @TableField(typeHandler = org.apache.ibatis.type.BlobTypeHandler.class)
    private byte[] taskObject;
    /**
     * 更新时间
     */
    private Long updateTime;

    @TableField(exist = false)
    private TimerTask timerTask;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTriggerTime() {
        return this.triggerTime;
    }

    public void setTriggerTime(Long triggerTime) {
        this.triggerTime = triggerTime;
    }

    public String getUniqueKey() {
        return this.uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    public byte[] getTaskObject() {
        return taskObject;
    }

    public void setTaskObject(byte[] taskObject) {
        this.taskObject = taskObject;
    }

    public Long getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public TimerTask getTimerTask() {
        return timerTask;
    }


    public void setTimerTask(TimerTask timerTask) {
        this.timerTask = timerTask;
    }

    @Override
    public String toString() {
        return "TimerTaskData{" +
                "id='" + id + '\'' +
                ", triggerTime=" + triggerTime +
                ", uniqueKey='" + uniqueKey + '\'' +
                ", updateTime=" + updateTime +
                ", timerTask=" + timerTask +
                '}';
    }
}
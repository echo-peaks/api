/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.elasticsearch;

/**
 *
 * es索引设置
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-03-21
 */

public class EsSettings {

    /**
     * 商品索引后缀
     */
    public static final String GOODS_INDEX_NAME="goods";


    /**
     * 拼团索引后缀
     */
    public static final String PINTUAN_INDEX_NAME="pt";



    /**
     * 日志索引前缀
     */
    public static  final String LOG_INDEX_NAME = "log-index-";


    /**
     * 7.4 默认不在支持指定索引类型，默认索引类型是_doc（隐含：include_type_name=false），为实现兼容性默认type_name=_doc
     * <p>见官网：https://www.elastic.co/guide/en/elasticsearch/reference/current/removal-of-types.html#_typeless_apis_in_7_0</p>
     */
    public static final String TYPE_NAME = "_doc";


}

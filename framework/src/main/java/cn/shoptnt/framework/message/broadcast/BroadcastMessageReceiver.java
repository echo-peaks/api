/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.message.broadcast;

/**
 * 敏感词消息接收接口
 *
 * @author fk
 * @version 2.0
 * @since 7.1.5
 * 2019-09-07 18：00
 */
public interface BroadcastMessageReceiver {

    /**
     * 管道名称
     * @return
     */
    String getChannelName();

    /**
     * 收到消息执行的方法
     * @param message
     */
    void receiveMsg(BroadcastMessage message);

}

${Ansi.CYAN}          _____  _    _   ____   _____  _______  _   _  _______
${Ansi.CYAN}          / ____|| |  | | / __ \ |  __ \|__   __|| \ | ||__   __|
${Ansi.CYAN}        | (___  | |__| || |  | || |__) |  | |   |  \| |   | |
${Ansi.CYAN}         \___ \ |  __  || |  | ||  ___/   | |   | . ` |   | |
${Ansi.CYAN}          ____) || |  | || |__| || |       | |   | |\  |   | |
${Ansi.CYAN}         |_____/ |_|  |_| \____/ |_|       |_|   |_| \_|   |_|


${AnsiColor.WHITE} Running Spring Boot ${spring-boot.version}

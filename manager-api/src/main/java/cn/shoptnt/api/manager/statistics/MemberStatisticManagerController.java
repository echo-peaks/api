/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.statistics;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.MemberStatisticManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 会员统计分析
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/4/28 下午5:10
 */
@RestController
@Tag(name = "后台-》会员分析-》")
@RequestMapping("/admin/statistics/member")
public class MemberStatisticManagerController {

    @Autowired
    private MemberStatisticManager memberAnalysisManager;

    /**
     * 后台-》会员分析-》新增会员统计
     *
     * @param searchCriteria
     * @return
     */
    @Operation(summary = "新增会员统计")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份",  in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份",  in = ParameterIn.QUERY)
    })
    @GetMapping(value = "/increase/member")
    public SimpleChart adminIncreaseMember(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.memberAnalysisManager.getIncreaseMember(searchCriteria);
    }

    @Operation(summary = "新增会员统计 page")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份",  in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份",  in = ParameterIn.QUERY)
    })
    @GetMapping(value = "/increase/member/page")
    public WebPage adminIncreaseMemberPage(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.memberAnalysisManager.getIncreaseMemberPage(searchCriteria);
    }

    @Operation(summary = "会员下单量统计-》下单量")
    @GetMapping(value = "/order/quantity")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份",  in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份",  in = ParameterIn.QUERY),
            @Parameter(name = "seller_id",description = "店铺id", in = ParameterIn.QUERY)
    })
    public SimpleChart adminMemberOrderQuantity(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.memberAnalysisManager.getMemberOrderQuantity(searchCriteria);
    }


    @Operation(summary = "会员下单量统计-》下单量 page")
    @GetMapping(value = "/order/quantity/page")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份",  in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份",  in = ParameterIn.QUERY),
            @Parameter(name = "seller_id",description = "店铺id", in = ParameterIn.QUERY)
    })
    public WebPage adminMemberOrderQuantityPage(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.memberAnalysisManager.getMemberOrderQuantityPage(searchCriteria);
    }


    @Operation(summary = "会员下单量统计-》下单商品数")
    @GetMapping(value = "/order/goods/num")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份",  in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份",  in = ParameterIn.QUERY),
            @Parameter(name = "seller_id",description = "店铺id", in = ParameterIn.QUERY)
    })
    public SimpleChart adminMemberGoodsNum(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.memberAnalysisManager.getMemberGoodsNum(searchCriteria);
    }


    @Operation(summary = "会员下单量统计-》下单商品数page")
    @GetMapping(value = "/order/goods/num/page")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份",  in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份",  in = ParameterIn.QUERY),
            @Parameter(name = "seller_id",description = "店铺id", in = ParameterIn.QUERY)
    })
    public WebPage adminMemberGoodsNumPage(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.memberAnalysisManager.getMemberGoodsNumPage(searchCriteria);
    }

    @Operation(summary = "会员下单量统计-》下单金额")
    @GetMapping(value = "/order/money")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份",  in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份",  in = ParameterIn.QUERY),
            @Parameter(name = "seller_id",description = "店铺id", in = ParameterIn.QUERY)
    })
    public SimpleChart adminMemberMoney(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.memberAnalysisManager.getMemberMoney(searchCriteria);
    }

    @Operation(summary = "下单金额page")
    @GetMapping(value = "/order/money/page")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型",   in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份",  in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份",  in = ParameterIn.QUERY),
            @Parameter(name = "seller_id",description = "店铺id", in = ParameterIn.QUERY)
    })
    public WebPage adminMemberMoneyPage(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.memberAnalysisManager.getMemberMoneyPage(searchCriteria);
    }

}

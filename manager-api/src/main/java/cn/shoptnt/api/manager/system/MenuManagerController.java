/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.system.vo.MenuVO;
import cn.shoptnt.model.system.vo.MenusVO;
import cn.shoptnt.framework.util.BeanUtil;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;

import cn.shoptnt.model.system.dos.Menu;
import cn.shoptnt.service.system.MenuManager;

import java.util.List;

/**
 * 菜单管理控制器
 *
 * @author zh
 * @version v7.0
 * @since v7.0.0
 * 2018-06-19 09:46:02
 */
@RestController
@RequestMapping("/admin/systems/menus")
@Tag(name = "菜单管理相关API")
public class MenuManagerController {

    @Autowired
    private MenuManager menuManager;


    @Operation(summary = "根据父id查询所有菜单")
    @Parameters({
            @Parameter(name = "parent_id", description = "菜单父id，如果查询顶级菜单则传0", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping("/{parent_id}/children")
    public List<MenusVO> getMenuTree(@PathVariable("parent_id") @Parameter(hidden = true) Long parentId) {
        return this.menuManager.getMenuTree(parentId);
    }


    @Operation(summary = "添加菜单")
    @PostMapping
    public Menu add(@Valid MenuVO menu) {
        return this.menuManager.add(menu);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改菜单")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public Menu edit(@Valid MenuVO menuVO, @PathVariable Long id) {
        Menu menu = new Menu();
        BeanUtil.copyProperties(menuVO, menu);
        return this.menuManager.edit(menu, id);
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除菜单")
    @Parameters({
            @Parameter(name = "id", description = "要删除的菜单管理主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.menuManager.delete(id);
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个菜单")
    @Parameters({
            @Parameter(name = "id", description = "要查询的菜单管理主键", required = true,  in = ParameterIn.PATH)
    })
    public Menu get(@PathVariable Long id) {
        Menu menu = this.menuManager.getModel(id);
        return menu;
    }

}

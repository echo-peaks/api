/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goods;

import cn.shoptnt.model.goods.dos.CategoryBrandDO;
import cn.shoptnt.model.goods.dos.CategoryDO;
import cn.shoptnt.model.goods.dos.CategorySpecDO;
import cn.shoptnt.model.goods.vo.ParameterGroupVO;
import cn.shoptnt.model.goods.vo.SelectVO;
import cn.shoptnt.service.goods.BrandManager;
import cn.shoptnt.service.goods.CategoryManager;
import cn.shoptnt.service.goods.ParameterGroupManager;
import cn.shoptnt.service.goods.SpecificationManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 商品分类控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-15 17:22:06
 */
@RestController
@RequestMapping("/admin/goods/categories")
@Tag(name = "商品分类相关API")
@Validated
public class CategoryManagerController {

    @Autowired
    private CategoryManager categoryManager;

    @Autowired
    private BrandManager brandManager;

    @Autowired
    private SpecificationManager specificationManager;

    @Autowired
    private ParameterGroupManager parameterGroupManager;

    @Operation(summary = "查询某分类下的子分类列表")
    @Parameters({
            @Parameter(name = "parent_id", description = "父id，顶级为0", required = true, in = ParameterIn.PATH),
            @Parameter(name = "format", description = "类型，如果值是plugin则查询插件使用的格式数据", in = ParameterIn.QUERY),})
    @GetMapping(value = "/{parent_id}/children")
    public List list(@Parameter(hidden = true) @PathVariable("parent_id") Long parentId, @Parameter(hidden = true) String format) {

        List list = this.categoryManager.list(parentId, format, null);

        return list;
    }

    @Operation(summary = "查询某分类下的全部子分类列表")
    @Parameter(name = "parent_id", description = "父id，顶级为0", required = true, in = ParameterIn.PATH)
    @GetMapping(value = "/{parent_id}/all-children")
    public List list(@Parameter(hidden = true) @PathVariable("parent_id") Long parentId) {

        List list = this.categoryManager.listAllChildren(parentId);

        return list;
    }

    @Operation(summary = "添加商品分类")
    @PostMapping
    public CategoryDO add(@Valid CategoryDO category) {

        this.categoryManager.add(category);

        return category;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改商品分类")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true, in = ParameterIn.PATH)})
    public CategoryDO edit(@Valid CategoryDO category, @Parameter(hidden = true) @PathVariable Long id) {

        this.categoryManager.edit(category, id);

        return category;
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除商品分类")
    @Parameters({
            @Parameter(name = "id", description = "要删除的商品分类主键", required = true, in = ParameterIn.PATH)})
    public String delete(@PathVariable Long id) {

        this.categoryManager.delete(id);

        return "";
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个商品分类")
    @Parameters({
            @Parameter(name = "id", description = "要查询的商品分类主键", required = true, in = ParameterIn.PATH)})
    public CategoryDO get(@PathVariable Long id) {

        CategoryDO category = this.categoryManager.getModel(id);

        return category;
    }

    @Operation(summary = "查询某个分类绑定的品牌")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id", required = true, in = ParameterIn.PATH)})
    @GetMapping(value = "/{category_id}/brands/selected")
    public List<SelectVO> getCatBrand(@PathVariable("category_id") Long categoryId) {

        List<SelectVO> brands = brandManager.getCatBrand(categoryId);

        return brands;
    }

    @Operation(summary = "查询分类品牌")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id", required = true, in = ParameterIn.PATH),
            @Parameter(name = "brand_name", description = "要查询的品牌的名称", required = true, in = ParameterIn.PATH)})
    @GetMapping(value = "/{category_id}/brands")
    public List<SelectVO> searchBrand(@PathVariable("category_id") Long categoryId, @NotEmpty String brandName) {

        List<SelectVO> brands = brandManager.searchBrand(categoryId, brandName);

        return brands;
    }

    @Operation(summary = "管理员操作分类绑定品牌")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id", required = true, in = ParameterIn.PATH),
            @Parameter(name = "choose_brands", description = "品牌id数组", required = true, in = ParameterIn.QUERY)})
    @PutMapping(value = "/{category_id}/brands")
    public List<CategoryBrandDO> saveBrand(@PathVariable("category_id") Long categoryId, @Parameter(hidden = true) @RequestParam(value = "choose_brands") Long[] chooseBrands) {
        return this.categoryManager.saveBrand(categoryId, chooseBrands);
    }

    @Operation(summary = "查询分类规格")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id", required = true, in = ParameterIn.PATH),})
    @GetMapping(value = "/{category_id}/specs")
    public List<SelectVO> getCatSpecs(@PathVariable("category_id") Long categoryId) {

        List<SelectVO> brands = specificationManager.getCatSpecification(categoryId);

        return brands;

    }

    @Operation(summary = "管理员操作分类绑定规格")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id", required = true, in = ParameterIn.PATH),
            @Parameter(name = "choose_specs", description = "规格id数组", required = true, in = ParameterIn.QUERY),})
    @PutMapping(value = "/{category_id}/specs")
    public List<CategorySpecDO> saveSpec(@PathVariable("category_id") Long categoryId, @Parameter(hidden = true) @RequestParam(value = "choose_specs") Long[] chooseSpecs) {

        return this.categoryManager.saveSpec(categoryId, chooseSpecs);
    }

    @Operation(summary = "查询分类参数")
    @Parameters({
            @Parameter(name = "category_id", description = "分类id", required = true, in = ParameterIn.PATH),})
    @GetMapping(value = "/{category_id}/param")
    public List<ParameterGroupVO> getCatParam(@PathVariable("category_id") Long categoryId) {

        return parameterGroupManager.getParamsByCategory(categoryId);
    }

}

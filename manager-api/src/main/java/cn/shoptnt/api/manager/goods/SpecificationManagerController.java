/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goods;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.dos.SpecificationDO;
import cn.shoptnt.service.goods.SpecificationManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 规格项控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-20 09:31:27
 */
@RestController
@RequestMapping("/admin/goods/specs")
@Tag(name = "规格项相关API")
public class SpecificationManagerController {

	@Autowired
	private SpecificationManager specificationManager;

	@Operation(summary = "查询规格项列表")
	@Parameters({
			@Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
			@Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
			@Parameter(name = "keyword", description = "规格名称",   in = ParameterIn.QUERY)
			})
	@GetMapping
	public WebPage list(@Parameter(hidden = true) @NotEmpty(message = "页码不能为空") Long pageNo,
                        @Parameter(hidden = true) @NotEmpty(message = "每页数量不能为空") Long pageSize, String keyword) {

		return this.specificationManager.list(pageNo, pageSize, keyword);
	}

	@Operation(summary = "添加规格项")
	@PostMapping
	public SpecificationDO add(@Valid SpecificationDO specification) {
		// 平台的规格
		specification.setSellerId(0L);
		this.specificationManager.add(specification);

		return specification;
	}

	@PutMapping(value = "/{id}")
	@Operation(summary = "修改规格项")
	@Parameters({
			@Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH) })
	public SpecificationDO edit(@Valid SpecificationDO specification, @PathVariable Long id) {

		this.specificationManager.edit(specification, id);

		return specification;
	}

	@DeleteMapping(value = "/{ids}")
	@Operation(summary = "删除规格项")
	@Parameters({
			@Parameter(name = "ids", description = "要删除的规格项主键", required = true,  in = ParameterIn.PATH) })
	public String delete(@PathVariable Long[] ids) {

		this.specificationManager.delete(ids);

		return "";
	}

	@GetMapping(value = "/{id}")
	@Operation(summary = "查询一个规格项")
	@Parameters({
			@Parameter(name = "id", description = "要查询的规格项主键", required = true,  in = ParameterIn.PATH) })
	public SpecificationDO get(@PathVariable Long id) {

		SpecificationDO specification = this.specificationManager.getModel(id);

		return specification;
	}

}

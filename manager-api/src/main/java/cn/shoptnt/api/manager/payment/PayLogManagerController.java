/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.payment;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.trade.order.dos.PayLog;
import cn.shoptnt.model.trade.order.dto.PayLogQueryParam;
import cn.shoptnt.service.payment.PayLogManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import java.util.List;

/**
 * 付款单相关API
 * @author Snow create in 2018/7/18
 * @version v2.0
 * @since v7.0.0
 */
@Tag(name = "付款单相关API")
@RestController
@RequestMapping("/admin/trade/orders")
@Validated
public class PayLogManagerController {

    @Autowired
    private PayLogManager payLogManager;

    @Operation(summary = "查询付款单列表")
    @GetMapping("/pay-log")
    public WebPage<PayLog> list(PayLogQueryParam param) {

        WebPage page = this.payLogManager.list(param);

        return page;
    }


    @Operation(summary = "收款单导出Excel")
    @GetMapping(value = "/pay-log/list")
    public List<PayLog> excel(PayLogQueryParam param) {



        return payLogManager.exportExcel(param);
    }


}

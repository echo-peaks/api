/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.base.dos.SensitiveWords;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;

import cn.shoptnt.service.system.SensitiveWordsManager;

/**
 * 敏感词控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-08-02 11:30:59
 */
@RestController
@RequestMapping("/admin/sensitive-words")
@Tag(name = "敏感词相关API")
public class SensitiveWordsManagerController {

    @Autowired
    private SensitiveWordsManager sensitiveWordsManager;


    @Operation(summary = "查询敏感词列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, String keyword) {

        return this.sensitiveWordsManager.list(pageNo, pageSize, keyword);
    }


    @Operation(summary = "添加敏感词")
    @PostMapping
    public SensitiveWords add(@Valid SensitiveWords sensitiveWords) {

        this.sensitiveWordsManager.add(sensitiveWords);

        return sensitiveWords;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除敏感词")
    @Parameters({
            @Parameter(name = "id", description = "要删除的敏感词主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.sensitiveWordsManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个敏感词")
    @Parameters({
            @Parameter(name = "id", description = "要查询的敏感词主键", required = true,  in = ParameterIn.PATH)
    })
    public SensitiveWords get(@PathVariable Long id) {

        SensitiveWords sensitiveWords = this.sensitiveWordsManager.getModel(id);

        return sensitiveWords;
    }

}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goods;

import cn.shoptnt.model.support.validator.annotation.SortType;
import cn.shoptnt.model.goods.dos.ParametersDO;
import cn.shoptnt.service.goods.ParametersManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;

/**
 * 参数控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-20 16:14:31
 */
@RestController
@RequestMapping("/admin/goods/parameters")
@Tag(name = "参数相关API")
@Validated
public class ParametersManagerController {

    @Autowired
    private ParametersManager parametersManager;

    @Operation(summary = "添加参数")
    @PostMapping
    public ParametersDO add(@Valid ParametersDO parameters) {

        this.parametersManager.add(parameters);

        return parameters;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改参数")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true, in = ParameterIn.PATH)})
    public ParametersDO edit(@Valid ParametersDO parameters, @PathVariable Long id) {

        this.parametersManager.edit(parameters, id);

        return parameters;
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除参数")
    @Parameters({
            @Parameter(name = "id", description = "要删除的参数主键", required = true, in = ParameterIn.PATH)})
    public String delete(@PathVariable Long id) {

        this.parametersManager.delete(id);

        return "";
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个参数")
    @Parameters({
            @Parameter(name = "id", description = "要查询的参数主键", required = true, in = ParameterIn.PATH)})
    public ParametersDO get(@PathVariable Long id) {

        ParametersDO parameters = this.parametersManager.getModel(id);

        return parameters;
    }

    @Operation(summary = "参数上移或者下移")
    @Parameters({
            @Parameter(name = "sort_type", description = "排序类型，上移 up，下移down", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "param_id", description = "参数主键", required = true, in = ParameterIn.PATH),})
    @PutMapping(value = "/{param_id}/sort")
    public String paramSort(@PathVariable("param_id") Long paramId, @Parameter(hidden = true) @SortType String sortType) {

        this.parametersManager.paramSort(paramId, sortType);

        return null;
    }

}

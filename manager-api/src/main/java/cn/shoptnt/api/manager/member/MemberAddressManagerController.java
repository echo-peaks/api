/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.model.member.dos.MemberAddress;
import cn.shoptnt.service.member.MemberAddressManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 会员地址api
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 * 2018-03-18 15:37:00
 */
@RestController
@RequestMapping("/admin/members")
@Tag(name = "会员地址相关API")
public class MemberAddressManagerController {

    @Autowired
    private MemberAddressManager memberAddressManager;

    @Operation(summary = "查询指定会员的地址列表")
    @GetMapping(value = "/addresses/{member_id}")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id", required = true,  in = ParameterIn.PATH)
    })
    public WebPage list(@Parameter(hidden = true) @PathVariable("member_id") Long memberId, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.memberAddressManager.list(pageNo, pageSize, memberId);
    }
}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.aftersale;

import cn.shoptnt.model.aftersale.dto.AfterSaleQueryParam;
import cn.shoptnt.model.aftersale.vo.AfterSaleExportVO;
import cn.shoptnt.model.aftersale.vo.AfterSaleRecordVO;
import cn.shoptnt.model.aftersale.vo.ApplyAfterSaleVO;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.service.aftersale.AfterSaleQueryManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 售后服务相关API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-24
 */
@Tag(name = "售后服务相关API")
@RestController
@RequestMapping("/admin/after-sales")
@Validated
public class AfterSaleManagerController {

    @Autowired
    private AfterSaleQueryManager afterSaleQueryManager;

    @Operation(summary = "获取申请售后服务记录列表")
    @GetMapping()
    public WebPage list(@Valid AfterSaleQueryParam param) {
        return afterSaleQueryManager.list(param);
    }

    @Operation(summary = "获取售后服务详细信息")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后服务单号", required = true, in = ParameterIn.PATH)
    })
    @GetMapping(value = "/detail/{service_sn}")
    public ApplyAfterSaleVO detail(@PathVariable("service_sn") String serviceSn) {

        return afterSaleQueryManager.detail(serviceSn, Permission.ADMIN);
    }

    @Operation(summary = "导出售后服务信息")
    @GetMapping(value = "/export")
    public List<AfterSaleExportVO> export(@Valid AfterSaleQueryParam param) {
        return afterSaleQueryManager.exportAfterSale(param);
    }
}

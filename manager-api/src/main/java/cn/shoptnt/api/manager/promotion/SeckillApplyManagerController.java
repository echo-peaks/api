/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.seckill.dto.SeckillQueryParam;
import cn.shoptnt.model.promotion.seckill.vo.SeckillApplyVO;
import cn.shoptnt.service.promotion.seckill.SeckillGoodsManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 限时抢购商品列表
 *
 * @author Snow create in 2018/6/13
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/admin/promotion/seckill-applys")
@Tag(name = "限时抢购商品管理API")
@Validated
public class SeckillApplyManagerController {

    @Autowired
    private SeckillGoodsManager seckillApplyManager;

    @Operation(summary = "查询限时抢购商品列表")
    @Parameters({
            @Parameter(name = "seckill_id", description = "限时抢购活动id", in = ParameterIn.QUERY),
            @Parameter(name = "status", description = "审核状态", in = ParameterIn.QUERY,
                    example = "APPLY:待审核，PASS：通过审核，FAIL：未通过审核"),
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "商家ID", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "取消原因", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "取消原因", in = ParameterIn.QUERY)
    })
    @GetMapping()
    public WebPage<SeckillApplyVO> list(@Parameter(hidden = true) Long seckillId, @Parameter(hidden = true) String status, @Parameter(hidden = true) String goodsName,
                                        @Parameter(hidden = true) Long sellerId, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        SeckillQueryParam queryParam = new SeckillQueryParam();
        queryParam.setPageNo(pageNo);
        queryParam.setPageSize(pageSize);
        queryParam.setSeckillId(seckillId);
        queryParam.setStatus(status);
        queryParam.setGoodsName(goodsName);
        queryParam.setSellerId(sellerId);
        WebPage webPage = this.seckillApplyManager.list(queryParam);
        return webPage;
    }

}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.model.promotion.coupon.dto.CouponParams;
import cn.shoptnt.model.promotion.coupon.enums.CouponType;
import cn.shoptnt.service.promotion.coupon.CouponManager;
import cn.shoptnt.framework.exception.NoPermissionException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

/**
 * 平台优惠券控制器
 *
 * @author Snow
 * @version v2.0
 * @since v7.0.0
 * 2018-04-17 23:19:39
 */
@SuppressWarnings("Duplicates")
@RestController
@RequestMapping("/admin/promotion/coupons")
@Tag(name = "平台优惠券相关API")
@Validated
public class CouponManagerController {

    @Autowired
    private CouponManager couponManager;

    @Operation(summary = "查询平台优惠券分页数据列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间",   in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "截止时间",   in = ParameterIn.QUERY),
            @Parameter(name = "keyword", description = "查询关键字",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize,
                        @Parameter(hidden = true) CouponParams params) {
        params.setPageNo(pageNo);
        params.setPageSize(pageSize);
        //如果是平台优惠券，所属商家ID为0
        params.setSellerId(0L);
        return this.couponManager.list(params);
    }

    @Operation(summary = "新增平台优惠券信息")
    @PostMapping
    public CouponDO add(@Valid CouponDO couponDO) {
        //平台优惠券商家ID默认为0
        couponDO.setSellerId(0L);
        couponDO.setSellerName("平台优惠券");
        //新增平台优惠券信息
        this.couponManager.add(couponDO);
        return couponDO;
    }

    @PutMapping(value = "/{coupon_id}")
    @Operation(summary = "修改平台优惠券信息")
    @Parameter(name = "coupon_id", description = "优惠券主键ID", required = true,  in = ParameterIn.PATH)
    public CouponDO add(@Parameter(hidden = true) @PathVariable("coupon_id") Long couponId, @Valid CouponDO couponDO) {
        //设置主键ID
        couponDO.setCouponId(couponId);
        //平台优惠券商家ID默认为0
        couponDO.setSellerId(0L);
        couponDO.setSellerName("平台优惠券");
        //修改平台优惠券信息
        this.couponManager.edit(couponDO, couponId);
        return couponDO;
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除平台优惠券信息")
    @Parameters({
            @Parameter(name = "id", description = "要删除的优惠券主键ID", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        //权限验证
        this.couponManager.verifyAuth(id);
        //删除平台优惠券信息
        this.couponManager.delete(id);
        return "";
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个平台优惠券信息")
    @Parameters({
            @Parameter(name = "id", description = "要查询的优惠券主键ID", required = true,  in = ParameterIn.PATH)
    })
    public CouponDO get(@PathVariable Long id) {
        //根据ID获取平台优惠券信息
        CouponDO coupon = this.couponManager.getModel(id);
        //权限校验
        if (coupon == null || coupon.getSellerId() != 0) {
            throw new NoPermissionException("无权操作或者数据不存在");
        }
        return coupon;
    }


    @GetMapping(value = "/list")
    @Operation(summary = "获取活动领取优惠券")
    public List<CouponDO> list(@PathVariable Integer status) {
        return this.couponManager.getList(0L, CouponType.ACTIVITY_GIVE.name());
    }

    @GetMapping(value = "/{status}/list")
    @Operation(summary = "根据优惠券状态获取优惠券数据集合")
    @Parameters({
            @Parameter(name = "status", description = "优惠券状态 0：全部，1：有效，2：失效", required = true,  in = ParameterIn.PATH)
    })
    public List<CouponDO> getByStatus(@PathVariable Integer status) {
        return this.couponManager.getByStatus(0L, status);
    }

}

package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.sign.dto.SignInRecordSearch;
import cn.shoptnt.service.promotion.sign.SignInRecordManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author zh
 * @version 1.0
 * @title SignInRecordManagerController
 * @description 签到记录相关api
 * @program: api
 * 2024/3/12 14:57
 */
@RestController
@RequestMapping("/admin/promotion/sign/record")
@Tag(name = "签到记录相关api")
@Validated
public class SignInRecordManagerController {

    @Autowired
    private SignInRecordManager signInRecordManager;

    @Operation(summary = "分页查询签到记录")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo,
                        @Parameter(hidden = true) Long pageSize,
                        SignInRecordSearch sign) {
        return this.signInRecordManager.list(pageNo, pageSize, sign);
    }
}

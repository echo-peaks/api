/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goodssearch;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.service.goodssearch.GoodsWordsManager;
import cn.shoptnt.model.goodssearch.enums.GoodsWordsType;
import cn.shoptnt.model.goodssearch.CustomWords;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
* @author liuyulei
 * @version 1.0
 * @Description:  搜索提示词设置
 * @date 2019/6/14 15:38
 * @since v7.0
 */

@RestController
@RequestMapping("/admin/goodssearch/goods-words")
@Tag(name = "搜索提示词相关API")
public class GoodsWordsManagerController {


    @Autowired
    private GoodsWordsManager goodsWordsManager;



    @Operation(summary = "查询提示词列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "keywords", description = "关键字",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String keywords) {

        return this.goodsWordsManager.listPage(pageNo, pageSize,keywords);
    }


    @Operation(summary = "添加自定义提示词")
    @Parameters({
            @Parameter(name = "words", description = "提示词", required = true,   in = ParameterIn.QUERY)
    })
    @PostMapping
    public void add(@Parameter(hidden = true) @Length(max = 30 ,message = "最大长度30") String words) {

        this.goodsWordsManager.addWord(words);

    }

    @PutMapping(value = "/{id}/words")
    @Operation(summary = "修改自定义提示词")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "words", description = "提示词", required = true,   in = ParameterIn.QUERY)
    })
    public void edit(@Parameter(hidden = true) @Length(max = 30 ,message = "最大长度30") String  words, @PathVariable Long id) {

        this.goodsWordsManager.updateWords(words, id);

    }


    @PutMapping(value = "/{id}/sort")
    @Operation(summary = "修改提示词排序")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "sort", description = "序号", required = true,  in = ParameterIn.QUERY)
    })
    public void updateSort(@PathVariable Long id,@NotNull(message = "必须输入序号") @Min(value = 0,message = "序号必须大于等于0")@Max(value = 999999,message = "最大值为999999")  Integer sort) {
        this.goodsWordsManager.updateSort(id, sort);

    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除提示词")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public void delete(@PathVariable Long id) {
        this.goodsWordsManager.delete(GoodsWordsType.PLATFORM,id);
    }

}

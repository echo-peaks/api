/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.distribution;

import cn.shoptnt.model.errorcode.DistributionErrorCode;
import cn.shoptnt.service.distribution.exception.DistributionException;
import cn.shoptnt.model.distribution.dos.WithdrawApplyDO;
import cn.shoptnt.model.distribution.vo.WithdrawApplyVO;
import cn.shoptnt.model.distribution.vo.WithdrawAuditPaidVO;
import cn.shoptnt.service.distribution.WithdrawManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分销提现控制器
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/24 下午5:11
 */
@Tag(name = "分销提现控制器")
@RestController
@RequestMapping("/admin/distribution/withdraw")
public class WithdrawManagerController {
    @Autowired
    private WithdrawManager withdrawManager;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Operation(summary = "提现申请审核列表")
    @GetMapping(value = "/apply")
    @Parameters({
            @Parameter(name = "page_size", description = "页码大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "uname", description = "会员名", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY),
            @Parameter(name = "status", description = "状态 全部的话，不要传递参数即可 APPLY:申请中/VIA_AUDITING:审核通过/FAIL_AUDITING:审核未通过/RANSFER_ACCOUNTS:已转账 ", in = ParameterIn.QUERY)
    })
    public WebPage<WithdrawApplyVO> pageApply(@Parameter(hidden = true) String endTime, @Parameter(hidden = true) String startTime, @Parameter(hidden = true) String uname, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String status) {

        Map<String,String> map = new HashMap(4);
        map.put("start_time", startTime);
        map.put("end_time", endTime);
        map.put("uname", uname);
        map.put("status",status);
        return withdrawManager.pageApply(pageNo, pageSize, map);
    }

    @Operation(summary = "导出提现申请")
    @GetMapping(value = "/export")
    @Parameters({
            @Parameter(name = "uname", description = "会员名", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY),
            @Parameter(name = "status", description = "状态 全部的话，不要传递参数即可 APPLY:申请中/VIA_AUDITING:审核通过/FAIL_AUDITING:审核未通过/RANSFER_ACCOUNTS:已转账 ", in = ParameterIn.QUERY)
    })
    public List<WithdrawApplyDO> exportApply(@Parameter(hidden = true) String endTime, @Parameter(hidden = true) String startTime, @Parameter(hidden = true) String uname, @Parameter(hidden = true) String status) {

        Map<String,String> map = new HashMap(4);
        map.put("start_time", startTime);
        map.put("end_time", endTime);
        map.put("uname", uname);
        map.put("status",status);
        return withdrawManager.exportApply(map);
    }

    @PostMapping(value = "/batch/auditing")
    @Operation(summary = "批量审核提现申请")
    @Parameters({
            @Parameter(name = "audit_result", description = "申请结果 VIA_AUDITING：审核通过/FAIL_AUDITING：审核拒绝", required = true, in = ParameterIn.QUERY)
    })
    public void batchAuditing(@Valid @RequestBody WithdrawAuditPaidVO withdrawAuditPaidVO, @Parameter(hidden = true) @NotEmpty(message = "请选择审核通过还是拒绝") String auditResult) {
        try {
            this.withdrawManager.batchAuditing(withdrawAuditPaidVO, auditResult);
        } catch (Exception e) {
            logger.error("提现审核异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }

    @Operation(summary = "批量设为已转账")
    @PostMapping(value = "/batch/account/paid")
    public void batchAccountPaid(@Valid @RequestBody WithdrawAuditPaidVO withdrawAuditPaidVO) {
        try {
            this.withdrawManager.batchAccountPaid(withdrawAuditPaidVO);
        } catch (Exception e) {
            logger.error("提现申请设置已转账出错：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }
}

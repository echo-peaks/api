/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.promotion;

import cn.shoptnt.model.promotion.seckill.dos.SeckillApplyDO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillDO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillRangeDO;
import cn.shoptnt.model.promotion.seckill.dto.SeckillQueryParam;
import cn.shoptnt.model.promotion.seckill.enums.SeckillStatusEnum;
import cn.shoptnt.model.promotion.seckill.vo.SeckillVO;
import cn.shoptnt.service.promotion.seckill.SeckillGoodsManager;
import cn.shoptnt.service.promotion.seckill.SeckillManager;
import cn.shoptnt.service.promotion.seckill.SeckillRangeManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.exception.SystemErrorCodeV1;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.StringUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * 限时抢购申请控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 17:30:09
 */
@RestController
@RequestMapping("/seller/promotion/seckill-applys")
@Tag(name = "限时抢购申请相关API")
@Validated
public class SeckillApplySellerController {

    @Autowired
    private SeckillGoodsManager seckillApplyManager;

    @Autowired
    private SeckillRangeManager seckillRangeManager;

    @Autowired
    private SeckillManager seckillManager;


    @Operation(summary = "查询限时抢购申请商品列表")
    @Parameters({
            @Parameter(name = "seckill_id", description = "限时抢购活动id", in = ParameterIn.QUERY),
            @Parameter(name = "status", description = "审核状态", in = ParameterIn.QUERY,
                    example = "APPLY:待审核，PASS：通过审核，FAIL：未通过审核"),
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "取消原因", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "取消原因", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage<SeckillVO> list(@Parameter(hidden = true) @NotNull(message = "限时抢购活动参数为空") Long seckillId, @Parameter(hidden = true) String status,
                                   @Parameter(hidden = true) String goodsName, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        SeckillQueryParam queryParam = new SeckillQueryParam();
        queryParam.setPageNo(pageNo);
        queryParam.setPageSize(pageSize);
        queryParam.setSeckillId(seckillId);
        queryParam.setStatus(status);
        queryParam.setGoodsName(goodsName);
        queryParam.setSellerId(UserContext.getSeller().getSellerId());
        return this.seckillApplyManager.list(queryParam);
    }


    @Operation(summary = "添加限时抢购申请")
    @PostMapping
    public List<SeckillApplyDO> add(@Valid @RequestBody @NotEmpty(message = "申请参数为空") List<SeckillApplyDO> seckillApplyList) {

        SeckillApplyDO applyDO = seckillApplyList.get(0);
        this.verifyParam(seckillApplyList, applyDO.getSeckillId());
        this.seckillApplyManager.addApply(seckillApplyList);

        return seckillApplyList;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除限时抢购申请")
    @Parameters({
            @Parameter(name = "id", description = "要删除的限时抢购申请主键", required = true, in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.seckillApplyManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个限时抢购申请")
    @Parameters({
            @Parameter(name = "id", description = "要查询的限时抢购申请主键", required = true, in = ParameterIn.PATH)
    })
    public SeckillApplyDO get(@PathVariable Long id) {
        SeckillApplyDO seckillApply = this.seckillApplyManager.getModel(id);

        Seller seller = UserContext.getSeller();
        //验证越权操作
        if (seckillApply == null || !seller.getSellerId().equals(seckillApply.getSellerId())) {
            throw new NoPermissionException("无权操作");
        }

        return seckillApply;
    }


    @Operation(summary = "查询所有的限时抢购活动")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", in = ParameterIn.QUERY),
            @Parameter(name = "seckill_name", description = "活动名称", in = ParameterIn.QUERY),
            @Parameter(name = "status", description = "状态", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "限时抢购日期-开始日期", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "限时抢购日期-结束日期", in = ParameterIn.QUERY)
    })
    @GetMapping(value = "/seckill")
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String seckillName,
                        @Parameter(hidden = true) String status, @Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {

        SeckillQueryParam param = new SeckillQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setSeckillName(seckillName);
        param.setStatus(status);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        WebPage page = this.seckillManager.list(param);

        List<SeckillDO> list = page.getData();
        Seller seller = UserContext.getSeller();

        List<SeckillVO> seckillVOList = new ArrayList<>();
        for (SeckillDO seckillDO : list) {

            //处于编辑中的显示抢购活动，不在商家显示  update -by liuyulei 2019-05-23
            if (SeckillStatusEnum.EDITING.name().equals(seckillDO.getSeckillStatus())) {
                continue;
            }

            String shopIds = seckillDO.getSellerIds();

            String[] shopId = new String[0];

            if (!StringUtil.isEmpty(shopIds)) {
                shopId = shopIds.split(",");
            }

            SeckillVO seckillVO = new SeckillVO();
            BeanUtils.copyProperties(seckillDO, seckillVO);

            //如果已超过报名时间，已截止
            long nowTime = DateUtil.getDateline();
            long applyEndTime = seckillDO.getApplyEndTime();
            if (nowTime > applyEndTime) {
                seckillVO.setIsApply(2);

                //如果申请过，已报名
            } else if (Arrays.asList(shopId).contains(seller.getSellerId() + "")) {
                seckillVO.setIsApply(1);

                //未报名
            } else {
                seckillVO.setIsApply(0);
            }
            seckillVOList.add(seckillVO);
        }
        page.setData(seckillVOList);
        return page;
    }


    @GetMapping(value = "/{seckill_id}/seckill")
    @Operation(summary = "查询一个限时抢购活动")
    @Parameters({
            @Parameter(name = "seckill_id", description = "要查询的限时抢购活动主键", required = true, in = ParameterIn.PATH)
    })
    public SeckillVO getSeckill(@Parameter(hidden = true) @PathVariable("seckill_id") Long seckillId) {
        SeckillVO seckill = this.seckillManager.getModel(seckillId);
        return seckill;
    }


    /**
     * 验证参数的正确性
     *
     * @param applyDOList
     * @param seckillId   限时抢购活动id
     */
    private void verifyParam(List<SeckillApplyDO> applyDOList, Long seckillId) {


        //根据限时抢购活动id 读取所有的时刻集合
        List<SeckillRangeDO> list = this.seckillRangeManager.getList(seckillId);
        List<Integer> rangIdList = new ArrayList<>();

        for (SeckillRangeDO seckillRangeDO : list) {
            rangIdList.add(seckillRangeDO.getRangeTime());
        }

        /**
         * 存储参加活动的商品id，用来判断同一个商品不能重复参加某个活动
         */
        Map<Long, Long> map = new HashMap<>();

        for (SeckillApplyDO applyDO : applyDOList) {

            Seller seller = UserContext.getSeller();
            applyDO.setSellerId(seller.getSellerId());
            applyDO.setShopName(seller.getSellerName());

            if (applyDO.getSeckillId() == null || applyDO.getSeckillId() == 0) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "限时抢购活动ID参数异常");
            } else {
                SeckillVO seckillVO = this.seckillManager.getModel(seckillId);

                //活动申请最后时间
                long applyEndTime = seckillVO.getApplyEndTime();

                //服务器当前时间
                long nowTime = DateUtil.getDateline();

                //当前时间大于活动最后申请时间，不能申请
                if (nowTime > applyEndTime) {
                    throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "已超过活动最后申请时间");
                }

            }

            if (applyDO.getTimeLine() == null) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "时刻参数异常");
            } else {

                //判断此活动的时刻集合是否包含正要添加的时刻,如果不包含说明时刻参数有异常
                if (!rangIdList.contains(applyDO.getTimeLine())) {
                    throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "时刻参数异常");
                }
            }

            if (applyDO.getStartDay() == null || applyDO.getStartDay().intValue() == 0) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动开始时间参数异常");
            }

            if (applyDO.getGoodsId() == null || applyDO.getGoodsId() == 0) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "商品ID参数异常");
            }

            if (StringUtil.isEmpty(applyDO.getGoodsName())) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "商品名称参数异常");
            }

            if (applyDO.getPrice() == null || applyDO.getPrice() < 0) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "抢购价参数不能小于0");
            }

//            if (applyDO.getSoldQuantity() == null || applyDO.getSoldQuantity().intValue() <= 0) {
//                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "售空数量参数不能小于0");
//            }

            if (applyDO.getPrice() > applyDO.getOriginalPrice()) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动价格不能大于商品原价");
            }

            if (map.get(applyDO.getSkuId()) != null) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, applyDO.getGoodsName() + ",该商品不能同时参加多个时间段的活动");
            }
            //由原来的商品id换成skuid判断重复性
            map.put(applyDO.getSkuId(), applyDO.getSkuId());

        }
    }


}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.shop;

import cn.shoptnt.model.shop.dos.ShopMenu;
import cn.shoptnt.model.shop.vo.ShopMenuVO;
import cn.shoptnt.model.shop.vo.ShopMenusVO;
import cn.shoptnt.service.shop.ShopMenuManager;
import cn.shoptnt.model.system.dos.Menu;
import cn.shoptnt.framework.util.BeanUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

/**
 * 菜单管理控制器
 *
 * @author zh
 * @version v7.0
 * @since v7.0.0
 * 2018-06-19 09:46:02
 */
@RestController
@RequestMapping("/seller/shops/menus")
@Tag(name = "菜单管理相关API")
public class MenuSellerController {

    @Autowired
    private ShopMenuManager shopMenuManager;


    @Operation(summary = "根据父id查询所有菜单")
    @Parameters({
            @Parameter(name = "parent_id", description = "菜单父id，如果查询顶级菜单则传0", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping("/{parent_id}/children")
    public List<ShopMenusVO> getMenuTree(@PathVariable("parent_id") @Parameter(hidden = true) Long parentId) {
        return this.shopMenuManager.getMenuTree(parentId);
    }


    @Operation(summary = "添加菜单")
    @PostMapping
    public ShopMenu add(@Valid ShopMenuVO menu) {
        return this.shopMenuManager.add(menu);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改菜单")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public ShopMenu edit(@Valid ShopMenuVO menuVO, @PathVariable Long id) {
        ShopMenu menu = new ShopMenu();
        //复制属性
        BeanUtil.copyProperties(menuVO, menu);
        return this.shopMenuManager.edit(menu, id);
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除菜单")
    @Parameters({
            @Parameter(name = "id", description = "要删除的菜单管理主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.shopMenuManager.delete(id);
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个菜单")
    @Parameters({
            @Parameter(name = "id", description = "要查询的菜单管理主键", required = true,  in = ParameterIn.PATH)
    })
    public ShopMenu get(@PathVariable Long id) {
        ShopMenu menu = this.shopMenuManager.getModel(id);
        return menu;
    }

}

/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.shop;

import cn.shoptnt.model.shop.enums.ShopCatShowTypeEnum;
import cn.shoptnt.model.shop.dos.ShopCatDO;
import cn.shoptnt.service.shop.ShopCatManager;
import cn.shoptnt.framework.context.user.UserContext;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;



import javax.validation.Valid;
import java.util.List;

/**
 * 店铺分组控制器
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-24 11:18:37
 */
@RestController
@RequestMapping("/seller/shops/cats")
@Tag(name = "店铺分组相关API")
public class ShopCatSellerController {

    @Autowired
    private ShopCatManager shopCatManager;


    @Operation(summary = "查询店铺分组列表")
    @GetMapping
    public List list() {
        return this.shopCatManager.list(UserContext.getSeller().getSellerId(), ShopCatShowTypeEnum.ALL.name());
    }


    @Operation(summary = "添加店铺分组")
    @PostMapping
    public ShopCatDO add(@Valid ShopCatDO shopCat) {

        this.shopCatManager.add(shopCat);

        return shopCat;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改店铺分组")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public ShopCatDO edit(@Valid ShopCatDO shopCat, @PathVariable("id") Long id) {
        this.shopCatManager.edit(shopCat, id);

        return shopCat;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除店铺分组")
    @Parameters({
            @Parameter(name = "id", description = "要删除的店铺分组主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable("id") Long id) {

        this.shopCatManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个店铺分组")
    @Parameters({
            @Parameter(name = "id", description = "要查询的店铺分组主键", required = true,  in = ParameterIn.PATH)
    })
    public ShopCatDO get(@PathVariable Long id) {

        ShopCatDO shopCat = this.shopCatManager.getModel(id);

        return shopCat;
    }


}
